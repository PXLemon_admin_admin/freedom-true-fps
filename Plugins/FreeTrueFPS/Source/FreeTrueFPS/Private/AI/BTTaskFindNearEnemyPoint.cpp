﻿
#include "AI/BTTaskFindNearEnemyPoint.h"
#include "NavigationSystem.h"
#include "AI/FreeAIController.h"
#include "Character/FreeHeroCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"


EBTNodeResult::Type UBTTaskFindNearEnemyPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AFreeAIController* MyAIController = Cast<AFreeAIController>(OwnerComp.GetAIOwner());
	if (MyAIController) return EBTNodeResult::Type::Failed;

	APawn* MyBot = MyAIController->GetPawn();
	AFreeHeroCharacter* Enemy = MyAIController->GetEnemy();

	if (Enemy && MyBot)
	{
		const float SearchRadius = 200.0f;
		const FVector SearchOrigin = Enemy->GetActorLocation() + 600.0f * (MyBot->GetActorLocation() - Enemy->GetActorLocation()).GetSafeNormal();
		FVector Loc(0);
		UNavigationSystemV1::K2_GetRandomReachablePointInRadius(MyAIController, SearchOrigin, Loc, SearchRadius);
		if (Loc != FVector::ZeroVector)
		{
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID(), Loc);
			return EBTNodeResult::Succeeded;
		}
	}
	
	return EBTNodeResult::Type::Failed;
}
