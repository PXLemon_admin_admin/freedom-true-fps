﻿
#include "AI/FreeAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "AI/FreeCharacterBot.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Character/FreeHeroCharacter.h"
#include "Component/FreeHealthComponent.h"
#include "Component/FreeHeroComponent.h"
#include "GameFramework/GameStateBase.h"
#include "Weapon/FreeFirearmBase.h"

AFreeAIController::AFreeAIController()
{
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));
	BrainComponent = BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));	
}

void AFreeAIController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	// 停止行为树
	BehaviorTreeComp->StopTree();

	// 停止任何已有的运动
	StopMovement();

	// 取消重新生成Timer
	GetWorldTimerManager().ClearTimer(TimerHandle_Respawn);

	// 清空所有敌人
	SetEnemy(nullptr);

	// 停止开火
	if (const AFreeCharacterBot* Bot = Cast<AFreeCharacterBot>(GetPawn()))
	{
		if (Bot->GetHeroComponent())
		{
			if (AFreeFirearmBase* Gun = Bot->GetHeroComponent()->GetCurrentGun())
			{
				Gun->StopFire();
			}
		}
	}
	
}

void AFreeAIController::BeginInactiveState()
{
	Super::BeginInactiveState();

	const AGameStateBase* const GameState = GetWorld()->GetGameState();

	const float MinRespawnDelay = GameState ? GameState->GetPlayerRespawnDelay(this): 1.0f;

	GetWorldTimerManager().SetTimer(TimerHandle_Respawn, this, &ThisClass::Respawn, MinRespawnDelay);
}

void AFreeAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AFreeCharacterBot* Bot = Cast<AFreeCharacterBot>(InPawn);

	// 开始执行行为树
	if (Bot && Bot->BotBehavior)
	{
		if (Bot->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Bot->BotBehavior->BlackboardAsset);
		}

		EnemyKeyID = BlackboardComp->GetKeyID("Enemy");
		NeedAmmoKeyID = BlackboardComp->GetKeyID("NeedAmmo");

		BehaviorTreeComp->StartTree(*Bot->BotBehavior);
	}
}

void AFreeAIController::OnUnPossess()
{
	Super::OnUnPossess();

	BehaviorTreeComp->StopTree();
}

void AFreeAIController::Respawn()
{
	GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}

void AFreeAIController::CheckAmmo(const AFreeFirearmBase* CurrentGun)
{
	if (CurrentGun && BlackboardComp)
	{
		const int32 Ammo = CurrentGun->GetTotalCurrentAmmo();
		const int32 MaxAmmo = CurrentGun->GetMaxAmmo();
		const float Ratio = static_cast<float>(Ammo) / static_cast<float>(MaxAmmo);

		BlackboardComp->SetValue<UBlackboardKeyType_Bool>(NeedAmmoKeyID, (Ratio <= 0.1f));
	}
}

void AFreeAIController::SetEnemy(APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InPawn);
		SetFocus(InPawn);
	}
}

AFreeHeroCharacter* AFreeAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<AFreeHeroCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID));
	}
	return nullptr;
}

void AFreeAIController::ShootEnemy()
{
	AFreeCharacterBot* MyBot = Cast<AFreeCharacterBot>(GetPawn());
	const UFreeHeroComponent* HeroComponent = nullptr;
	if (MyBot) HeroComponent = MyBot->GetHeroComponent();
	AFreeFirearmBase* MyGun = nullptr;
	if (HeroComponent) MyGun = HeroComponent->GetCurrentGun();

	if (MyGun) return;

	bool bCanShot = false;

	AFreeHeroCharacter* Enemy = GetEnemy();
	const UFreeHealthComponent* const HealthComponent = Enemy ? Enemy->GetHealthComponent() : nullptr;
	const bool bEnemyIsAlive = HealthComponent ? HealthComponent->GetIsAlive(): false;

	if (Enemy && bEnemyIsAlive && MyGun->GetTotalCurrentAmmo() > 0 && MyGun->CanFire())
	{
		if (LineOfSightTo(Enemy, MyBot->GetActorLocation()))
		{
			bCanShot = true;
		}
	}

	if (bCanShot)
	{
		MyGun->StartFire();
	}
	else
	{
		MyGun->StopFire();
	}
}

void AFreeAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	// Look toward focus
	FVector FocalPoint = GetFocalPoint();
	if (!FocalPoint.IsZero() && GetPawn())
	{
		FVector Direction = FocalPoint - GetPawn()->GetActorLocation();
		FRotator NewControlRot = Direction.Rotation();

		NewControlRot.Yaw = FRotator::ClampAxis(NewControlRot.Yaw);

		SetControlRotation(NewControlRot);

		APawn* const P = GetPawn();
		if (P && bUpdatePawn)
		{
			P->FaceRotation(NewControlRot, DeltaTime);
		}
	}
}

