﻿
#include "AI/FreeCharacterBot.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"


AFreeCharacterBot::AFreeCharacterBot()
{
	PrimaryActorTick.bCanEverTick = true;

	BotBehavior = CreateDefaultSubobject<UBehaviorTree>(TEXT("BotBehaviorTree"));
	
	AIControllerClass = AFreeCharacterBot::StaticClass();
	
	bUseControllerRotationYaw = true;

	if (FPCamera) FPCamera->SetActive(false);
	if (TPSpringArm) TPSpringArm->SetActive(false);
	
}

void AFreeCharacterBot::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	FRotator CurrentRot = FMath::RInterpTo(GetActorRotation(), NewControlRotation, DeltaTime, 8.0f);
	
	Super::FaceRotation(CurrentRot, DeltaTime);
}

