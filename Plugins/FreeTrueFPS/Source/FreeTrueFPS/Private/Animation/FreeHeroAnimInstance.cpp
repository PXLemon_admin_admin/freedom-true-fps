﻿
#include "Animation/FreeHeroAnimInstance.h"
#include "Character/FreeCharacterBase.h"
#include "Character/FreeHeroCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Curves/CurveVector.h"
#include "Component/FreeHeroComponent.h"
#include "Weapon/FreeFirearmBase.h"
#include "State/FirearmRecoilState.h"
#include "State/FreeGunSwayData.h"

UFreeHeroAnimInstance::UFreeHeroAnimInstance()
{
	AimInterpolationSpeed = 20.0f;

	Speed = 0.f;
	bIsInAir = false;
	bInterpRelativeToHand = false;
	bIsAiming = false;
	AimingAlpha = 0.0f;
	RotationAlpha = false;
	bInterpAiming = false;
	bIsReloadingForHandIK = false;
	Direction = 0.f;
	CurrentAcceleration = 0.f;
	SightDistance = 10.0f;
	AimingLeanHeadAngle = 35.f;

	MyHeroComponent = nullptr;
	Character = nullptr;
	Gun = nullptr;
}

void UFreeHeroAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	Character = Cast<AFreeCharacterBase>(TryGetPawnOwner());
	
	if (const AActor* OwingActor = GetOwningActor())
	{
		MyHeroComponent = Cast<UFreeHeroComponent>(OwingActor->GetComponentByClass(UFreeHeroComponent::StaticClass()));
		if (MyHeroComponent)
		{
			AimingLeanHeadAngle = MyHeroComponent->GetAimingHeadLeanAngle();
		}
	}
}

void UFreeHeroAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (!Character || !MyHeroComponent) return;

	if (Gun != MyHeroComponent->GetCurrentGun())
	{
		Gun = MyHeroComponent->GetCurrentGun();
	}

	RefreshSpinePitch(DeltaSeconds);
	RefreshRelativeToHand(DeltaSeconds);
	RefreshAimingState(DeltaSeconds);
	RefreshLocomotionState(DeltaSeconds);
	RefreshLeanAngle(DeltaSeconds);
	RefreshRecoil(DeltaSeconds);
	RefreshSway(DeltaSeconds);
}

void UFreeHeroAnimInstance::RefreshLocomotionState(float DeltaSeconds)
{
	const FVector CharacterVelocity = Character->GetVelocity();
	Speed = CharacterVelocity.Size();

	const FRotator ActorRot = Character->GetActorRotation();
	Direction = CalculateDirection(CharacterVelocity, ActorRot);
	bIsInAir = Character->GetCharacterMovement()->IsFalling();
	CurrentAcceleration = Character->GetCharacterMovement()->GetCurrentAcceleration().Size();

	if (Gun) bIsReloadingForHandIK = Gun->GetIsReloadingForHandIK();
}

void UFreeHeroAnimInstance::RefreshSpinePitch(float DeltaSeconds)
{
	const FRotator ComposeRot = UKismetMathLibrary::ComposeRotators(Character->GetReplicatedControlRotation(),
		FRotator(180.f, 0.f, 0.f));
	// for 4 bones
	PitchPerBone.Roll  = ComposeRot.Pitch / 4.f;
	//PitchLClavicle.Roll = ComposeRot.Pitch / 1.2f;
	const FRotator Rot = PitchPerBone;
	PitchPerBone = UKismetMathLibrary::RInterpTo(PitchPerBone, Rot, DeltaSeconds, 10.f);
}

void UFreeHeroAnimInstance::RefreshLeanAngle(float DeltaSeconds)
{
	LeanAngle = UKismetMathLibrary::FInterpTo(LeanAngle, MyHeroComponent->GetLeanAngle(),
		DeltaSeconds, FMath::RandRange(6, 10));
	LeanAnglePerBone = UKismetMathLibrary::MakeRotator(0.f, LeanAngle / 3.f, 0.f);
	LeanAngleForHead = LeanAnglePerBone * -1.25f;
}

void UFreeHeroAnimInstance::RefreshRecoil(float DeltaSeconds)
{
	if (!Gun /*|| MyHeroComponent->GetIsEnableFireMontage()*/) return;

	if (bInterpRecoil)
	{
		RunRecoil(DeltaSeconds);
	}
}

void UFreeHeroAnimInstance::RunRecoil(float DeltaSeconds)
{
	const float CurrentTime = GetWorld()->GetTimeSeconds() - RecoilStartTime;
	if (const FFirearmRecoilState* RecoilData = Gun->GetFirearmRecoilData())
	{
		if (const UCurveVector* RecoilLocationCurve = RecoilData->RecoilLocationRifleCurve)
		{
			const float Randomness = FMath::FRandRange(RecoilData->LocationRandomness.Min, RecoilData->LocationRandomness.Max);
			RecoilLocation += RecoilLocationCurve->GetVectorValue(CurrentTime) * Randomness;
			RecoilLocation *= RecoilData->RecoilMultiplier;
			FinalRecoilTransform.SetLocation(RecoilLocation);
		}
		if (const UCurveVector* RecoilRotationCurve = RecoilData->RecoilRotationRifleCurve)
		{
			const FVector CurveData = RecoilRotationCurve->GetVectorValue(CurrentTime);
			const float PitchRandom = (CurveData.Y * FMath::FRandRange(RecoilData->RecoilRotationRandomPitch.Min, RecoilData->RecoilRotationRandomPitch.Max)) * RecoilMultiplier;
			const float YawRandom = (CurveData.Z * FMath::FRandRange(RecoilData->RecoilRotationRandomYaw.Min, RecoilData->RecoilRotationRandomYaw.Max)) * RecoilMultiplier;
			const float RollRandom = (CurveData.X * FMath::FRandRange(RecoilData->RecoilRotationRandomRoll.Min, RecoilData->RecoilRotationRandomRoll.Max)) * RecoilMultiplier;
			
			RecoilRotation += FRotator(PitchRandom, YawRandom, RollRandom);
			RecoilRotation *= RecoilData->RecoilMultiplier;

			float ReRoll = RollRandom;
			float RePitch = PitchRandom;
			float ReYaw = YawRandom;
			if (RecoilData->bEnableRotationClamp)
			{
				ReRoll = FMath::Clamp(RecoilRotation.Roll, RecoilData->RecoilRotationRandomRoll.Min, RecoilData->RecoilRotationRandomRoll.Max);
				RePitch = FMath::Clamp(RecoilRotation.Pitch, RecoilData->RecoilRotationRandomPitch.Min, RecoilData->RecoilRotationRandomPitch.Max);
				ReYaw = FMath::Clamp(RecoilRotation.Yaw, RecoilData->RecoilRotationRandomYaw.Min, RecoilData->RecoilRotationRandomYaw.Max);
			}
			RecoilRotation = FRotator(RePitch, ReYaw, ReRoll);
			FinalRecoilTransform.SetRotation(RecoilRotation.Quaternion());
		}
	}
	
	RefreshRecoilToZero(DeltaSeconds);
}

void UFreeHeroAnimInstance::RefreshRecoilToZero(float DeltaSeconds)
{
	FinalRecoilTransform = UKismetMathLibrary::TInterpTo(FinalRecoilTransform, FTransform(), DeltaSeconds, 8.0f);
	RecoilLocation = FinalRecoilTransform.GetLocation();
	RecoilRotation = FinalRecoilTransform.Rotator();
	if (RecoilLocation.Equals(FVector::ZeroVector, 0.1f) && RecoilRotation.Equals(FRotator::ZeroRotator, 0.1f))
	{
		bInterpRecoil = false;
	}
}

void UFreeHeroAnimInstance::RefreshSway(float DeltaSeconds)
{
	float SwayRotRoll = MyHeroComponent->GetMoveRightAxis() * SwayPitchMultiplier;
	float SwayRotPitch = MyHeroComponent->GetLookUpAxis() * SwayPitchMultiplier;
	float SwayRotYaw = MyHeroComponent->GetTurnAxis() * SwayPitchMultiplier;

	if (Gun)
	{
		if (const FFreeGunSwayData* SwayData = Gun->GetFirearmSwayData())
		{
			float AimingMultiplier = 1.0f;
			if (bIsAiming)
			{
				if (MyHeroComponent)
				{
					AimingMultiplier = MyHeroComponent->GetAimingSwayMultiplier();
				}
				else
				{
					AimingMultiplier = 0.3f; // 摇晃百分比覆盖 0.3 = 摇晃程度只有未瞄准的30%
				}
			}
			SwayRotRoll = FMath::Clamp(SwayRotRoll, SwayData->SwayRotationRoll.Min * AimingMultiplier, SwayData->SwayRotationRoll.Max * AimingMultiplier);
			SwayRotPitch = FMath::Clamp(SwayRotPitch, SwayData->SwayRotationPitch.Min * AimingMultiplier, SwayData->SwayRotationPitch.Max * AimingMultiplier);
			SwayRotYaw = FMath::Clamp(SwayRotYaw, SwayData->SwayRotationYaw.Min * AimingMultiplier, SwayData->SwayRotationYaw.Max * AimingMultiplier);
		}
	}
	
	SwayInterpRotation = FRotator(SwayRotPitch, SwayRotYaw, SwayRotRoll);
	SwayRotation = FMath::RInterpTo(SwayRotation, SwayInterpRotation, DeltaSeconds, SwayInterpSpeed);

	/*const FRotator TempRot = FRotator(SwayRotPitch, SwayRotRoll, SwayRotYaw);
	FMath::RInterpConstantTo(SwayRotation, TempRot, DeltaSeconds, 5.0f);*/
}

void UFreeHeroAnimInstance::DoRecoil(float Multiplier)
{
	bInterpRecoil = true;
	RecoilStartTime = GetWorld()->GetTimeSeconds();
	RecoilMultiplier = Multiplier;
}

void UFreeHeroAnimInstance::DoRecoil(bool bUseAxisMultiplier)
{
	if (!bUseAxisMultiplier)
	{
		DoRecoil();
		return;
	}
	if (bUseAxisMultiplier && Gun)
	{
		if (const FFirearmRecoilState* RecoilData = Gun->GetFirearmRecoilData())
		{
			DoRecoil(RecoilData->RecoilMultiplierRotPerAxis);
		}
	}
}

void UFreeHeroAnimInstance::SetAiming(bool bNewAiming)
{
	if (!bNewAiming)
	{
		bIsAiming = false;
		bInterpAiming = true;
		return;
	}

	if (bIsAiming != bNewAiming)
	{
		bIsAiming = bNewAiming;
		bInterpAiming = true;
	}
}

void UFreeHeroAnimInstance::SetReloading(bool bNewReloading)
{
	bIsReloadingForHandIK = bNewReloading;
}

#pragma region Procedural ADS

void UFreeHeroAnimInstance::RefreshRelativeToHand(float DeltaSeconds)
{
	if (Gun)
	{
		SetRelativeToHand();
		InterpRelativeToHand(DeltaSeconds);
	}
}

void UFreeHeroAnimInstance::RefreshAimingState(float DeltaSeconds)
{
	if (bInterpAiming && Gun)
	{
		InterpAimingAlpha(DeltaSeconds);
	}
}

void UFreeHeroAnimInstance::SetSightTransform()
{
	FTransform CameraTransform = Character->GetMesh()->GetSocketTransform(MyHeroComponent->GetFPCameraSocket(), RTS_ParentBoneSpace);
	/*UE_LOG(LogTemp, Error, TEXT(" fpcamera socket %s"), *MyHeroComponent->GetFPCameraSocket().ToString())
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(
			-1, 0.f, FColor::Emerald,
			FString::Printf(TEXT("FPCameraSocket(), RTS_ParentBoneSpace %s"), *CameraTransform.ToString())
		);
	}*/

	FRotator NewRot = FRotator::ZeroRotator;
	NewRot.Roll += -90.0f;
	NewRot.Yaw += 90.0f;
	CameraTransform.SetRotation(NewRot.Quaternion());
	
	FVector CameraVector = CameraTransform.GetLocation();
	const float AdditiveDistance = SightDistance + 10.0f;

	CameraVector.Y += AdditiveDistance;
	CameraTransform.SetLocation(CameraVector);
	
	SightLocation = CameraTransform.GetLocation();
	SightRotation = CameraTransform.Rotator();
}

void UFreeHeroAnimInstance::SetRelativeToHand()
{
	AimingOffsetRot = Gun->GetAimingOffsetRot();
	AimingOffsetLoc = Gun->GetAimingOffsetLoc();

	const FTransform SightSocketTransform  = Gun->GetSightAimTransform();
	const FTransform Hand_RTransform = MyHeroComponent->GetHandRTransform();
	
	FinalRelativeHand = UKismetMathLibrary::MakeRelativeTransform(SightSocketTransform, Hand_RTransform);

	/*FVector TestLoc = SightSocketTransform.GetLocation();
	TestLoc.Z += 80.0f;
	SightSocketTransform.SetLocation(TestLoc);*/

	const FTransform DefaultTransform = Gun->GetSightAimTransform();
	DefaultRelativeToHand = UKismetMathLibrary::MakeRelativeTransform(DefaultTransform, Hand_RTransform);
	bInterpRelativeToHand = true;
}

void UFreeHeroAnimInstance::InterpAimingAlpha(float DeltaSeconds)
{
	float InterpSpeed = AimInterpolationSpeed;
	float Multiplier = Gun->GetAimingInterpSpeed();
	Multiplier = UKismetMathLibrary::NormalizeToRange(Multiplier, -40.0f, 150.0f);
	InterpSpeed *= Multiplier;

	AimingAlpha = UKismetMathLibrary::FInterpTo(AimingAlpha, bIsAiming, DeltaSeconds, InterpSpeed);
	
	if ((bIsAiming && AimingAlpha >= 1.0f) || (!bIsAiming && AimingAlpha <= 0.0f))
	{
		bInterpAiming = false;
	}
}

void UFreeHeroAnimInstance::InterpRelativeToHand(float DeltaSeconds)
{
	float InterpSpeed = AimInterpolationSpeed;
	float Multiplier = Gun->GetAimingInterpSpeed();

	Multiplier = UKismetMathLibrary::NormalizeToRange(Multiplier, -40.0f, 150.0f);
	Multiplier = FMath::Clamp(Multiplier, 0.0f, 1.0f);
	InterpSpeed *= Multiplier;
	
	// Change InterpSpeed to be modified by firearm in hand
	RelativeToHandTransform = UKismetMathLibrary::TInterpTo(RelativeToHandTransform, FinalRelativeHand, DeltaSeconds, InterpSpeed);
	RelativeToHandLocation = RelativeToHandTransform.GetLocation();
	RelativeToHandRotation = RelativeToHandTransform.Rotator();
	
	//float HandToSightDistance = FinalRelativeHand.GetLocation().X;
	const float HandToSightDistance = -SightDistance;

	SightDistance = UKismetMathLibrary::FInterpTo(SightDistance, HandToSightDistance * -1.0f, DeltaSeconds, InterpSpeed);
	SetSightTransform();

	if (RelativeToHandTransform.Equals(FinalRelativeHand))
	{
		bInterpRelativeToHand = false;
		SightDistance = HandToSightDistance * -1.0f;
		SetSightTransform();
	}
}

#pragma endregion 


