
#include "Animation/FreeHeroAnimInstanceProxy.h"

/*#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"*/

FFreeHeroAnimInstanceProxy::FFreeHeroAnimInstanceProxy(UAnimInstance* Instance)
{
	/*Character = nullptr;
	Movement = nullptr;
	bIsInAir = false;
	bIsAccelerating = false;
	bIsCrouched = false;
	bIsLocalControlled = false;
	Speed = 0.0f;*/
}

void FFreeHeroAnimInstanceProxy::InitializeObjects(UAnimInstance* InAnimInstance)
{
	FAnimInstanceProxy::InitializeObjects(InAnimInstance);

	/*Character = Cast<ACharacter>(InAnimInstance->TryGetPawnOwner());
	if (!Character) return;
	
	Movement = Character->GetCharacterMovement();*/
}

void FFreeHeroAnimInstanceProxy::PreUpdate(UAnimInstance* InAnimInstance, float DeltaSeconds)
{
	FAnimInstanceProxy::PreUpdate(InAnimInstance, DeltaSeconds);

	//if (!InAnimInstance || !Character) return;

	/*bIsInAir = Movement->IsFalling();
	bIsAccelerating = Movement->GetCurrentAcceleration().Size() > 0.0f ? true : false;
	bIsCrouched = Movement->IsCrouching();
	bIsLocalControlled = Character->IsLocallyControlled();*/
	
}

void FFreeHeroAnimInstanceProxy::Update(float DeltaSeconds)
{
	FAnimInstanceProxy::Update(DeltaSeconds);
}

void FFreeHeroAnimInstanceProxy::PostUpdate(UAnimInstance* InAnimInstance) const
{
	FAnimInstanceProxy::PostUpdate(InAnimInstance);
}

