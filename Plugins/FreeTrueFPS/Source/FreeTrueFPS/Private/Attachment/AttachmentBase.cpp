﻿
#include "Attachment/AttachmentBase.h"
#include "Data/AttachmentBaseData.h"
#include "Components/StaticMeshComponent.h"


AAttachmentBase::AAttachmentBase()
{
	PrimaryActorTick.bCanEverTick = false;

	AttachmentMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AttachmentMesh"));
	SetRootComponent(AttachmentMesh);

	AttachmentMesh->SetCollisionEnabled( ECollisionEnabled::NoCollision);
	AttachmentMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	AttachmentMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	AttachmentMesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	AttachmentMesh->SetCollisionResponseToChannel(ECC_Destructible, ECR_Block);
	AttachmentMesh->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Block);

	static ConstructorHelpers::FObjectFinder<UAttachmentBaseData>
	const DDFinder(TEXT("AttachmentBaseData'/FreeTrueFPS/FreeAsset/Attachment/DA_Sight.DA_Sight'"));
	if (DDFinder.Succeeded())
	{
		AttachmentBaseData = DDFinder.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("XXXXXXXX in AAttachmentBase Constructor UAttachmentBaseData load failed !!!!!!!"))
	}
	
	bReplicates = true;
}

void AAttachmentBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AAttachmentBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAttachmentBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (AttachmentBaseData)
	{
		AttachmentMesh->SetStaticMesh(AttachmentBaseData->AttachmentMesh);
	}
}

FORCEINLINE UStaticMeshComponent* AAttachmentBase::GetAttachmentMesh() const
{
	return AttachmentMesh;
}

FORCEINLINE UAttachmentBaseData* AAttachmentBase::GetAttachmentBaseData() const
{
	return AttachmentBaseData;
}

FAttachmentBoostData* AAttachmentBase::GetAttachmentBoostData() const
{
	return &AttachmentBaseData->AttachmentBoostData;
}

FAttachmentBoostData& AAttachmentBase::GetAttachmentBoostDataRef() const
{
	return AttachmentBaseData->AttachmentBoostData;
}

FName AAttachmentBase::GetAttachToItemSocketName() const
{
	return AttachmentBaseData->AttachToItemSocket;
}

FVector AAttachmentBase::GetEffectSocketLocation() const
{
	return AttachmentMesh->GetSocketLocation(AttachmentBaseData->EffectSocket);
}
