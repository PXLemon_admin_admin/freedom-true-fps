﻿
#include "Attachment/FreeHandGuard.h"


AFreeHandGuard::AFreeHandGuard()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AFreeHandGuard::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFreeHandGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

