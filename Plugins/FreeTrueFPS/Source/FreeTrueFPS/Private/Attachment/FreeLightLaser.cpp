﻿
#include "Attachment/FreeLightLaser.h"

#include "Data/AttachmentBaseData.h"


AFreeLightLaser::AFreeLightLaser()
{
	PrimaryActorTick.bCanEverTick = false;

	bAimable = true;
	AimSocket = AttachmentBaseData->AimSocket;
}

void AFreeLightLaser::BeginPlay()
{
	Super::BeginPlay();
}

void AFreeLightLaser::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FTransform AFreeLightLaser::GetAimTransform() const
{
	return AttachmentMesh->GetSocketTransform(AimSocket);
}

FORCEINLINE bool AFreeLightLaser::GetAimable() const
{
	return bAimable;
}
