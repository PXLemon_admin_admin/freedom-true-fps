﻿
#include "Attachment/FreeMuzzle.h"
#include "Data/AttachmentBaseData.h"


AFreeMuzzle::AFreeMuzzle()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AFreeMuzzle::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFreeMuzzle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AFreeMuzzle::GetIsSilencer() const
{
	return AttachmentBaseData->bIsSilencer;
}

FName AFreeMuzzle::GetMuzzleSocketName() const
{
	return AttachmentBaseData->EffectSocket;
}

