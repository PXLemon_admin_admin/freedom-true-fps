﻿
#include "Attachment/FreeSightBase.h"
#include "Data/AttachmentBaseData.h"

AFreeSightBase::AFreeSightBase()
{
	PrimaryActorTick.bCanEverTick = false;
	
	bAimable = AttachmentBaseData->bAimable;
	
}

void AFreeSightBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFreeSightBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AFreeSightBase::GetIsGun() const
{
	return false;
}

void AFreeSightBase::ZoomSight(bool bZoomIn)
{
	ISightInterface::ZoomSight(bZoomIn);
}

float AFreeSightBase::GetZoomSensitivity() const
{
	return 1.0f;
}

FTransform AFreeSightBase::GetSightAimTransform()
{
	return AttachmentMesh->GetSocketTransform(AttachmentBaseData->AimSocket);
}

FORCEINLINE bool AFreeSightBase::GetAimable() const
{
	return bAimable;
}

