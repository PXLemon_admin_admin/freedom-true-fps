﻿
#include "Character/FreeCharacterBase.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"


AFreeCharacterBase::AFreeCharacterBase()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
}

void AFreeCharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME_CONDITION(ThisClass, RepControlRotation, COND_SkipOwner);
}

void AFreeCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

/*FRotator AFreeCharacterBase::GetAimOffset() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}*/

void AFreeCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (IsLocallyControlled() || HasAuthority())
	{
		RepControlRotation = GetControlRotation();
	}
	RepControlRotForAimOffset = UKismetMathLibrary::NormalizedDeltaRotator(RepControlRotation, GetActorRotation());
}

void AFreeCharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	GetCharacterMovement()->SetJumpAllowed(true);
}

FRotator AFreeCharacterBase::GetReplicatedControlRotation() const
{
	return RepControlRotation;
}

FRotator AFreeCharacterBase::GetAimOffsetRotation() const
{
	return RepControlRotForAimOffset;
}

