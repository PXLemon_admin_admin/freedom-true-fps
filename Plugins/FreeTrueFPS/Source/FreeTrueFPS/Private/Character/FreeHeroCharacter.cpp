﻿
#include "Character/FreeHeroCharacter.h"
#include "FreeTrueFPS.h"
// #include "Animation/FreeHeroAnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Component/FreeHealthComponent.h"
#include "Component/FreeHeroComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Weapon/FreeDamageType.h"


AFreeHeroCharacter::AFreeHeroCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	FPCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FPCamea"));
	TPSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("TPSpringArm"));
	TPCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("TPCamera"));
	FreeHeroComponent = CreateDefaultSubobject<UFreeHeroComponent>(TEXT("FreeHeroComponent"));
	FreeHealthComponent = CreateDefaultSubobject<UFreeHealthComponent>(TEXT("FreeHealthComponent"));
	/*Weapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon"));
	Optic = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Optic"));

	Weapon->SetupAttachment(GetMesh(), TEXT("KA48"));
	Optic->SetupAttachment(Weapon, TEXT("Sight"));*/

	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Ignore);

	FPCamera->SetupAttachment(GetMesh(), TEXT("FP_Camera"));
	TPCamera->SetupAttachment(TPSpringArm);
	TPCamera->SetActive(false);
}

void AFreeHeroCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
}

void AFreeHeroCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	FreeHeroComponent->Init(FPCamera, GetMesh(), nullptr);

	OnTakePointDamage.AddDynamic(this, &ThisClass::OnDamage);
}

void AFreeHeroCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

#pragma region InputBind
void AFreeHeroCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ThisClass::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ThisClass::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ThisClass::Turn);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &ThisClass::LookUp);

	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ThisClass::JumpStart);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Released, this, &ThisClass::JumpStop);
	PlayerInputComponent->BindAction(TEXT("Aim"), EInputEvent::IE_Pressed, this, &ThisClass::AimingStart);
	PlayerInputComponent->BindAction(TEXT("Aim"), EInputEvent::IE_Released, this, &ThisClass::AimingStop);
	
	PlayerInputComponent->BindAction(TEXT("LeanLeft"), EInputEvent::IE_Pressed, this, &ThisClass::LeanLeftStart);
	PlayerInputComponent->BindAction(TEXT("LeanLeft"), EInputEvent::IE_Released, this, &ThisClass::LeanStop);
	PlayerInputComponent->BindAction(TEXT("LeanRight"), EInputEvent::IE_Pressed, this, &ThisClass::LeanRightStart);
	PlayerInputComponent->BindAction(TEXT("LeanRight"), EInputEvent::IE_Released, this, &ThisClass::LeanStop);

	PlayerInputComponent->BindAction(TEXT("CycleSights"), EInputEvent::IE_Pressed, this, &ThisClass::CycleSights);
	PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &ThisClass::Reload);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &ThisClass::FirePressed);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &ThisClass::FireRelease);
	PlayerInputComponent->BindAction(TEXT("SwitchFireMode"), EInputEvent::IE_Released, this, &ThisClass::SwitchFireMode);
}
#pragma endregion 

#pragma region Init

void AFreeHeroCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
}

void AFreeHeroCharacter::PostInitProperties()
{
	Super::PostInitProperties();
	
}

#pragma endregion

#pragma region Input

#pragma region Basic
void AFreeHeroCharacter::MoveForward(float Axis)
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->MoveForward(Axis);
	}
}

void AFreeHeroCharacter::MoveRight(float Axis)
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->MoveRight(Axis);
	}
}

void AFreeHeroCharacter::Turn(float Axis)
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->Turn(Axis);
	}
}

void AFreeHeroCharacter::LookUp(float Axis)
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->LookUp(Axis);
	}
}
#pragma endregion 

void AFreeHeroCharacter::JumpStart()
{
	Jump();
	/*if (FreeHeroComponent)
	{
		FreeHeroComponent->JumpStart();
	}*/
}

void AFreeHeroCharacter::JumpStop()
{
	StopJumping();
	/*if (FreeHeroComponent)
	{
		FreeHeroComponent->JumpStop();
	}*/
}

void AFreeHeroCharacter::AimingStart()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->AimingStart();
	}
}

void AFreeHeroCharacter::AimingStop()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->AimingStop();
	}
}

void AFreeHeroCharacter::LeanLeftStart()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->LeanLeftStart();
	}
}

void AFreeHeroCharacter::LeanRightStart()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->LeanRightStart();
	}
}

void AFreeHeroCharacter::LeanStop()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->LeanStop();
	}
}

void AFreeHeroCharacter::CycleSights()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->CycleSights();
	}
}

void AFreeHeroCharacter::Reload()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->Reload();
	}
}

void AFreeHeroCharacter::FirePressed()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->FirePressed();
	}
}

void AFreeHeroCharacter::FireRelease()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->FireRelease();
	}
}

void AFreeHeroCharacter::SwitchFireMode()
{
	if (FreeHeroComponent)
	{
		FreeHeroComponent->SwitchFireMode();
	}
}

#pragma endregion

FTransform AFreeHeroCharacter::GetCameraTransform() const
{
	return FPCamera ? FPCamera->GetComponentTransform() : GetActorTransform();
}

UFreeHeroComponent* AFreeHeroCharacter::GetHeroComponent() const
{
	return FreeHeroComponent;
}

UFreeHealthComponent* AFreeHeroCharacter::GetHealthComponent() const
{
	return FreeHealthComponent;
}

void AFreeHeroCharacter::ApplyDamage(float BaseDamage, const UPhysicalMaterial* const PhysicalMaterial,
                                     AActor* DamageActor, FVector& HitFromDirection, const FHitResult& HitResult,
                                     AController* EventInstigator, AActor* DamageCauser, TSubclassOf<UFreeDamageType> DamageTypeClass)
{
	if (FreeHealthComponent)
	{
		FreeHealthComponent->ApplyDamage(BaseDamage, PhysicalMaterial, DamageActor,
			HitFromDirection, HitResult, EventInstigator, DamageCauser, DamageTypeClass);
	}
}

void AFreeHeroCharacter::OnDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation,
                                  UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection,
                                  const UDamageType* DamageType, AActor* DamageCauser)
{
	if (FreeHealthComponent)
	{
		FreeHealthComponent->TakeDamage(Damage);
	}
}


