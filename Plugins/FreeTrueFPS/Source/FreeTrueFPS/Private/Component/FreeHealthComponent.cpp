﻿
#include "Component/FreeHealthComponent.h"
#include "Net/UnrealNetwork.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Data/BodyDamageData.h"
#include "Weapon/FreeDamageType.h"


UFreeHealthComponent::UFreeHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UBodyDamageData>
	const DataFinder(TEXT("BodyDamageData'/FreeTrueFPS/FreeAsset/FreeHeroes/DA_FreeHeroHealth.DA_FreeHeroHealth'"));

	if (DataFinder.Succeeded())
	{
		BodyDamageData = DataFinder.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("XXXXXXX in UFreeHealthComponent Constructor UBodyDamageData load failed !!!!"))
	}

	CurrentHealth = MaxHealth;
}

void UFreeHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ThisClass, CurrentHealth);
}

void UFreeHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	
	SetIsReplicated(true);
	if (GetOwnerRole() == ROLE_Authority)
	{
		CurrentHealth = MaxHealth;
		OnRep_CurrentHealth();
	}
}

void UFreeHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                         FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UFreeHealthComponent::OnRep_CurrentHealth()
{
	// @Todo 更新血量之后的工作
	
	if (CurrentHealth <= 0.0f)
	{
		// 死亡
	}
	// 更新血量UI
	
}


void UFreeHealthComponent::ApplyDamage(float BaseDamage, const UPhysicalMaterial* const PhysicalMaterial,
	AActor* DamageActor, FVector& HitFromDirection, const FHitResult& HitResult,
	AController* EventInstigator, AActor* DamageCauser, TSubclassOf<UFreeDamageType> DamageTypeClass)
{
	float FinalDamage = BaseDamage;

	if (PhysicalMaterial)
	{
		switch (PhysicalMaterial->SurfaceType)
		{
		case SurfaceType1:
			break;
		case SurfaceType2:
			break;
		default:
			break;
		}
	}

	UGameplayStatics::ApplyPointDamage(DamageActor, FinalDamage, HitFromDirection,
		HitResult, EventInstigator, DamageCauser, DamageTypeClass);
	
}

void UFreeHealthComponent::TakeDamage(float Damage)
{
	if (Damage <= 0.0f) return;
	
	if (Damage >= CurrentHealth)
	{
		CurrentHealth = 0.0f;
	}
	else
	{
		CurrentHealth = CurrentHealth - Damage;
	}
	OnRep_CurrentHealth();
}

float UFreeHealthComponent::GetMaxHealth() const
{
	return MaxHealth;
}

float UFreeHealthComponent::GetCurrentHealth() const
{
	return CurrentHealth;
}

float UFreeHealthComponent::GetHealthPercentage() const
{
	return CurrentHealth / MaxHealth;
}

bool UFreeHealthComponent::GetIsAlive() const
{
	return CurrentHealth > 0.0f;
}
