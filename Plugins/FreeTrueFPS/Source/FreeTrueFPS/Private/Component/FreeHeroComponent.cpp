﻿
#include "Component/FreeHeroComponent.h"
#include "Net/UnrealNetwork.h"
#include "Animation/FreeHeroAnimInstance.h"
#include "Animation/FreeMontageCallbackProxy.h"
#include "Attachment/FreeSightBase.h"
#include "Camera/CameraComponent.h"
#include "Character/FreeHeroCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Weapon/FreeFirearmBase.h"
#include "Data/FreeHeroBaseData.h"
#include "State/FirearmAnimState.h"
#include "Components/SkeletalMeshComponent.h"

UFreeHeroComponent::UFreeHeroComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	MyPawn = nullptr;
	TPMesh = nullptr;
	FPMesh = nullptr;
	FPCamera = nullptr;
	CurrentGunFree = nullptr;

	static ConstructorHelpers::FObjectFinder<UFreeHeroBaseData>
	const DFinder(TEXT("FreeHeroBaseData'/FreeTrueFPS/FreeAsset/FreeHeroes/DA_HeroBaseData.DA_HeroBaseData'"));
	if (DFinder.Succeeded())
	{
		InitHeroData(DFinder.Object);
	}
}

void UFreeHeroComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ThisClass, CurrentGunFree);
	DOREPLIFETIME(ThisClass, LeanBodyAngle);
	DOREPLIFETIME_CONDITION(ThisClass, bIsAiming, COND_SkipOwner);

	// 仅对本地所有者武器切换请求本地发起 其他客户端不需要它
	DOREPLIFETIME_CONDITION(ThisClass, FirearmsInventory, COND_OwnerOnly);
}

void UFreeHeroComponent::BeginPlay()
{
	Super::BeginPlay();
	
	SetIsReplicated(true);
}

bool UFreeHeroComponent::GetHasAuthority() const
{
	return GetOwner() ? GetOwner()->HasAuthority() : false;
}

void UFreeHeroComponent::InitHeroData(class UFreeHeroBaseData* HeroData)
{
	if (!HeroData) return;

	FreeHeroBaseData = HeroData;

	BaseTurnRate = FreeHeroBaseData->InputBaseTurnRate;
	DefaultLeanAngle = FreeHeroBaseData->DefaultLeanBodyAngle;
	AimingSwayMultiplier = FreeHeroBaseData->AimingSwayMultiplier;
	FPCameraSocket = FreeHeroBaseData->FPCameraSocket;
	HandRSocket = FreeHeroBaseData->HandRSocket;
}

#pragma region BasicInput
FORCEINLINE void UFreeHeroComponent::MoveForward(float Axis)
{
	if (MyPawn && MyPawn->Controller)
	{
		MoveForwardAxis = Axis;
		if (Axis != 0.0f)
		{
			MyPawn->AddMovementInput(FRotationMatrix(
				MyPawn->GetControlRotation()).GetUnitAxis(EAxis::X), Axis);
		}
	}
}

FORCEINLINE void UFreeHeroComponent::MoveRight(float Axis)
{
	if (MyPawn && MyPawn->Controller)
	{
		MoveRightAxis = Axis;
		if (Axis != 0.0f)
		{
			MyPawn->AddMovementInput(FRotationMatrix(
				MyPawn->GetControlRotation()).GetUnitAxis(EAxis::Y), Axis);
		}
	}
}

FORCEINLINE void UFreeHeroComponent::Turn(float Axis)
{
	if (MyPawn && MyPawn->Controller)
	{
		TurnAxis = Axis;
		if (Axis != 0.0f)
		{
			MyPawn->AddControllerYawInput(
				Axis *
				BaseTurnRate *
				GetWorld()->GetDeltaSeconds() *
				GetSightZoomSensitivity()
			);
		}
	}
}

FORCEINLINE void UFreeHeroComponent::LookUp(float Axis)
{
	if (MyPawn && MyPawn->Controller)
	{
		LookUpAxis = Axis;
		if (Axis != 0.0f)
		{
			MyPawn->AddControllerPitchInput(
				Axis *
				BaseTurnRate *
				GetWorld()->GetDeltaSeconds() *
				GetSightZoomSensitivity()
			);
		}
	}
}
#pragma endregion 

void UFreeHeroComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
}

void UFreeHeroComponent::PostInitProperties()
{
	Super::PostInitProperties();

	InitHeroData(FreeHeroBaseData);
}

FORCEINLINE APawn* UFreeHeroComponent::GetOwnerPawn() const
{
	return MyPawn;
}

FORCEINLINE USkeletalMeshComponent* UFreeHeroComponent::GetPawnMesh() const
{
	return TPMesh;
}

FORCEINLINE USkeletalMeshComponent* UFreeHeroComponent::GetTPMesh() const
{
	return TPMesh;
}

FORCEINLINE USkeletalMeshComponent* UFreeHeroComponent::GetFPMesh() const
{
	return FPMesh;
}

FORCEINLINE AController* UFreeHeroComponent::GetPawnController() const
{
	return PawnController.Get();
}

#pragma region InputAction

void UFreeHeroComponent::JumpStart()
{
	
}

void UFreeHeroComponent::JumpStop()
{
	
}

void UFreeHeroComponent::AimingStart()
{

	/*if (CurrentGunFree && CurrentGunFree->GetCurrentSight())
	{
		UE_LOG(LogTemp, Error, TEXT("hero component if (CurrentGunFree && CurrentGunFree->GetCurrentSight())"));
		if (!CurrentGunFree->GetCurrentSight()->GetAimable())
		{
			UE_LOG(LogTemp, Error, TEXT("hero component current sight aimable is false!!!"));
			return;
		}
	}*/
	SetAimingState(true);
}

void UFreeHeroComponent::AimingStop()
{
	SetAimingState(false);
}

void UFreeHeroComponent::LeanLeftStart()
{
	SetLeanAngle(-DefaultLeanAngle);
	if (!GetHasAuthority())
	{
		Server_SetLeanAngle(-DefaultLeanAngle);
	}
}

void UFreeHeroComponent::LeanRightStart()
{
	SetLeanAngle(DefaultLeanAngle);
	if (!GetHasAuthority())
	{
		Server_SetLeanAngle(DefaultLeanAngle);
	}
}

void UFreeHeroComponent::LeanStop()
{
	SetLeanAngle(0.f);
	if (!GetHasAuthority())
	{
		Server_SetLeanAngle(0.f);
	}
}

void UFreeHeroComponent::CycleSights()
{
	if (CurrentGunFree)
	{
		CurrentGunFree->SwitchSight();
	}
}

void UFreeHeroComponent::Reload()
{
	if (CurrentGunFree)
	{
		CurrentGunFree->Reload();
	}
	/*if (!CheckCanReload()) return;
	
	bIsReloading = true;
	if (!GetHasAuthority())
	{
		Server_PlayReload();
		return;
	}
	Multi_PlayReloadAnim();*/
}

void UFreeHeroComponent::FirePressed()
{
	if (CurrentGunFree)
	{
		CurrentGunFree->StartFire();
	}
	
	/*if (!GetHasAuthority())
	{
		Server_StartFire();
	}
	else
	{
		Multi_StartFire();
	}*/
}

void UFreeHeroComponent::FireRelease()
{
	if (CurrentGunFree)
	{
		CurrentGunFree->StopFire();
	}
}

void UFreeHeroComponent::SwitchFireMode()
{
	if (CurrentGunFree)
	{
		CurrentGunFree->SwitchFireMode();
	}
}

#pragma endregion

#pragma region RPC

void UFreeHeroComponent::Server_SetAiming_Implementation(bool bAiming)
{
	bIsAiming = bAiming;
	OnRep_IsAiming();
}

/*FORCEINLINE void UFreeHeroComponent::Server_PlayReload_Implementation()
{
	bIsReloading = true;
	Multi_PlayReloadAnim();
}

FORCEINLINE void UFreeHeroComponent::Multi_PlayReloadAnim_Implementation()
{
	if (CurrentGunFree && HeroInterface && HeroInterface->GetPawnMesh())
	{
		if (FirearmAnimState && FirearmAnimState->FullBodyReloadMontage)
		{
			if (UFreeMontageCallbackProxy* PlayMontageCallbackProxy =
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(HeroInterface->GetPawnMesh(), FirearmAnimState->FullBodyReloadMontage))
			{
				PlayMontageCallbackProxy->OnCompleted.AddDynamic(this, &ThisClass::ReloadComplete);
				PlayMontageCallbackProxy->OnInterrupted.AddDynamic(this, &ThisClass::ReloadOnInterrupted);
			}
		}
		if (FirearmAnimState->FirearmReloadMontage && CurrentGunFree->GetMesh())
		{
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(CurrentGunFree->GetMesh(), FirearmAnimState->FirearmReloadMontage);
		}
	}
}*/


void UFreeHeroComponent::Server_SetLeanAngle_Implementation(const float LeanAngle)
{
	SetLeanAngle(LeanAngle);
}

void UFreeHeroComponent::Server_StartFire_Implementation()
{
	Multi_StartFire();
}

void UFreeHeroComponent::Multi_StartFire_Implementation()
{
	Fire();
}

void UFreeHeroComponent::Server_SetEquipGun_Implementation(class AFreeFirearmBase* Gun)
{
	EquipGun(Gun);
	OnRep_CurrentGun(Gun);
}

#pragma endregion

#pragma region OnRep

void UFreeHeroComponent::OnRep_CurrentGun(class AFreeFirearmBase* LastWeapon)
{
	SetCurrentWeapon(CurrentGunFree, LastWeapon);
}

void UFreeHeroComponent::OnRep_IsAiming()
{
	if (AnimInstance.IsValid())
	{
		AnimInstance->SetAiming(bIsAiming);
	}
}

#pragma endregion

void UFreeHeroComponent::SetAimingState(bool bAiming)
{
	bIsAiming = bAiming;
	if (AnimInstance.IsValid())
	{
		AnimInstance->SetAiming(bIsAiming);
	}
	if (!GetHasAuthority())
	{
		Server_SetAiming(bAiming);
	}
}

void UFreeHeroComponent::SetLeanAngle(const float NewLeanAngle)
{
	LeanBodyAngle = NewLeanAngle;
}

FTransform UFreeHeroComponent::GetSightSocketTransform() const
{
	return CurrentGunFree ? CurrentGunFree->GetSightAimTransform() : FTransform();
}

FTransform UFreeHeroComponent::GetHandRTransform() const
{
	return TPMesh->GetSocketTransform(HandRSocket);
}

FTransform UFreeHeroComponent::GetFPCameraSocketTransform(ERelativeTransformSpace RelativeTransformSpace) const
{
	return TPMesh->GetSocketTransform(FPCameraSocket, RelativeTransformSpace);
}

FFreeMinMax* UFreeHeroComponent::GetDefaultLookUpAngle() const
{
	return &FreeHeroBaseData->DefaultLookUpAngle;
}

bool UFreeHeroComponent::GetAiming() const
{
	return bIsAiming;
}

float UFreeHeroComponent::GetLeanAngle() const
{
	return LeanBodyAngle;
}

float UFreeHeroComponent::GetAimingHeadLeanAngle() const
{
	return FreeHeroBaseData->AimingLeanHeadAngle;
}

bool UFreeHeroComponent::GetIsEnableFireMontage() const
{
	if (FirearmAnimState)
	{
		return FirearmAnimState->bEnableFireMontage;
	}
	return false;
}

void UFreeHeroComponent::ExecRecoil(float Multiplier) const
{
	if (AnimInstance.IsValid())
	{
		AnimInstance->DoRecoil(Multiplier);
	}
}

void UFreeHeroComponent::SetMyPawn(class AFreeHeroCharacter* NewPawn)
{
	this->MyPawn = NewPawn;
}

void UFreeHeroComponent::SetMyAnimInstance(class UFreeHeroAnimInstance* NewAnimInstance)
{
	this->AnimInstance = NewAnimInstance;
}

void UFreeHeroComponent::Fire()
{
	if (CurrentGunFree->GetFirearmAnimData()->bEnableFireMontage)
	{
		PlayFireAnimMontage();
	}
	else
	{
		// 程序化后坐力
		ExecRecoil(1.f);
	}
}

void UFreeHeroComponent::PlayFireAnimMontage()
{
	if (FirearmAnimState)
	{
		if (FirearmAnimState->FullBodyFireMontage && TPMesh)
		{
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(
				TPMesh, FirearmAnimState->FullBodyFireMontage
			);
		}
	}
}

float UFreeHeroComponent::GetSightZoomSensitivity() const
{
	return CurrentGunFree ? CurrentGunFree->GetSightZoomSensitivity() : 1.0f;
}

void UFreeHeroComponent::SetCurrentWeapon(AFreeFirearmBase* NewWeapon, AFreeFirearmBase* LastWeapon)
{
	AFreeFirearmBase* LocalLastWeapon = nullptr;

	if (LastWeapon != nullptr)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentGunFree)
	{
		LocalLastWeapon = CurrentGunFree;
	}

	// UnEquip previous
	if (LocalLastWeapon)
	{
		LocalLastWeapon->OnUnEquip();
	}

	CurrentGunFree = NewWeapon;

	// equip new one
	if (IsValid(NewWeapon))
	{
		FirearmAnimState = NewWeapon->GetFirearmAnimData();
		FirearmRecoilState = NewWeapon->GetFirearmRecoilData();

		/**
		 * 确保武器的 HeroComponent, OwnerPawn指向我们
		 * 在复制过程中，我们无法保证APawn::CurrentWeapon会在AFreeFirearmBase::HeroComponent之后进行复制！
		 */
		NewWeapon->SetHeroComponent(this);
		
		NewWeapon->OnEquip(LastWeapon);
	}
}

void UFreeHeroComponent::SpawnDefaultInventory()
{
	if (!GetHasAuthority()) return;

	const int NumGunClasses = DefaultInventoryClasses.Num();
	for (int i = 0; i < NumGunClasses; ++i)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.Instigator = MyPawn;
			SpawnInfo.Owner = MyPawn;
			SpawnInfo.bNoFail = true;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AFreeFirearmBase* NewWeapon = GetWorld()->SpawnActor<AFreeFirearmBase>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}
	/*if (FirearmsInventory.Num() > 0)
	{
		EquipGun(FirearmsInventory[0]);
	}*/
}

void UFreeHeroComponent::AddWeapon(AFreeFirearmBase* Gun)
{
	if (Gun && GetHasAuthority())
	{
		Gun->OnEnterInventory(this);
		FirearmsInventory.AddUnique(Gun);
	}
}

void UFreeHeroComponent::RemoveWeapon(AFreeFirearmBase* Gun)
{
	if (Gun && GetHasAuthority())
	{
		Gun->OnLeaveInventory();
		FirearmsInventory.RemoveSingle(Gun);
	}
}

void UFreeHeroComponent::Init(class UCameraComponent * NewFPCamera, class USkeletalMeshComponent* NewTPMesh, class USkeletalMeshComponent* NewFPMesh)
{
	MyPawn = GetOwner<APawn>();

	FPCamera = NewFPCamera;

	if (!FPCamera)
	{
		UE_LOG(LogTemp, Error, TEXT(" 第一人称相机组件为空!!!! "));
	}

	TPMesh = NewTPMesh;

	if (!TPMesh)
	{
		UE_LOG(LogTemp, Error, TEXT(" 全身模型组件为空!!!! "));
	}

	FPMesh = NewFPMesh;
	
	if (MyPawn)
	{
		PawnController = MyPawn->Controller;
		bIsLocalControlled = MyPawn->IsLocallyControlled();

		if (TPMesh)
		{
			AnimInstance = Cast<UFreeHeroAnimInstance>(TPMesh->GetAnimInstance());
		}

		if (bIsLocalControlled)
		{
			UE_LOG(LogTemp, Warning, TEXT(" 是本地控制 "))
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT(" 非本地控制！！！ "))
		}
		
		if (bIsLocalControlled)
		{
			if (const APlayerController* const PC = Cast<APlayerController>(MyPawn->Controller))
			{
				if (const FFreeMinMax* LookUpAngle = GetDefaultLookUpAngle())
				{
					UE_LOG(LogTemp, Warning, TEXT("set LookUpAngle"))
					PC->PlayerCameraManager->ViewPitchMin = LookUpAngle->Min;
					PC->PlayerCameraManager->ViewPitchMax = LookUpAngle->Max;
				}
			}
		}
		
		FPCamera->AttachToComponent(TPMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, FPCameraSocket);
	}

	if (IsValid(MyPawn))
	{
		MyPawn->GetWorldTimerManager().SetTimerForNextTick(this, &ThisClass::SpawnDefaultInventory);
	}
	bInitComplete = true;
}

FORCEINLINE AFreeFirearmBase* UFreeHeroComponent::GetCurrentGun() const
{
	return CurrentGunFree;
}

/*FORCEINLINE void UFreeHeroComponent::AttachItem(AActor* Item, FName SocketName)
{
	if (!(Item && SocketName != NAME_None && HeroInterface)) return;

	if (USkeletalMeshComponent* Mesh = HeroInterface->GetPawnMesh())
	{
		Item->AttachToComponent(Mesh, FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
		/*if (AFreeFirearmBase* Firearm = Cast<AFreeFirearmBase>(Item))
		{
			
		}#1#
	}
}*/

void UFreeHeroComponent::EquipGun(AFreeFirearmBase* Weapon)
{
	if (!Weapon) return;

	if (GetHasAuthority())
	{
		CurrentGunFree = Weapon;
		OnRep_CurrentGun(CurrentGunFree);
	}
	else
	{
		Server_SetEquipGun(Weapon);
	}
}

FORCEINLINE float UFreeHeroComponent::GetMoveForwardAxis() const
{
	return MoveForwardAxis;
}

FORCEINLINE float UFreeHeroComponent::GetMoveRightAxis() const
{
	return MoveRightAxis;
}

FORCEINLINE float UFreeHeroComponent::GetTurnAxis() const
{
	return TurnAxis;
}

FORCEINLINE float UFreeHeroComponent::GetLookUpAxis() const
{
	return LookUpAxis;
}

FORCEINLINE bool UFreeHeroComponent::GetInitComplete() const
{
	return bInitComplete;
}

FORCEINLINE bool UFreeHeroComponent::IsLocallyControlled()
{
	return bIsLocalControlled;
}

FORCEINLINE UCameraComponent* UFreeHeroComponent::GetFPCameraComponent() const
{
	return FPCamera;
}

FORCEINLINE float UFreeHeroComponent::GetAimingSwayMultiplier() const
{
	return AimingSwayMultiplier;
}

FORCEINLINE FName UFreeHeroComponent::GetFPCameraSocket() const
{
	return FPCameraSocket;
}

FORCEINLINE FName UFreeHeroComponent::GetHandRSocket() const
{
	return HandRSocket;
}

bool UFreeHeroComponent::HasMontagePlaying() const
{
	if (AnimInstance.IsValid())
	{
		return AnimInstance->IsAnyMontagePlaying();
	}
	return false;
}

void UFreeHeroComponent::StopMontage(const UAnimMontage* Montage , float BlendOut) const
{
	if (!Montage) return;

	if (AnimInstance.IsValid())
	{
		if (BlendOut < 0.f)
		{
			BlendOut = 0.f;
		}
		AnimInstance->Montage_Stop(BlendOut, Montage);
	}
}

void UFreeHeroComponent::StopFireSlotMontage(float BlendOut) const
{
	if (AnimInstance.IsValid())
	{
		if (BlendOut < 0.f)
		{
			BlendOut = 0.f;
		}
		AnimInstance->StopSlotAnimation(BlendOut, TEXT("Fire"));
	}
}

void UFreeHeroComponent::StopAllMontage(float BlendOut) const
{
	if (AnimInstance.IsValid())
	{
		if (BlendOut < 0.f) BlendOut = 0.f;
		AnimInstance->StopAllMontages(BlendOut);
	}
}
