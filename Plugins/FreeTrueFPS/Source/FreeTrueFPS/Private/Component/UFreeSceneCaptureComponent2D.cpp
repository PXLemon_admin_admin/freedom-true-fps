﻿
#include "Component/FreeSceneCaptureComponent2D.h"
#include "Data/FreeSceneCaptureComp2dData.h"
#include "Kismet/KismetRenderingLibrary.h"


UFreeSceneCaptureComponent2D::UFreeSceneCaptureComponent2D()
{
	bHiddenInGame = false;
	bCaptureEveryFrame = false;
	bCaptureOnMovement = false;
	bAlwaysPersistRenderingState = true;
	PrimitiveRenderMode = ESceneCapturePrimitiveRenderMode::PRM_RenderScenePrimitives;
	CaptureSource = SCS_SceneColorHDRNoAlpha;
	
	ShowFlags.DynamicShadows = true;
	ShowFlags.AmbientOcclusion = false;
	ShowFlags.AmbientCubemap = false;
	ShowFlags.DistanceFieldAO = false;
	ShowFlags.LightFunctions = false;
	ShowFlags.LightShafts = false;
	ShowFlags.ReflectionEnvironment = false;
	ShowFlags.ScreenSpaceReflections = false;
	ShowFlags.TexturedLightProfiles = false;
	ShowFlags.VolumetricFog = false;
	ShowFlags.MotionBlur = 0;

	/*static ConstructorHelpers::FObjectFinder<UFreeSceneCaptureComp2dData>
	const DataFinder(TEXT("FreeSceneCaptureComp2dData'/FreeTrueFPS/FreeAsset/SceneCapture2D/DA_SceneCapture2DData.DA_SceneCapture2DData'"));
	if (DataFinder.Succeeded())
	{
		SceneCaptureComp2dData = DataFinder.Object;
		if (SceneCaptureComp2dData)
		{
			ScopeOptimization = MoveTemp(SceneCaptureComp2dData->ScopeOptimization);
			RenderTargetSize = MoveTemp(SceneCaptureComp2dData->RenderTargetSize);
			SceneCaptureComp2dData = nullptr;
			bInitDataComplete = true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("XXXXXXX in UFreeSceneCaptureComponent2D Constructor SceneCaptureComp2dData load failed !!!!"))
	}*/

}

void UFreeSceneCaptureComponent2D::TickComponent(float DeltaTime, ELevelTick TickType,
												 FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UFreeSceneCaptureComponent2D::PostInitProperties()
{
	Super::PostInitProperties();
	/*if (!bInitDataComplete)
	{
		LoadSceneCaptureComp2dData();
	}*/
}

void UFreeSceneCaptureComponent2D::BeginPlay()
{
	Super::BeginPlay();

	SetupOnBegin();
}

void UFreeSceneCaptureComponent2D::LoadSceneCaptureComp2dData()
{
	UFreeSceneCaptureComp2dData* CaptureComp2dData = LoadObject<UFreeSceneCaptureComp2dData>(
		this,
		TEXT("FreeHeroBaseData'/FreeTrueFPS/FreeAsset/FreeHeroes/DA_HeroBaseData.DA_HeroBaseData'"));
	if (CaptureComp2dData)
	{
		ScopeOptimization = MoveTemp(CaptureComp2dData->ScopeOptimization);
		RenderTargetSize = MoveTemp(CaptureComp2dData->RenderTargetSize);
		CaptureComp2dData->ConditionalBeginDestroy();
		CaptureComp2dData = nullptr;
		bInitDataComplete = true;
	}
}

void UFreeSceneCaptureComponent2D::SetupOnBegin()
{
	SetComponentTickEnabled(false);
	bCaptureEveryFrame = false;
	bCaptureOnMovement = false;

	if (ScopeOptimization.bOverrideCaptureEveryFrame)
	{
		SetComponentTickInterval(1.0f / ScopeOptimization.RefreshRate);
	}
}

void UFreeSceneCaptureComponent2D::StartCapture()
{
	if (ScopeOptimization.bOverrideCaptureEveryFrame && !bCaptureEveryFrame)
	{
		CaptureScene();
		SetComponentTickInterval(1.0f / ScopeOptimization.RefreshRate);
	}
	bCaptureEveryFrame = true;
	bCaptureOnMovement = true;
	SetComponentTickEnabled(true);
}

void UFreeSceneCaptureComponent2D::StopCapture()
{
	if (ScopeOptimization.bDisableWhenNotAiming)
	{
		SetComponentTickInterval(1.0f / ScopeOptimization.RefreshRateNotAiming);
	}
	else
	{
		if (ScopeOptimization.bClearScopeWithColor)
		{
			UKismetRenderingLibrary::ClearRenderTarget2D(GetWorld(), TextureTarget, ScopeOptimization.ClearedColor);
		}
	}
	bCaptureEveryFrame = false;
	bCaptureOnMovement = false;
	SetComponentTickEnabled(false);
}

