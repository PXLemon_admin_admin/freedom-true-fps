﻿
#include "Interface/SightInterface.h"


// Add default functionality here for any ISightInterface functions that are not pure virtual.
bool ISightInterface::GetIsGun() const
{
	return false;
}

FTransform ISightInterface::GetSightAimTransform()
{
	return FTransform();
}

void ISightInterface::ZoomSight(bool bZoomIn)
{
	
}

float ISightInterface::GetZoomSensitivity() const
{
	return 1.0f;
}
