﻿
#include "Library/FreeHeroLibrary.h"
#include "Data/FreeImpactData.h"
#include "Engine/DataTable.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"


float UFreeHeroLibrary::CalcDefAmmoInitSpeedByAmmoType(EFreeAmmoType AmmoType)
{
	switch (AmmoType)
	{
		case EFreeAmmoType::Rocket:
			break;
		case EFreeAmmoType::PistolAmmo:
			break;
		case EFreeAmmoType::RifleAmmo:
			break;
		case EFreeAmmoType::ShotgunAmmo:
			break;
		case EFreeAmmoType::SniperAmmo:
			break;
		default:
			break;
	}
	return 80000.f;
}

void UFreeHeroLibrary::CalcAttachmentBoostDataAdd(FAttachmentBoostData& BoostDataNeedAdd, const FAttachmentBoostData& BoostDataAdd)
{
	BoostDataNeedAdd.DamageBoost += BoostDataAdd.DamageBoost;
	BoostDataNeedAdd.RecoilBoostPercent += BoostDataAdd.RecoilBoostPercent;
	BoostDataNeedAdd.ProjectileInitSpeedBoost += BoostDataAdd.ProjectileInitSpeedBoost;
	BoostDataNeedAdd.ProjectileMaxSpeedBoost += BoostDataAdd.ProjectileMaxSpeedBoost;
}

void UFreeHeroLibrary::CalcAttachmentBoostDataSubtract(FAttachmentBoostData& BoostDataNeedSubtract, const FAttachmentBoostData& BoostDataSubtract)
{
	BoostDataNeedSubtract.DamageBoost -= FMath::Abs(BoostDataSubtract.DamageBoost);
	BoostDataNeedSubtract.RecoilBoostPercent -= FMath::Abs(BoostDataSubtract.RecoilBoostPercent);
	BoostDataNeedSubtract.ProjectileInitSpeedBoost -= FMath::Abs(BoostDataSubtract.ProjectileInitSpeedBoost);
	BoostDataNeedSubtract.ProjectileMaxSpeedBoost -= FMath::Abs(BoostDataSubtract.ProjectileMaxSpeedBoost);
}

FRotator UFreeHeroLibrary::GetEstimatedMuzzleToScopeZero(const FTransform& MuzzleTransform,
	const FTransform& SightTransform, const float RangeMeters)
{
	const FVector EndLocation = SightTransform.GetLocation() + SightTransform.GetRotation().Vector() * RangeMeters;
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(MuzzleTransform.GetLocation(), EndLocation);
	LookAtRotation.Pitch += 0.04f; // Increase vertical projectile launch angle (thanks gravity)
	return LookAtRotation;
}

FRotator UFreeHeroLibrary::SetMuzzleMOA(FRotator MuzzleRotation, float MOA)
{
	MOA /= 100.0f;
	float MOAChange = FMath::RandRange(-MOA / 2.0f, MOA / 2.0f) * 2.54f; // 1 inch at 100 yards
	MuzzleRotation.Yaw += MOAChange;
	MOAChange = FMath::RandRange(-MOA / 2.0f, MOA / 2.0f) * 2.54f;
	MuzzleRotation.Pitch += MOAChange * 0.6f; // reduce vertical MOA shift for consistency

	return MuzzleRotation;
}

/*
void UFreeHeroLibrary::SpawnEffectOnImpact(const FHitResult& HitResult)
{
	if (!HitResult.PhysMaterial.IsValid() || !HitResult.PhysMaterial.Get()) return;

	const FString TablePath = TEXT("");
	const UDataTable* DT = LoadObject<UDataTable>(nullptr, *TablePath);

	if (!DT)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit Impact DataTable 加载失败！请检查路径！"));
		return;
	}

	FName NeedFindRowName = NAME_Name;
	switch (HitResult.PhysMaterial.Get())
	{
		case SurfaceType1:
			NeedFindRowName = TEXT("Blood");
			break;
		case SurfaceType2:
			NeedFindRowName = TEXT("Blood");
			break;
		case SurfaceType3:
			NeedFindRowName = TEXT("Blood");
			break;
		case SurfaceType4:
			NeedFindRowName = TEXT("Blood");
			break;
		case SurfaceType5:
			NeedFindRowName = TEXT("Concrete");
			break;
		case SurfaceType6:
			NeedFindRowName = TEXT("Metal");
			break;
		/*case SurfaceType7:
			NeedFindRowName = TEXT("Dirt");
			break;#1#
		case SurfaceType8:
			NeedFindRowName = TEXT("Wood");
			break;
		/*case SurfaceType9:
			NeedFindRowName = TEXT("Stone");
			break;#1#
		case SurfaceType10:
			NeedFindRowName = TEXT("Plastic");
			break;
		/*case SurfaceType11:
			NeedFindRowName = TEXT("Tile");
			break;#1#
		case SurfaceType12:
			NeedFindRowName = TEXT("Grass");
			break;
		/*case SurfaceType13:
			NeedFindRowName = TEXT("Glass");
			break;
		case SurfaceType14:
			NeedFindRowName = TEXT("Flesh");
			break;#1#
		default:
			NeedFindRowName = TEXT("Default");
			break;
	}

	FFreeImpactData* ImpactRow = DT->FindRow<FFreeImpactData>(NeedFindRowName, TEXT(""));

	if (!ImpactRow)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit Impact DataTable ImpactRow 为空！请检查数据表是否拥有数据！"));
		return;
	}

	UGameplayStatics::SpawnDecalAtLocation();
}
*/

