﻿
#include "Player/FreeHeroPlayerState.h"
#include "Net/UnrealNetwork.h"


AFreeHeroPlayerState::AFreeHeroPlayerState()
{
	//PrimaryActorTick.bCanEverTick = true;

	TeamNumber = 0;
	NumKills = 0;
	NumDeaths = 0;
	NumBulletsFired = 0;
	NumRocketsFired = 0;
	bQuitter = false;
}

void AFreeHeroPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ThisClass, TeamNumber);
	DOREPLIFETIME(ThisClass, NumKills);
	DOREPLIFETIME(ThisClass, NumDeaths);
}

void AFreeHeroPlayerState::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFreeHeroPlayerState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFreeHeroPlayerState::UpdateTeamColors()
{
	// Todo 更新TeamColors
}

void AFreeHeroPlayerState::OnRep_TeamColor()
{
	UpdateTeamColors();
}

void AFreeHeroPlayerState::AddBulletsFired(int32 NumBullets)
{
	NumBulletsFired += NumBullets;
}
void  AFreeHeroPlayerState::AddRocketsFired(int32 NumRockets)
{
	NumRocketsFired += NumRockets;
}
