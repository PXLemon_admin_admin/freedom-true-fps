﻿
#include "Weapon/FreeFirearmBase.h"
#include "FreeTrueFPS.h"
#include "AI/FreeAIController.h"
#include "Animation/FreeMontageCallbackProxy.h"
#include "Interface/FreeHeroInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Library/FreeHeroLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Player/FreeHeroPlayerState.h"
#include "Player/FreePlayerController.h"
#include "Weapon/FreeProjectile.h"
#include "Data/FirearmBaseData.h"
#include "Component/FreeHeroComponent.h"
#include "Attachment/FreeSightBase.h"
#include "Animation/FreeWeaponAnimInstance.h"
#include "Attachment/FreeMuzzle.h"


AFreeFirearmBase::AFreeFirearmBase()
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);

	WeaponMesh->SetCollisionObjectType(ECC_WorldDynamic);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);

	static ConstructorHelpers::FObjectFinder<UFirearmBaseData>
	const DAFinder(TEXT("FirearmBaseData'/FreeTrueFPS/FreeAsset/Firearm/DA_KA48.DA_KA48'"));
	if (DAFinder.Succeeded())
	{
		InitBaseData(DAFinder.Object);
	}

	SightIndex = 0;
	
	bPendingReload = false;
	bIsReloadingForHandIK = false;
	bFiring = false;
	
	bReplicates = true;
}

void AFreeFirearmBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ThisClass, HeroComponent);
	DOREPLIFETIME(ThisClass, AttachmentBoostData);
	DOREPLIFETIME_CONDITION(ThisClass, WeaponName, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(ThisClass, FireModeIndex, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ThisClass, SightIndex, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ThisClass, bIsReloadingForHandIK, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ThisClass, AmmoInClip, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ThisClass, bPendingReload, COND_SkipOwner);

	/** 配件 */
	DOREPLIFETIME(ThisClass, Muzzle);
	// DOREPLIFETIME(ThisClass, Barrel);
	DOREPLIFETIME(ThisClass, HandGuard);
	DOREPLIFETIME(ThisClass, ForeGrip);
	DOREPLIFETIME(ThisClass, LightLaser);
	DOREPLIFETIME(ThisClass, Magazine);
	DOREPLIFETIME(ThisClass, Stock);
	DOREPLIFETIME(ThisClass, CurrentSight);
	DOREPLIFETIME(ThisClass, ReplicatedSights);
}

void AFreeFirearmBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFreeFirearmBase::OnRep_Owner()
{
	if (const AActor* OwningActor = GetOwner())
	{
		HeroComponent = Cast<UFreeHeroComponent>(OwningActor->GetComponentByClass(UFreeHeroComponent::StaticClass()));
	}
}

void AFreeFirearmBase::InitBaseData(UFirearmBaseData* Data)
{
	if (!Data) return;

	FirearmBaseData = Data;

	WeaponName = FirearmBaseData->WeaponName;
	FireRateRPM = FirearmBaseData->FireRateRPM;
	BaseDamage = FirearmBaseData->BaseDamage;
	WeaponType = FirearmBaseData->WeaponType;
	AmmoType = FirearmBaseData->AmmoType;
	BurstCount = FirearmBaseData->BurstCount;
	FireModes = FirearmBaseData->FireModes;
	AimingOffsetRot = FirearmBaseData->AimingOffsetState.AimingOffsetRot;
	AimingOffsetLoc = FirearmBaseData->AimingOffsetState.AimingOffsetLoc;
	AttachToPawnSocket = FirearmBaseData->AttachToPlayerSocket;

	AimingInterpSpeed = FirearmBaseData->AimingInterpSpeed;

	WeaponMesh->SetSkeletalMesh(FirearmBaseData->WeaponMesh);
	WeaponMesh->SetAnimInstanceClass(FirearmBaseData->WeaponAnimClass);

	MagCapacity = FirearmBaseData->PerMagCapacityMaxAmmo * FirearmBaseData->MagCount;
	PerMagCapacityMaxAmmo = FirearmBaseData->PerMagCapacityMaxAmmo;
	AmmoInClip = FirearmBaseData->AmmoInClip;

	TotalCurrentAmmo = AmmoInClip + MagCapacity;
	
	RecoilPitch.Min = FirearmBaseData->FirearmRecoilState.RecoilPitch.Min;
	RecoilPitch.Max = FirearmBaseData->FirearmRecoilState.RecoilPitch.Max;
	RecoilYaw.Min = FirearmBaseData->FirearmRecoilState.RecoilYaw.Min;
	RecoilYaw.Max = FirearmBaseData->FirearmRecoilState.RecoilYaw.Max;
	
	// AimingRecoilPercentBuff 是一个 0 - 100的数值 转化成百分比做计算 也就是除以100
	// 再将它乘以某个数值的AimingRecoilPercentBuff个百分比
	// 如 AimingRecoilPercentBuff == 30.f, 10.f * (AimingRecoilPercentBuff / 100.f) = 3.f
	
	AimingRecoilBuffPercent = FirearmBaseData->FirearmRecoilState.AimingRecoilPercentBuff;
}

void AFreeFirearmBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFreeFirearmBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	InitBaseData(FirearmBaseData);
}

void AFreeFirearmBase::PostInitProperties()
{
	Super::PostInitProperties();

	// 将RPM转换为 计时器参数时间  公式: 每分钟秒数除以RPM
	AutoFireTimerRate = SECONDOFONEMINUTE / FireRateRPM;

	TotalCurrentAmmo = AmmoInClip + MagCapacity;
	
	CheckFireModes();
}

void AFreeFirearmBase::Server_StartFire_Implementation(const FProjectileTransform& ProjectileTransform/*FVector Origin, FVector_NetQuantizeNormal ShootDir*/)
{
	//StartLineTrace(CameraLoc, CameraRot);

	//FireProjectile(Origin, ShootDir);
	
	Multi_StartFire(ProjectileTransform);
}

void AFreeFirearmBase::Server_StopFire_Implementation()
{
	StopFire();
}

void AFreeFirearmBase::Server_StartReload_Implementation()
{
	bPendingReload = true;
	OnRep_PendingReload();
	
	const float AnimDuration = CalcMontageLength(GetFullBodyReloadMontage(), FirearmBaseData->FirearmAnimState.NoAnimReloadDuration);
	GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &ThisClass::StopReload, AnimDuration, false);
	
	bIsReloadingForHandIK = true;
	Multi_StartReload();
}

void AFreeFirearmBase::Multi_StartReload_Implementation()
{
	if (HeroComponent && !HeroComponent->IsLocallyControlled())
	{
		PlayReloadAnim();
	}
}

float AFreeFirearmBase::CalcMontageLength(const class UAnimMontage* Montage, float Min)
{
	if (!Montage) return 0.f;
	
	const float AnimDuration = Montage->SequenceLength / Montage->RateScale;

	if (Min < 0.1f)
	{
		Min = 0.1f;
	}
	const float Duration = FMath::Max(Min, AnimDuration - 0.1f);
	
	return Duration;
}

void AFreeFirearmBase::StopReload()
{
	bPendingReload = false;
}

void AFreeFirearmBase::SetReloadingForHandIK(FName NotifyName)
{
	if (NotifyName == FirearmBaseData->FirearmAnimState.ReloadMontageNotifyName)
	{
		bIsReloadingForHandIK = false;
	}
}

void AFreeFirearmBase::Multi_StartFire_Implementation(const FProjectileTransform& ProjectileTransform)
{
	if (!HeroComponent->IsLocallyControlled())
	{
		HandleProjectile(ProjectileTransform);
		PlayFireAllEffect();
	}
}

void AFreeFirearmBase::Server_SetFireModeIndex_Implementation(uint8 NewIndex)
{
	FireModeIndex = NewIndex;
	OnRep_FireModeIndex();
}

void AFreeFirearmBase::Server_SetSightIndex_Implementation(uint8 NewIndex)
{
	SightIndex = NewIndex;
	OnRep_SightIndex();
}

void AFreeFirearmBase::Server_SetBoostData_Implementation(const FAttachmentBoostData& BoostData)
{
	AttachmentBoostData = BoostData;
}

void AFreeFirearmBase::OnRep_FireModeIndex()
{
	CurrentFireMode = FireModes[FireModeIndex];
}

void AFreeFirearmBase::OnRep_SightIndex()
{
	CurrentSight = ReplicatedSights[SightIndex];
}

void AFreeFirearmBase::Server_SetBoostOnEquip_Implementation(const class AAttachmentBase* Attachment, bool Add)
{
	SetAttachmentBoostDataOnEquip(Attachment, Add);
}

void AFreeFirearmBase::Server_SetMuzzle_Implementation(class AFreeMuzzle* NewMuzzle)
{
	Muzzle = NewMuzzle;
}

void AFreeFirearmBase::Server_SetBarrel_Implementation(AFreeBarrel* NewBarrel)
{
	// Barrel = NewBarrel;
}

void AFreeFirearmBase::Server_SetHandGuard_Implementation(AFreeHandGuard* NewHandGuard)
{
	HandGuard = NewHandGuard;
}

void AFreeFirearmBase::Server_SetForeGrip_Implementation(AFreeForeGrip* NewForeGrip)
{
	ForeGrip = NewForeGrip;
}

void AFreeFirearmBase::Server_SetLightLaser_Implementation(AFreeLightLaser* NewLightLaser)
{
	LightLaser = NewLightLaser;
}

void AFreeFirearmBase::Server_SetSight_Implementation(AFreeSightBase* NewSight)
{
	ReplicatedSights.Add(NewSight);
}

void AFreeFirearmBase::Server_SetMagazine_Implementation(AFreeMagazine* NewMagazine)
{
	Magazine = NewMagazine;
}

void AFreeFirearmBase::Server_SetStock_Implementation(AFreeStock* NewStock)
{
	Stock = NewStock;
}

void AFreeFirearmBase::OnRep_HeroComponent()
{
	if (HeroComponent)
	{
		OnEnterInventory(HeroComponent);
	}
	else
	{
		OnLeaveInventory();
	}
}

void AFreeFirearmBase::OnRep_PendingReload()
{
	
}

/*void AFreeFirearmBase::OnRep_AttachmentBoostData()
{
	// ToDo OnRep_AttachmentBoostData()
}*/

void AFreeFirearmBase::SimulateWeaponFire()
{
	if (GetLocalRole() == ROLE_Authority) return;

	if (FirearmBaseData->FirearmMuzzleFireFX)
	{
		// ToDO spawn muzzleFX
	}
}

void AFreeFirearmBase::StopSimulatingWeaponFire()
{
	// ToDo StopSimulatingWeaponFire()
}

void AFreeFirearmBase::GiveAmmo(int AddAmount)
{
	if (!HeroComponent) return;

	const int32 MissingAmmo = FMath::Max(0, FirearmBaseData->MaxAmmo - MagCapacity);
	AddAmount = FMath::Max(AddAmount, MissingAmmo);
	MagCapacity += AddAmount;

	TotalCurrentAmmo = AmmoInClip + MagCapacity;

	AFreeAIController* AIBot = Cast<AFreeAIController>(HeroComponent->GetPawnController());
	if (AIBot)
	{
		AIBot->CheckAmmo(this);
	}

	// 如果当前弹夹中的子弹空了则开始换弹
	if (GetAmmoInClip() <= 0 && CheckCanReload() && HeroComponent->GetCurrentGun() == this)
	{
		Reload();
	}
}

void AFreeFirearmBase::UseAmmo()
{
	--AmmoInClip;
	--MagCapacity;
	--TotalCurrentAmmo;

	if (!HeroComponent) return;
	
	const AFreePlayerController* PlayerController = Cast<AFreePlayerController>(HeroComponent->GetPawnController());
	AFreeAIController* AIBot = Cast<AFreeAIController>(HeroComponent->GetPawnController());

	if (AIBot)
	{
		AIBot->CheckAmmo(this);
	}
	else if (PlayerController)
	{
		AFreeHeroPlayerState* PlayerState = Cast<AFreeHeroPlayerState>(PlayerController->PlayerState);
		switch (FirearmBaseData->AmmoType)
		{
			case EFreeAmmoType::Rocket:
				PlayerState->AddRocketsFired(1);
				break;
			// 除了火箭弹都是子弹
			default:
				PlayerState->AddBulletsFired(1);
				break;
		}
	}
}

void AFreeFirearmBase::AttachMeshToPawn()
{
	if (const IFreeHeroInterface* HeroInterface = Cast<IFreeHeroInterface>(GetOwner()))
	{
		USkeletalMeshComponent* PawnMesh = HeroInterface->GetPawnMesh();
		WeaponMesh->AttachToComponent(PawnMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, GetAttachToPawnSocketName());
	}
}

void AFreeFirearmBase::DetachMeshFromPawn()
{
	WeaponMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
}

void AFreeFirearmBase::CheckFireModes()
{
	if (FireModes.Num() <= 0)
	{
		FireModes.Add(EFreeFireMode::Semi);
		/*FireModes.Add(EFreeFireMode::Burst);
		FireModes.Add(EFreeFireMode::FullAuto);*/
		FireModes.Add(EFreeFireMode::Safe);
	}
	FireModeIndex = 0;
	CurrentFireMode = FireModes[FireModeIndex];
}

void AFreeFirearmBase::OnEquip(const AFreeFirearmBase* LastWeapon)
{
	AttachMeshToPawn();
}

/*void AFreeFirearmBase::OnRep_MyPawn()
{
	MyPawn ? OnEnterInventory(MyPawn) : OnLeaveInventory();
}*/

void AFreeFirearmBase::OnUnEquip()
{
	DetachMeshFromPawn();
}

void AFreeFirearmBase::OnEnterInventory(class UFreeHeroComponent* InHeroComponent)
{
	this->HeroComponent = InHeroComponent;
	if (HeroComponent->GetOwnerPawn())
	{
		SetMyPawn(HeroComponent->GetOwnerPawn());
	}
}

void AFreeFirearmBase::OnLeaveInventory()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		this->HeroComponent = nullptr;
		SetMyPawn(nullptr);
	}
}

void AFreeFirearmBase::OnEquipAttachment(const AAttachmentBase* Attachment)
{
	SetAttachmentBoostDataOnEquip(Attachment, true);
	if (!HasAuthority())
	{
		Server_SetBoostData(AttachmentBoostData);
	}
}

void AFreeFirearmBase::OnUnEquipAttachment(const AAttachmentBase* Attachment)
{
	SetAttachmentBoostDataOnEquip(Attachment, false);
	if (!HasAuthority())
	{
		Server_SetBoostData(AttachmentBoostData);
	}
}

void AFreeFirearmBase::SetMuzzle(AFreeMuzzle* NewMuzzle)
{
	Muzzle = NewMuzzle;
	if (!HasAuthority())
	{
		Server_SetMuzzle(NewMuzzle);
	}
}

void AFreeFirearmBase::SetHandGuard(AFreeHandGuard* NewHandGuard)
{
	HandGuard = NewHandGuard;
	if (!HasAuthority())
	{
		Server_SetHandGuard(NewHandGuard);
	}
}

void AFreeFirearmBase::SetForeGrip(AFreeForeGrip* NewForeGrip)
{
	ForeGrip = NewForeGrip;
	if (!HasAuthority())
	{
		Server_SetForeGrip(NewForeGrip);
	}
}

void AFreeFirearmBase::SetLightLaser(AFreeLightLaser* NewLightLaser)
{
	LightLaser = NewLightLaser;
	if (!HasAuthority())
	{
		Server_SetLightLaser(NewLightLaser);
	}
}

void AFreeFirearmBase::SetMagazine(AFreeMagazine* NewMagazine)
{
	Magazine = NewMagazine;
	if (!HasAuthority())
	{
		Server_SetMagazine(NewMagazine);
	}
}

void AFreeFirearmBase::SetStock(AFreeStock* NewStock)
{
	Stock = NewStock;
	if (!HasAuthority())
	{
		Server_SetStock(NewStock);
	}
}

void AFreeFirearmBase::AddSight(AFreeSightBase* NewSight)
{
	ReplicatedSights.Add(NewSight);
	if (!HasAuthority())
	{
		Server_SetSight(NewSight);
	}
}

bool AFreeFirearmBase::CanFire() const
{
	if (bPendingReload) return false;
	if (CurrentFireMode == EFreeFireMode::Safe || !HeroComponent) return false;
	if (WeaponType == EFreeWeaponType::Shotgun || WeaponType == EFreeWeaponType::SniperRifle) return !bFiring;
	return true;
}

void AFreeFirearmBase::StartFireTimer()
{
	GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &ThisClass::StartFire, AutoFireTimerRate, true);
}

FTransform AFreeFirearmBase::GetPawnFPCameraTransform() const
{
	FTransform Result = FTransform::Identity;
	if (const IFreeHeroInterface* HeroInterface = Cast<IFreeHeroInterface>(GetOwner()))
	{
		Result = HeroInterface->GetCameraTransform();
	}
	return Result;
}

void AFreeFirearmBase::StartLineTrace(FVector CameraLoc, FRotator CameraRot)
{
	FHitResult HitResult;
	TArray<AActor*> IgnoredObjects;
	// 忽略武器
	IgnoredObjects.Add(this);
	// 忽略发起伤害的Actor
	IgnoredObjects.Add(GetOwner());

	FVector CameraLocation = CameraLoc;
	const FVector AdjustLoc = UKismetMathLibrary::RandomUnitVectorInEllipticalConeInDegrees(CameraLocation, ShootRandomMaxYawDegrees, ShootRandomMaxPitchDegrees);
	const FVector EndLocation = AdjustLoc + FQuat(CameraRot).GetForwardVector() * (ShootDistanceMeter * 100.f);
	
	// 这里的TraceTypeQuery1 指 Visibility 通道
	const bool HitSuccess = UKismetSystemLibrary::LineTraceSingle(
		GetWorld(), CameraLoc, EndLocation,
		TraceTypeQuery1, true, IgnoredObjects,
		EDrawDebugTrace::Persistent, HitResult, true,
		FLinearColor::Red, FLinearColor::Green, 3.f
	);

	if (HitSuccess)
	{
		UKismetSystemLibrary::PrintString(this,
			FString::Printf(TEXT("---------HitSuccess Hit Actor Name: %s,  BoneName:%s----"),
			*HitResult.GetActor()->GetName(),*HitResult.BoneName.ToString())
		);

		// 是玩家 执行发起伤害逻辑
		if (IFreeHeroInterface* HeroInterface = Cast<IFreeHeroInterface>(HitResult.GetActor()))
		{
			HeroInterface->ApplyDamage(BaseDamage, HitResult.PhysMaterial.Get(),
				HitResult.GetActor(), CameraLocation, HitResult,
				HeroComponent->GetPawnController(),
				this, UDamageType::StaticClass());
		}
	}
	
}

void AFreeFirearmBase::Reload()
{
	if (!CheckCanReload()) return;
	StopFire();

	bPendingReload = true;
	OnRep_PendingReload();

	const float AnimDuration = CalcMontageLength(GetFullBodyReloadMontage(), FirearmBaseData->FirearmAnimState.NoAnimReloadDuration);
	GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &ThisClass::StopReload, AnimDuration, false);

	bIsReloadingForHandIK = true;
	PlayReloadAnim();

	if (HasAuthority())
	{
		Multi_StartReload();
	}
	else
	{
		Server_StartReload();
	}
}

bool AFreeFirearmBase::ApplyProjectileState(FFreeProjectileState& Data) const
{
	if (FirearmBaseData)
	{
		//Data = FirearmBaseData->ProjectileState;
		return true;
	}
	return false;
}

bool AFreeFirearmBase::CheckCanReload() const
{
	return !bPendingReload;
}

bool AFreeFirearmBase::AmmoInClipIsEmpty() const
{
	return AmmoInClip <= 0;
}

UAnimMontage* AFreeFirearmBase::GetFullBodyReloadMontage() const
{
	if (AmmoInClip <= 0)
	{
		return FirearmBaseData->FirearmAnimState.FullBodyReloadEmptyMontage;
	}
	return FirearmBaseData->FirearmAnimState.FullBodyReloadMontage;
}

UAnimMontage* AFreeFirearmBase::GetGunReloadMontage() const
{
	if (AmmoInClip <= 0)
	{
		return FirearmBaseData->FirearmAnimState.FirearmReloadEmptyMontage;
	}
	return FirearmBaseData->FirearmAnimState.FirearmReloadMontage;
}

void AFreeFirearmBase::PlayReloadAnim()
{
	if (const IFreeHeroInterface* HeroInterface = Cast<IFreeHeroInterface>(GetOwner()))
	{
		UAnimMontage* FullBodyReloadMontage = GetFullBodyReloadMontage();
		UAnimMontage* GunReloadMontage = GetGunReloadMontage();
		
		if (FullBodyReloadMontage && HeroInterface->GetPawnMesh())
		{
			if (UFreeMontageCallbackProxy* PlayMontageCallbackProxy =
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(HeroInterface->GetPawnMesh(), FullBodyReloadMontage))
			{
				PlayMontageCallbackProxy->OnNotifyBegin.AddDynamic(this, &ThisClass::SetReloadingForHandIK);
				PlayMontageCallbackProxy->OnCompleted.AddDynamic(this, &ThisClass::ReloadComplete);
				PlayMontageCallbackProxy->OnInterrupted.AddDynamic(this, &ThisClass::ReloadOnInterrupted);
			}
		}
		
		if (GunReloadMontage)
		{
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(WeaponMesh, GunReloadMontage);
		}
	}
}

void AFreeFirearmBase::ReloadComplete(FName NotifyName)
{
	bIsReloadingForHandIK = false;
}

void AFreeFirearmBase::ReloadOnInterrupted(FName NotifyName)
{
	bIsReloadingForHandIK = false;
}

void AFreeFirearmBase::StartFire()
{
	if (!CanFire())
	{
		StopFire();
		return;
	}

	/*FFireProjectileParam ProjectileParam = CalcBeforeFireProjectile();
	
	const APlayerController* PC = Cast<APlayerController>(HeroComponent->GetPawnController());
	if (!PC)
	{
		StopFire();
		return;
	}
	
	FVector ViewLoc;
	FRotator ViewRot;
	PC->GetPlayerViewPoint(ViewLoc, ViewRot);*/

	if (WeaponType == EFreeWeaponType::Shotgun || WeaponType == EFreeWeaponType::SniperRifle)
	{
		const float ShootSpendTime = CalcMontageLength(FirearmBaseData->FirearmAnimState.FullBodyFireMontage,
					FirearmBaseData->FirearmAnimState.NoAnimFireDuration);
		GetWorldTimerManager().SetTimer(TimerHandle_FireAction, this, &ThisClass::FiringActionComplete, ShootSpendTime);
	}

	const FProjectileTransform ProjectileTransform = GetMuzzleProjectileSocketTransform(100.f, 0.f);

	HandleProjectile(ProjectileTransform);

	PlayFireAllEffect();

	//StartLineTrace(ViewLoc, ViewRot);

	if (HasAuthority())
	{
		Multi_StartFire(ProjectileTransform);
	}
	else
	{
		Server_StartFire(ProjectileTransform);
	}

	switch (CurrentFireMode)
	{
		case EFreeFireMode::Burst:
			if (++BurstCounter > BurstCount)
			{
				StopFire();
				BurstCounter = 0;
			}
			else
			{
				StartFireTimer();
			}
			break;
		case EFreeFireMode::FullAuto:
			StartFireTimer();
			break;
		default:
			break;
	}
}

void AFreeFirearmBase::StopFire()
{
	if (GetLocalRole() < ROLE_Authority && HeroComponent && HeroComponent->IsLocallyControlled())
	{
		Server_StopFire();
	}
	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	ResetRecoil();
}

void AFreeFirearmBase::PlayRecoil()
{
	RandomRecoil();
	/*if (const FFirearmRecoilState* RecoilState = GetFirearmRecoilData())
	{
		if (AController* UseController = HeroComponent->GetPawnController())
		{
			const UCurveFloat* RecoilPitchCurve = RecoilState->ControlRotRecoilPitchCurve;
			const UCurveFloat* RecoilYawCurve = RecoilState->ControlRotRecoilYawCurve;
			RecoilCoordPerShoot += 0.1; // 每次射击 向曲线后面位移 0.1秒
			if (RecoilPitchCurve)	
			{
				NewPitchRecoilAmount = RecoilPitchCurve->GetFloatValue(RecoilCoordPerShoot);
			}
			if (RecoilYawCurve)
			{
				NewYawRecoilAmount = RecoilYawCurve->GetFloatValue(RecoilCoordPerShoot);
			}
	
			PitchRecoilAmount = NewPitchRecoilAmount - OldPitchRecoilAmount;
			YawRecoilAmount = NewYawRecoilAmount - OldYawRecoilAmount;

			// 瞄准时减少后坐力
			if (HeroComponent->GetAiming())
			{
				const float Buff = 0.01f + AimingRecoilBuffPercent;
				PitchRecoilAmount = PitchRecoilAmount - (PitchRecoilAmount * Buff);
				YawRecoilAmount = YawRecoilAmount - (YawRecoilAmount * Buff);
			}

			const FRotator ControlRotator = UseController->GetControlRotation();

			FRotator InterpControlRot = FRotator(
				ControlRotator.Pitch + PitchRecoilAmount,
				ControlRotator.Yaw + YawRecoilAmount,
				ControlRotator.Roll);

			FRotator FinalInterpRot = UKismetMathLibrary::RInterpTo(
				ControlRotator, InterpControlRot,
				GetWorld()->DeltaTimeSeconds, RecoilInterpSpeed);
			
			// 应用后坐力
			UseController->SetControlRotation(FinalInterpRot);

			OldPitchRecoilAmount = NewPitchRecoilAmount;
			OldYawRecoilAmount = NewYawRecoilAmount;
		}
	}*/
}

void AFreeFirearmBase::RandomRecoil()
{
	AController* UseController = HeroComponent->GetPawnController();
	if (!UseController) return;

	float UseRecoilPitch = FMath::FRandRange(RandomRecoilPitchMin, RandomRecoilPitchMax);
	float UseRecoilYaw = FMath::FRandRange(RandomRecoilYawMin, RandomRecoilYawMax);

	// 瞄准时减少后坐力
	if (HeroComponent->GetAiming())
	{
		// 基础后坐力 +
		// GetFormatAimingRecoilBuffPercent() 应为正数
		const float Buff = BaseRecoilBuffPercent + GetFormatAimingRecoilBuffPercent();
		UseRecoilPitch = UseRecoilPitch - UseRecoilPitch * Buff;
		UseRecoilYaw = UseRecoilYaw - UseRecoilYaw * Buff;
	}
	// 应用配件的后坐力数据
	UseRecoilPitch += GetFormatBoostDataRecoilPercent();
	UseRecoilYaw += GetFormatBoostDataRecoilPercent();

	// 限制后坐力最小为0, 0表示无后坐力
	UseRecoilPitch = FMath::Clamp(UseRecoilPitch, 0.0f, UseRecoilPitch);
	UseRecoilYaw = FMath::Clamp(UseRecoilYaw, 0.0f, UseRecoilYaw);

	const FRotator ControlRot = UseController->GetControlRotation();
	const FRotator ToInterpRot = FRotator(
				ControlRot.Pitch + UseRecoilPitch,
				ControlRot.Yaw + UseRecoilYaw,
				ControlRot.Roll);
	const FRotator FinalRot = FMath::RInterpTo(ControlRot, ToInterpRot, GetWorld()->DeltaTimeSeconds, RandomRecoilInterpSpeed);

	UseController->SetControlRotation(FinalRot);
}

void AFreeFirearmBase::PlayFireAnim()
{
	if (FirearmBaseData->FirearmAnimState.bEnableFireMontage)
	{
		if (UAnimMontage* FireMontage = FirearmBaseData->FirearmAnimState.FullBodyFireMontage)
		{
			if (USkeletalMeshComponent* PawnMesh = HeroComponent->GetPawnMesh())
			{
				UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(PawnMesh, FireMontage);
			}
		}
		else
		{
			HeroComponent->ExecRecoil(1.0);
		}
	}
	else
	{
		HeroComponent->ExecRecoil(1.0);
	}
	
	if (WeaponMesh)
	{
		if (UAnimMontage* FirearmFire = FirearmBaseData->FirearmAnimState.FirearmFireMontage)
		{
			UFreeMontageCallbackProxy::CreateProxyObjectForPlayMontage(WeaponMesh, FirearmFire);
		}
	}
}

void AFreeFirearmBase::PlayFireFX()
{
	if (Muzzle)
	{
		UParticleSystem* UseFX = Muzzle->GetIsSilencer() ? FirearmBaseData->FirearmMuzzleFireFXSuppressed : FirearmBaseData->FirearmMuzzleFireFX;
		UGameplayStatics::SpawnEmitterAttached(UseFX, Muzzle->GetAttachmentMesh(), Muzzle->GetMuzzleSocketName());
		return;
	}
	
	UGameplayStatics::SpawnEmitterAttached(FirearmBaseData->FirearmMuzzleFireFX, WeaponMesh, FirearmBaseData->FirearmSocket.MuzzleFXSocket);
}

void AFreeFirearmBase::PlayFireSound()
{
	if (Muzzle)
	{
		USoundBase* UseSound = Muzzle->GetIsSilencer() ? FirearmBaseData->FireSoundSuppressed : FirearmBaseData->FireSound;
		UGameplayStatics::SpawnSoundAtLocation(this, UseSound, Muzzle->GetEffectSocketLocation());
		return;
	}
	
	UGameplayStatics::SpawnSoundAtLocation(this, FirearmBaseData->FireSound, GetMuzzleLocation());
}

void AFreeFirearmBase::PlayFireCameraShake(bool bEnableCameraShake)
{
	if (!bEnableCameraShake) return;
	
	if (const TSubclassOf<UCameraShakeBase> FireShake = FirearmBaseData->FirearmRecoilState.FireCameraShake)
	{
		if (const APawn* UsePawn = HeroComponent->GetOwnerPawn())
		{
			if (APlayerController* PC = UsePawn->GetController<APlayerController>())
			{
				PC->ClientStartCameraShake(FireShake);
			}
		}
	}
}

void AFreeFirearmBase::PlayFireAllEffect()
{
	if (!FirearmBaseData || !HeroComponent) return;
	
	PlayFireAnim();
	PlayRecoil();
	PlayFireSound();
	PlayFireFX();
	PlayFireCameraShake();
}

void AFreeFirearmBase::SetAttachmentBoostDataOnEquip(const AAttachmentBase* Attachment, const bool Add)
{
	if (!Attachment) return;

	const FAttachmentBoostData& NewAttachmentBoostData = Attachment->GetAttachmentBoostDataRef();
	
	if (Add)
	{
		UFreeHeroLibrary::CalcAttachmentBoostDataAdd(AttachmentBoostData, NewAttachmentBoostData);
		return;
	}
	
	UFreeHeroLibrary::CalcAttachmentBoostDataSubtract(AttachmentBoostData, NewAttachmentBoostData);
}

FVector AFreeFirearmBase::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	if (!HeroComponent) return FVector::ZeroVector;

	const APawn* MyPawn = HeroComponent->GetOwnerPawn();
	
	AFreePlayerController* PC = MyPawn ? Cast<AFreePlayerController>(MyPawn->Controller) : nullptr;
	AFreeAIController* AIPC = MyPawn ? Cast<AFreeAIController>(MyPawn->Controller) : nullptr;
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// 使用玩家相机
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// 调整追踪 以便在相机和角色之间没有任何阻挡射线 并计算调整后起点的距离
		OutStartTrace = OutStartTrace + AimDir * (GetInstigator()->GetActorLocation() - OutStartTrace | AimDir);
	}
	else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}

	return OutStartTrace;
}

FVector AFreeFirearmBase::GetAdjustedAim() const
{
	const AFreePlayerController* PlayerController = GetInstigatorController<AFreePlayerController>();
	FVector FinalAim = FVector::ZeroVector;

	// 如果我们有一个玩家控制器 使用它来进行瞄准
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (GetInstigator())
	{
		// 看看我们是否有一个AI控制器 - 如果有的话 我们将想要从那里获取目标
		const AFreeAIController* AIController = HeroComponent ? Cast<AFreeAIController>(HeroComponent->GetPawnController()) : nullptr;
		if(AIController != nullptr )
		{
			FinalAim = AIController->GetControlRotation().Vector();
		}
		else
		{
			FinalAim = GetInstigator()->GetBaseAimRotation().Vector();
		}
	}

	return FinalAim;
}

FVector AFreeFirearmBase::GetMuzzleLocation() const
{
	return WeaponMesh->GetSocketLocation(FirearmBaseData->FirearmSocket.MuzzleFXSocket);
}

FVector AFreeFirearmBase::GetMuzzleDirection() const
{
	return WeaponMesh->GetSocketRotation(FirearmBaseData->FirearmSocket.MuzzleFXSocket).Vector();
}

FHitResult AFreeFirearmBase::WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const
{
	// 执行跟踪以检索命中信息

	// SCENE_QUERY_STAT是一种用于性能分析和优化的宏 用于统计场景查询的执行时间 它可以用于跟踪和识别代码中的性能瓶颈
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(WeaponTrace), true, GetInstigator());
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, COLLISION_WEAPON, TraceParams);

	return Hit;
}

FFireProjectileParam AFreeFirearmBase::CalcBeforeFireProjectile()
{
	UE_LOG(LogTemp, Warning, TEXT(" 进入 CalcBeforeFireProjectile()！！！！！！！"))
	/*FTransform SightTransform = FTransform::Identity;
	if (HeroComponent->GetAiming())
	{
		SightTransform = GetSightAimTransform();
	}
	else
	{
		SightTransform = GetPawnFPCameraTransform();
	}

	FTransform MuzzleTransform = GetMuzzleTransform();

	const float FinalShootMeter = ShootDistanceMeter * 100.f;
	FVector EndLoc = SightTransform.GetLocation() + SightTransform.GetRotation().Vector() * FinalShootMeter;
	FRotator LookAtRot = UKismetMathLibrary::FindLookAtRotation(MuzzleTransform.GetLocation(), EndLoc);*/

	FVector ShootDir = GetAdjustedAim();
	FVector Origin = GetMuzzleLocation();

	// 追踪从相机到准星下方以检查下方的内容
	const float ProjectileAdjustRange = 10000.0f;
	const FVector StartTrace = GetCameraDamageStartLocation(ShootDir);
	const FVector EndTrace = StartTrace + ShootDir * ProjectileAdjustRange;
	
	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	
	// 并调整方向以击中那个角色
	if (Impact.bBlockingHit)
	{
		const FVector AdjustedDir = (Impact.ImpactPoint - Origin).GetSafeNormal();
		bool bWeaponPenetration = false;

		const float DirectionDot = FVector::DotProduct(AdjustedDir, ShootDir);
		if (DirectionDot < 0.0f)
		{
			// 射击向后 = 武器穿透
			bWeaponPenetration = true;
		}
		else if (DirectionDot < 0.5f)
		{
			/**
			 * 如果角度差足够大，请检查武器穿透。沿着武器网格进行射线投射，以检查是否有阻塞的命中。
			 */
			FVector MuzzleStartTrace = Origin - GetMuzzleDirection() * 150.0f;
			FVector MuzzleEndTrace = Origin;
			FHitResult MuzzleImpact = WeaponTrace(MuzzleStartTrace, MuzzleEndTrace);

			if (MuzzleImpact.bBlockingHit)
			{
				bWeaponPenetration = true;
			}
		}

		if (bWeaponPenetration)
		{
			// 在准星位置生成/生成
			Origin = Impact.ImpactPoint - ShootDir * 10.0f;
		}
		else
		{
			// 调整方向以命中
			ShootDir = AdjustedDir;
		}
	}

	return FFireProjectileParam{Origin, ShootDir};
}

void AFreeFirearmBase::FireProjectile(FVector Origin, FVector_NetQuantizeNormal ShootDir)
{
	const FTransform SpawnTM(ShootDir.Rotation(), Origin);
	
	AFreeProjectile* Projectile = Cast<AFreeProjectile>(
		UGameplayStatics::BeginDeferredActorSpawnFromClass(this, FirearmBaseData->ProjectileClass, SpawnTM)
	);
	if (Projectile)
	{
		Projectile->SetInstigator(GetInstigator());
		Projectile->SetOwner(this);
		Projectile->SetBoostData(AttachmentBoostData);
		Projectile->InitVelocity(ShootDir);

		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTM);
	}
}

FProjectileTransform AFreeFirearmBase::GetMuzzleProjectileSocketTransform(float Meters, float MOA)
{
	float FinalMeters = Meters * 100.f;
	if (FinalMeters > 10000.f)
	{
		FinalMeters = 10000.f;
	}
	else if (FinalMeters < 5000.f)
	{
		FinalMeters = 5000.f;
	}

	FTransform AimPointTransform;
	if (HeroComponent->GetAiming())
	{
		AimPointTransform = GetSightAimTransform();
	}
	else
	{
		APlayerController* PC = Cast<APlayerController>(HeroComponent->GetPawnController());
		FVector ViewVec;
		FRotator ViewRot;
		if (PC)
		{
			PC->GetPlayerViewPoint(ViewVec, ViewRot);
			AimPointTransform = FTransform(ViewRot, ViewVec, FVector::OneVector);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT(" PlayerController Is nullptr !!!!!! in Firearm class ,func :GetMuzzleProjectileSocketTransform "))
		}
	}

	//const FTransform SightTransform = GetSightAimTransform();
	FTransform MuzzleTransform = GetMuzzleTransform();
	FRotator MuzzleRotation = UFreeHeroLibrary::GetEstimatedMuzzleToScopeZero(MuzzleTransform, AimPointTransform, FinalMeters);
	//MuzzleRotation = UFreeHeroLibrary::SetMuzzleMOA(MuzzleRotation, MOA);
	
	MuzzleTransform.SetRotation(MuzzleRotation.Quaternion());
	return MuzzleTransform;
}

void AFreeFirearmBase::HandleAmmo()
{
	if (GetAmmoInClip() > 0)
	{
		UseAmmo();
	}
}

void AFreeFirearmBase::HandleProjectile(const FProjectileTransform& ProjectileTransform)
{
	const FTransform FinalTransform = FProjectileTransform::GetTransformFromProjectile(ProjectileTransform);
	
	AFreeProjectile* Projectile = Cast<AFreeProjectile>(
		UGameplayStatics::BeginDeferredActorSpawnFromClass(this, FirearmBaseData->ProjectileClass, FinalTransform)
	);
	if (Projectile)
	{
		Projectile->SetInstigator(GetInstigator());
		Projectile->SetOwner(this);
		Projectile->SetBoostData(AttachmentBoostData);
		/*FVector ShootDir{8000.f,0.f,0.f};
		Projectile->InitVelocity(ShootDir);*/

		UGameplayStatics::FinishSpawningActor(Projectile, FinalTransform);
	}

	HandleAmmo();
}

void AFreeFirearmBase::FiringActionComplete()
{
	bFiring = false;
}

FAttachmentBoostData& AFreeFirearmBase::GetPartBoostData()
{
	return AttachmentBoostData;
}

void AFreeFirearmBase::ResetRecoil()
{
	NewPitchRecoilAmount = 0.f;
	OldPitchRecoilAmount = 0.f;
	PitchRecoilAmount = 0.f;
	RecoilCoordPerShoot = 0.f;
	NewYawRecoilAmount = 0.f;
	OldYawRecoilAmount = 0.f;
	YawRecoilAmount = 0.f;
}

void AFreeFirearmBase::SwitchFireMode()
{
	if (FireModes.Num() <= 0) return;

	if (++FireModeIndex >= FireModes.Num())
	{
		FireModeIndex = 0;
	}
	CurrentFireMode = FireModes[FireModeIndex];
	
	if (!HasAuthority())
	{
		Server_SetFireModeIndex(FireModeIndex);
	}
}

void AFreeFirearmBase::SetMyPawn(APawn* NewPawn)
{
	if (GetOwner() != NewPawn)
	{
		SetInstigator(NewPawn);
		// net owner for RPC calls
		SetOwner(NewPawn);
	}
}

void AFreeFirearmBase::SetHeroComponent(UFreeHeroComponent* InHeroComponent)
{
	this->HeroComponent = InHeroComponent;
	if (HeroComponent->GetOwnerPawn())
	{
		SetMyPawn(HeroComponent->GetOwnerPawn());
	}
}

void AFreeFirearmBase::SwitchSight()
{
	if (ReplicatedSights.Num() <= 0) return;
	
	if (++SightIndex >= ReplicatedSights.Num())
	{
		SightIndex = 0;
	}
	CurrentSight = ReplicatedSights[SightIndex];
	
	if (!HasAuthority())
	{
		Server_SetSightIndex(SightIndex);
	}
	/*if (Sights.Num() <= 0) return;
	
	if (++SightIndex >= Sights.Num())
	{
		SightIndex = 0;
	}
	CurrentSight = Sights[SightIndex];
	
	if (!HasAuthority())
	{
		Server_SetSightIndex(SightIndex);
	}*/
}

bool AFreeFirearmBase::GetIsGun() const
{
	return true;
}

void AFreeFirearmBase::ZoomSight(bool bZoomIn)
{
	//ISightInterface::ZoomSight(bZoomIn);
}

float AFreeFirearmBase::GetZoomSensitivity() const
{
	return 1.0f;
}

float AFreeFirearmBase::GetSightZoomSensitivity() const
{
	return CurrentSight ? CurrentSight->GetZoomSensitivity() : 1.0f;
}

FTransform AFreeFirearmBase::GetSightAimTransform()
{
	if (CurrentSight)
	{
		return CurrentSight->GetSightAimTransform();
	}
	
	if (WeaponMesh->DoesSocketExist(FirearmBaseData->FirearmSocket.AimSocket))
	{
		return WeaponMesh->GetSocketTransform(FirearmBaseData->FirearmSocket.AimSocket);
	}
	
	return WeaponMesh->GetComponentTransform();
}

FName AFreeFirearmBase::GetAttachToPawnSocketName() const
{
	return AttachToPawnSocket;
}

FName AFreeFirearmBase::GetReloadMontageNotifyName() const
{
	return FirearmBaseData->FirearmAnimState.ReloadMontageNotifyName;
}

FORCEINLINE AFreeSightBase* AFreeFirearmBase::GetCurrentSight() const
{
	return CurrentSight;
}

FFirearmAnimState* AFreeFirearmBase::GetFirearmAnimData() const
{
	return &FirearmBaseData->FirearmAnimState;
}

FFirearmRecoilState* AFreeFirearmBase::GetFirearmRecoilData() const
{
	return &FirearmBaseData->FirearmRecoilState;
}

FFirearmAnimState& AFreeFirearmBase::GetFirearmAnimDataRef() const
{
	return FirearmBaseData->FirearmAnimState;
}

FFirearmRecoilState& AFreeFirearmBase::GetFirearmRecoilDataRef() const
{
	return FirearmBaseData->FirearmRecoilState;
}

FFreeGunSwayData& AFreeFirearmBase::GetFirearmSwayDataRef() const
{
	return FirearmBaseData->GunSwayData;
}

FFreeGunSwayData* AFreeFirearmBase::GetFirearmSwayData() const
{
	return &FirearmBaseData->GunSwayData;
}

FORCEINLINE USkeletalMeshComponent* AFreeFirearmBase::GetFirearmMesh() const
{
	return WeaponMesh;
}

FORCEINLINE EFreeFireMode AFreeFirearmBase::GetCurrentFireMode() const
{
	return CurrentFireMode;
}

UFreeHeroComponent* AFreeFirearmBase::GetHeroComponent()
{
	if (!HeroComponent)
	{
		if (IsValid(GetOwner()))
		{
			HeroComponent = Cast<UFreeHeroComponent>(GetOwner()->GetComponentByClass(UFreeHeroComponent::StaticClass()));
			OnRep_HeroComponent();
		}
	}
	return HeroComponent;
}

FORCEINLINE bool AFreeFirearmBase::GetIsReloadingForHandIK() const
{
	return bIsReloadingForHandIK;
}

FORCEINLINE int32 AFreeFirearmBase::GetMaxAmmo() const
{
	return FirearmBaseData->MaxAmmo;
}

FORCEINLINE int32 AFreeFirearmBase::GetAmmoInClip() const
{
	return AmmoInClip;
}

FORCEINLINE int32 AFreeFirearmBase::GetTotalCurrentAmmo() const
{
	return TotalCurrentAmmo;
}

FORCEINLINE EFreeAmmoType AFreeFirearmBase::GetAmmoType() const
{
	return AmmoType;
}

FORCEINLINE FTransform AFreeFirearmBase::GetMuzzleTransform() const
{
	return WeaponMesh->GetSocketTransform(FirearmBaseData->FirearmSocket.MuzzleFXSocket);
}

FORCEINLINE FRotator AFreeFirearmBase::GetAimingOffsetRot() const
{
	return AimingOffsetRot;
}

FORCEINLINE FVector AFreeFirearmBase::GetAimingOffsetLoc() const
{
	return AimingOffsetLoc;
}

FORCEINLINE float AFreeFirearmBase::GetAimingInterpSpeed() const
{
	return AimingInterpSpeed;
}

FORCEINLINE float AFreeFirearmBase::GetFormatAimingRecoilBuffPercent() const
{
	return AimingRecoilBuffPercent / 100.0f;
}

FORCEINLINE float AFreeFirearmBase::GetFormatBoostDataRecoilPercent() const
{
	return AttachmentBoostData.RecoilBoostPercent / 100.0f;
}


