﻿
#include "Weapon/FreeProjectile.h"
#include "FreeTrueFPS.h"
#include "Components/AudioComponent.h"
#include "Components/SphereComponent.h"
#include "Data/AttachmentBoostData.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Interface/FreeHeroInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
//#include "Weapon/FreeFirearmBase.h"

AFreeProjectile::AFreeProjectile()
{
	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
	ParticleComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleComp"));
	
	/*static ConstructorHelpers::FObjectFinder<UFreeProjectileData>
	const DataFinder(TEXT("FirearmBaseData'/FreeTrueFPS/FreeAsset/Firearm/DA_KA48.DA_KA48'"));
	if (DataFinder.Succeeded())
	{
		ProjectileData = DataFinder.Object;
		if (ProjectileData)
		{
			CollisionComp->InitSphereRadius(ProjectileData->InitSphereRadius);
			MovementComp->InitialSpeed = ProjectileData->InitSpeed;
			MovementComp->MaxSpeed = ProjectileData->MaxSpeed;
			MovementComp->ProjectileGravityScale = ProjectileData->ProjectileGravityScale;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AFreeProjectile DataAsset 查找失败！！！！！！！"))
	}*/
	
	CollisionComp->InitSphereRadius(ProjectileData.InitSphereRadius);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Destructible, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Vehicle, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Block);
	RootComponent = CollisionComp;

	ParticleComp->bAutoActivate = false;
	ParticleComp->bAutoDestroy = false;
	ParticleComp->SetupAttachment(RootComponent);

	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->bRotationFollowsVelocity = true;
	
	MovementComp->InitialSpeed = ProjectileData.InitSpeed;
	MovementComp->MaxSpeed = ProjectileData.MaxSpeed;
	MovementComp->ProjectileGravityScale = ProjectileData.ProjectileGravityScale;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	SetReplicatingMovement(true);
}

void AFreeProjectile::BeginPlay()
{
	Super::BeginPlay();

	LastPosition = GetActorLocation();
}

void AFreeProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult HitResult;
	FCollisionQueryParams Parameters;
	Parameters.AddIgnoredActor(this);
	Parameters.AddIgnoredActor(GetOwner());
	Parameters.AddIgnoredActor(GetOwner()->GetOwner());
	Parameters.bReturnPhysicalMaterial = true;

	if (GetWorld()->LineTraceSingleByChannel(OUT HitResult, LastPosition, GetActorLocation(),
		CollisionChannel, Parameters, FCollisionResponseParams::DefaultResponseParam))
	{
		DoImpact(HitResult);
		//Destroy();
	}
	LastPosition = GetActorLocation();
	MovementComp->Velocity *= MovePerTick;
}

void AFreeProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//MovementComp->OnProjectileStop.AddDynamic(this, &ThisClass::OnImpact);
	CollisionComp->MoveIgnoreActors.Add(GetInstigator());
	
	SetLifeSpan(ProjectileData.ProjectileLife);

	MyController = GetInstigatorController();
	
	/*const AFreeFirearmBase* Gun = Cast<AFreeFirearmBase>(GetOwner());
	if (Gun && !Gun->ApplyProjectileState(ProjectileData))
	{
		 
	}*/
}

void AFreeProjectile::DisableAndDestroy()
{
	UAudioComponent* ProjAudioComp = FindComponentByClass<UAudioComponent>();
	if (ProjAudioComp && ProjAudioComp->IsPlaying())
	{
		ProjAudioComp->FadeOut(0.1f, 0.f);
	}

	MovementComp->StopMovementImmediately();

	// give clients some time to show explosion
	SetLifeSpan(2.0f);
}

void AFreeProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	Super::PostNetReceiveVelocity(NewVelocity);
}

void AFreeProjectile::InitVelocity(FVector& ShootDirection)
{
	if (MovementComp)
	{
		MovementComp->Velocity = ShootDirection * MovementComp->InitialSpeed;
	}
}

/*void AFreeProjectile::MaxSpeedBoost(float SpeedBoost)
{
	if (MovementComp)
	{
		MovementComp->MaxSpeed += SpeedBoost;
	}
}*/

void AFreeProjectile::DoImpact(const FHitResult& HitResult)
{
	if (IFreeHeroInterface* HeroInterface = Cast<IFreeHeroInterface>(HitResult.GetActor()))
	{
		if (HeroInterface->GetIsPlayer())
		{
			FVector CameraLocation = HeroInterface->GetCameraTransform().GetLocation();
			// 发起伤害
			HeroInterface->ApplyDamage(ProjectileData.Damage, HitResult.PhysMaterial.Get(),
				HitResult.GetActor(), CameraLocation, HitResult, MyController.Get(),
				this, ProjectileData.DamageType);
		}
	}

	// 生成Hit效果
	DoEnvImpact(HitResult);
}

FORCEINLINE void AFreeProjectile::DoSetSpeed()
{
	MovementComp->InitialSpeed += ProjectileData.InitSpeed;
	MovementComp->MaxSpeed += ProjectileData.MaxSpeed;
}

void AFreeProjectile::OnImpact(const FHitResult& HitResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Projectile OnImpact Enter"));
	if (GetLocalRole() == ROLE_Authority)
	{
		UE_LOG(LogTemp, Warning, TEXT("Projectile OnImpact GetLocalRole() == ROLE_Authority"));
		// ToDo Projectile OnImpact
		DoImpact(HitResult);
		//DisableAndDestroy();
	}
}

void AFreeProjectile::SetBoostData(const FAttachmentBoostData& BoostData)
{
	ProjectileData.Damage += BoostData.DamageBoost;
	ProjectileData.InitSpeed += BoostData.ProjectileInitSpeedBoost;
	ProjectileData.MaxSpeed += BoostData.ProjectileMaxSpeedBoost;
	// 更新速度
	DoSetSpeed();
}
