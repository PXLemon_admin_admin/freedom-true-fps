﻿
#include "Widget/OverHeadWidget.h"
#include "Components/TextBlock.h"

void UOverHeadWidget::SetDisplayText(const FString& TextToDisplay)
{
	if (DisplayText)
	{
		DisplayText->SetText(FText::FromString(TextToDisplay));
	}
}

void UOverHeadWidget::ShowPlayerNetRoleText(const APawn* const InPawn)
{
	if (!InPawn) return;

	const ENetRole LocalRole = InPawn->GetLocalRole();
	const ENetRole RemoteRole = InPawn->GetRemoteRole();
	FString Role;
	FString LocalRoleStr;
	switch (RemoteRole)
	{
		case ENetRole::ROLE_Authority:
			Role = FString("ROLE_Authority");
			break;
		case ENetRole::ROLE_None:
			Role = FString("ROLE_None");
			break;
		case ENetRole::ROLE_AutonomousProxy:
			Role = FString("ROLE_AutonomousProxy");
			break;
		case ENetRole::ROLE_SimulatedProxy:
			Role = FString("ROLE_SimulatedProxy");
			break;
		case ENetRole::ROLE_MAX:
			Role = FString("ROLE_MAX");
			break;
		default:
			Role = FString("Default");
	}
	switch (LocalRole)
	{
		case ENetRole::ROLE_Authority:
			LocalRoleStr = FString("ROLE_Authority");
			break;
		case ENetRole::ROLE_None:
			LocalRoleStr = FString("ROLE_None");
			break;
		case ENetRole::ROLE_AutonomousProxy:
			LocalRoleStr = FString("ROLE_AutonomousProxy");
			break;
		case ENetRole::ROLE_SimulatedProxy:
			LocalRoleStr = FString("ROLE_SimulatedProxy");
			break;
		case ENetRole::ROLE_MAX:
			LocalRoleStr = FString("ROLE_MAX");
			break;
		default:
			LocalRoleStr = FString("Default");
	}

	FString LocalRoleString = FString::Printf(TEXT("Local Role: %s, -- Remote Role: %s"), *LocalRoleStr, *Role);
	SetDisplayText(Role);
}
