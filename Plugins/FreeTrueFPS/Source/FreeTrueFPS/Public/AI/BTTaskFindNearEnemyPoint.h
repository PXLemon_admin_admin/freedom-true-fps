﻿
#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTaskFindNearEnemyPoint.generated.h"

/**
 * 尝试在当前敌人附近找到位置的Bot AI任务
 */
UCLASS()
class FREETRUEFPS_API UBTTaskFindNearEnemyPoint : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
