﻿
#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "FreeAIController.generated.h"

class AFreeHeroCharacter;
class UBehaviorTreeComponent;
class UBlackboardComponent;

UCLASS()
class FREETRUEFPS_API AFreeAIController : public AAIController
{
	GENERATED_BODY()

public:
	AFreeAIController();
	
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Transient, Category = "FreeAI", meta = (AllowPrivateAccess = true))
	UBlackboardComponent* BlackboardComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Transient, Category = "FreeAI", meta = (AllowPrivateAccess = true))
	UBehaviorTreeComponent* BehaviorTreeComp;
	
public:
	// Begin AController interface
	virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override;
	virtual void BeginInactiveState() override;

protected:
	virtual void OnPossess(class APawn* InPawn) override;
	virtual void OnUnPossess() override;
	// End APlayerController interface

public:
	void Respawn();

	void CheckAmmo(const class AFreeFirearmBase* CurrentGun);

	void SetEnemy(APawn* InPawn);

	AFreeHeroCharacter* GetEnemy() const;

	/**
	 * 如果能够看到当前的敌人 开始向他们开火
	 */
	UFUNCTION(BlueprintCallable, Category = "FreeAI | Behavior")
	void ShootEnemy();

	// Begin AAIController interface
	/** Update direction AI is looking based on FocalPoint */
	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;
	// End AAIController interface
	
protected:

	int32 EnemyKeyID;
	int32 NeedAmmoKeyID;
	
	FTimerHandle TimerHandle_Respawn;

public:
	UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	UBehaviorTreeComponent* GetBehaviorTreeComp() const { return BehaviorTreeComp; }
	
};
