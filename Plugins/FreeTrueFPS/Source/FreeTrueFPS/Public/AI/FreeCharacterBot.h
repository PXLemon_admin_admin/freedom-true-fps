﻿
#pragma once

#include "CoreMinimal.h"
#include "Character/FreeHeroCharacter.h"
#include "FreeCharacterBot.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeCharacterBot : public AFreeHeroCharacter
{
	GENERATED_BODY()

public:
	AFreeCharacterBot();
	
	UPROPERTY(EditAnywhere, Category = "FreeBot | Behavior")
	class UBehaviorTree* BotBehavior;

	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime) override;

	virtual bool GetIsPlayer() const override { return false; }
	
};
