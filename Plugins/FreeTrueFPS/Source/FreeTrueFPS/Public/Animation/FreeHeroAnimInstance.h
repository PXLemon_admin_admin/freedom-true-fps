﻿
#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "FreeHeroAnimInstanceProxy.h"
#include "FreeHeroAnimInstance.generated.h"

class AFreeFirearmBase;
class UFreeHeroComponent;
class AFreeHeroCharacter;
/**
 * 
 */
UCLASS()
class FREETRUEFPS_API UFreeHeroAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UFreeHeroAnimInstance();
	virtual void NativeBeginPlay() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	
private:

	///////////////////////////////////////////////////
	// Thread Safe Proxy
	///////////////////////////////////////////////////

	FFreeHeroAnimInstanceProxy Proxy;

	virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override { return &Proxy; }

	virtual void DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy) override {}

	friend FFreeHeroAnimInstanceProxy;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Ref")
	class AFreeCharacterBase* Character;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Ref")
	UFreeHeroComponent* MyHeroComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Ref")
	AFreeFirearmBase* Gun;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	float Direction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	bool bIsInAir;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	float CurrentAcceleration;

	/**
	 * 侧身角度
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	float LeanAngle;

	/**
	 * 每根骨骼侧身角度(侧身涉及多个骨骼 其中单个骨骼的侧身角度, 但不包括head骨骼)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	FRotator LeanAnglePerBone;

	/**
	 * head侧身角度
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	FRotator LeanAngleForHead;

	/**
	 * 上半身Pitch每根骨骼的角度(当向上/下看时)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	FRotator PitchPerBone;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Locomotion")
	bool bIsReloadingForHandIK;
	
	FRotator SwayInterpRotation;
	UPROPERTY(BlueprintReadOnly, Category = "Freedom | Sway")
	FVector SwayLocation;
	UPROPERTY(BlueprintReadOnly, Category = "Freedom | Sway")
	FRotator SwayRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Sway")
	float SwayRollMultiplier{-10.f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Sway")
	float SwayPitchMultiplier{6.f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Sway")
	float SwayYawMultiplier{5.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Sway")
	float SwayInterpSpeed{5.0f};

#pragma region ProceduralRecoil
protected:	
	float RecoilStartTime;
	float RecoilMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Recoil")
	FTransform FinalRecoilTransform;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Recoil")
	bool bInterpRecoil{false};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Recoil")
	FRotator RecoilRotation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Freedom | Recoil")
	FVector RecoilLocation;

#pragma endregion 
protected:
	void RefreshLocomotionState(float DeltaSeconds);

	void RefreshSpinePitch(float DeltaSeconds);

	void RefreshLeanAngle(float DeltaSeconds);

	void RefreshRecoil(float DeltaSeconds);
	void RunRecoil(float DeltaSeconds);
	void RefreshRecoilToZero(float DeltaSeconds);

	void RefreshSway(float DeltaSeconds);
public:
	void DoRecoil(float Multiplier = 1.0f);
	void DoRecoil(bool bUseAxisMultiplier);
	
	void SetAiming(bool bNewAiming);
	void SetReloading(bool bNewReloading);

#pragma region Procedural ADS
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Aiming")
	FRotator AimingOffsetRot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Aiming")
	FVector AimingOffsetLoc;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	FVector RelativeToHandLocation;
	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	FRotator RelativeToHandRotation;
	FTransform RelativeToHandTransform;
	FTransform FinalRelativeHand;
	bool bInterpRelativeToHand;
	
	FTransform DefaultRelativeToHand;

	UPROPERTY(EditDefaultsOnly, Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	float SightDistance;

	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	FVector SightLocation;
	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	FRotator SightRotation;

	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming")
	bool bIsAiming;

	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Actions")
	float RotationAlpha;

	UPROPERTY(Transient, BlueprintReadOnly, Category = "Freedom | Aiming", Meta = (ClampMin = 0, ClampMax = 1))
	float AimingAlpha;

	bool bInterpAiming;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Aiming")
	float AimInterpolationMultiplier;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freedom | Aiming")
	float AimInterpolationSpeed;

	UPROPERTY(BlueprintReadOnly, Category = "Freedom | Aiming")
	float AimingLeanHeadAngle;

	void RefreshRelativeToHand(float DeltaSeconds);

	void RefreshAimingState(float DeltaSeconds);

	void SetSightTransform();
	void SetRelativeToHand();

	void InterpAimingAlpha(float DeltaSeconds);
	void InterpRelativeToHand(float DeltaSeconds);
	
#pragma endregion

	
};
