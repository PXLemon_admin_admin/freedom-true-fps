
#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstanceProxy.h"
#include "FreeHeroAnimInstanceProxy.generated.h"

class UFreeHeroAnimInstance;
class ACharacter;
class UCharacterMovementComponent;

/**
 * 动画代理 在工作线程上运行 提高性能
 */
USTRUCT()
struct FREETRUEFPS_API FFreeHeroAnimInstanceProxy : public FAnimInstanceProxy
{
	GENERATED_BODY()

	friend UFreeHeroAnimInstance;

	FFreeHeroAnimInstanceProxy() = default;
	explicit FFreeHeroAnimInstanceProxy(UAnimInstance* Instance);

protected:
	virtual void InitializeObjects(UAnimInstance* InAnimInstance) override;
	virtual void PreUpdate(UAnimInstance* InAnimInstance, float DeltaSeconds) override;
	virtual void Update(float DeltaSeconds) override;
	virtual void PostUpdate(UAnimInstance* InAnimInstance) const override;

private:
	/*UPROPERTY(Transient)
	UCharacterMovementComponent* Movement;

	UPROPERTY(Transient)
	bool bIsInAir1;

	UPROPERTY(Transient)
	bool bIsAccelerating1;

	UPROPERTY(Transient)
	bool bIsCrouched1;

	UPROPERTY(Transient)
	bool bIsLocalControlled1;

	UPROPERTY(Transient)
	float Speed1;*/
	
};
