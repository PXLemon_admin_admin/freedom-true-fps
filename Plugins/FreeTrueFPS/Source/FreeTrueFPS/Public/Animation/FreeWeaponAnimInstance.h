﻿
#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "FreeWeaponAnimInstance.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class FREETRUEFPS_API UFreeWeaponAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
};
