﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AttachmentBase.generated.h"


UCLASS()
class FREETRUEFPS_API AAttachmentBase : public AActor
{
	GENERATED_BODY()

public:
	AAttachmentBase();
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeAttachment")
	class UStaticMeshComponent* AttachmentMesh;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeAttachment | Data")
	class UAttachmentBaseData* AttachmentBaseData;

public:
	// UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeAttachment | Get")
	class UStaticMeshComponent* GetAttachmentMesh() const;
	
	// UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeAttachment | Data")
	class UAttachmentBaseData* GetAttachmentBaseData() const;

	struct FAttachmentBoostData* GetAttachmentBoostData() const;

	struct FAttachmentBoostData& GetAttachmentBoostDataRef() const;

	UFUNCTION(BlueprintCallable, Category = "Sight | Get")
	FName GetAttachToItemSocketName() const;

	UFUNCTION(BlueprintCallable, Category = "Sight | Get")
	FVector GetEffectSocketLocation() const;
	
};
