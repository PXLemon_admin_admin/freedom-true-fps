﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeBarrel.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeBarrel : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeBarrel();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	
};
