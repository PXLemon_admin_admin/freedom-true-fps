﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeForeGrip.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeForeGrip : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeForeGrip();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	
};
