﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeHandGuard.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeHandGuard : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeHandGuard();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	
};
