﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeLightLaser.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeLightLaser : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeLightLaser();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Aim")
	bool bAimable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Aim")
	FName AimSocket;

public:
	FTransform GetAimTransform() const;

	bool GetAimable() const;
	
};