﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeMagazine.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeMagazine : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeMagazine();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

};
