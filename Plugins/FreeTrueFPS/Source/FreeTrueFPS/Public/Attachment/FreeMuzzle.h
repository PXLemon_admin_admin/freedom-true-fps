﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeMuzzle.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeMuzzle : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeMuzzle();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Muzzle | Get")
	bool GetIsSilencer() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Muzzle | Get")
	FName GetMuzzleSocketName() const;
	
};
