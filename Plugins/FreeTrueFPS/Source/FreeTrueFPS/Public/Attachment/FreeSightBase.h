﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "Interface/SightInterface.h"
#include "FreeSightBase.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeSightBase : public AAttachmentBase, public ISightInterface
{
	GENERATED_BODY()

public:
	AFreeSightBase();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sight")
	bool bAimable{true};

public:
	/**
	 * 放大
	 */
	UFUNCTION(BlueprintCallable, Category = "Sight | Magnification")
	virtual void ZoomIn() {};
	/**
	 * 缩小
	 */
	UFUNCTION(BlueprintCallable, Category = "Sight | Magnification")
	virtual void ZoomOut() {};
	
	virtual bool GetIsGun() const override;
	virtual void ZoomSight(bool bZoomIn) override;
	virtual float GetZoomSensitivity() const override;
	
	// UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Sight | Get")
	virtual FTransform GetSightAimTransform() override;

	UFUNCTION(BlueprintCallable, Category = "Sight | Get")
	bool GetAimable() const;
	
};
