﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBase.h"
#include "FreeStock.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeStock : public AAttachmentBase
{
	GENERATED_BODY()

public:
	AFreeStock();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	
};
