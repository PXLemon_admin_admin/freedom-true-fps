﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FreeCharacterBase.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	AFreeCharacterBase();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(Replicated)
	FRotator RepControlRotation;

protected:

	UPROPERTY(BlueprintReadOnly, Category = "Freedom | Locomotion")
	FRotator RepControlRotForAimOffset;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Locomotion")
	float WalkSpeedNotAiming{240.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Locomotion")
	float RunSpeedNotAiming{480.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Freedom | Locomotion")
	float WalkSpeedAtAimingMultiplier{0.6f};

public:
	/*UFUNCTION(BlueprintCallable)
	FRotator GetAimOffset() const;*/

	UFUNCTION(BlueprintCallable, Category = "Freedom | Locomotion")
	FRotator GetReplicatedControlRotation() const;
	
	UFUNCTION(BlueprintCallable, Category = "Freedom | Locomotion")
	FRotator GetAimOffsetRotation() const;
	
};
