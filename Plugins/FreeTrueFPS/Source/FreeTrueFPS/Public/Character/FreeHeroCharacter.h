﻿
#pragma once

#include "CoreMinimal.h"
#include "FreeCharacterBase.h"
#include "GameFramework/Character.h"
#include "Interface/FreeHeroInterface.h"
#include "FreeHeroCharacter.generated.h"


UCLASS(Blueprintable)
class FREETRUEFPS_API AFreeHeroCharacter : public AFreeCharacterBase, public IFreeHeroInterface
{
	GENERATED_BODY()

public:
	AFreeHeroCharacter();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void PostInitializeComponents() override;
	virtual void PostInitProperties() override;

protected:
	virtual void BeginPlay() override;

#pragma region Input
private:
	void MoveForward(float Axis);
	void MoveRight(float Axis);
	void Turn(float Axis);
	void LookUp(float Axis);
	void JumpStart();
	void JumpStop();
	void AimingStart();
	void AimingStop();
	void LeanLeftStart();
	void LeanRightStart();
	void LeanStop();
	void CycleSights();
	void Reload();
	void FirePressed();
	void FireRelease();
	void SwitchFireMode();

#pragma endregion

#pragma region Component
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Component")
	class UCameraComponent* FPCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Component")
	class USpringArmComponent* TPSpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Component")
	class UCameraComponent* TPCamera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FreeHeroes | Component")
	class UFreeHeroComponent* FreeHeroComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FreeHeroes | Component")
	class UFreeHealthComponent* FreeHealthComponent;

	/*UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FreeHeroes | Component")
	class USkeletalMeshComponent* Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FreeHeroes | Component")
	class UStaticMeshComponent* Optic;*/

#pragma endregion
	
public:

	virtual USkeletalMeshComponent* GetPawnMesh() const override { return GetMesh(); }
	virtual void WantsToJump() override { Jump(); };
	virtual void WantsToStopJump() override { StopJumping(); };
	virtual FTransform GetCameraTransform() const override;
	virtual UCameraComponent* GetFPCameraComponent() const override { return FPCamera; }

	class UFreeHeroComponent* GetHeroComponent() const;
	class UFreeHealthComponent* GetHealthComponent() const;

	virtual void ApplyDamage(float BaseDamage, const UPhysicalMaterial* const PhysicalMaterial,
		AActor* DamageActor, FVector& HitFromDirection, const FHitResult& HitResult,
		AController* EventInstigator, AActor* DamageCauser, TSubclassOf<class UFreeDamageType> DamageTypeClass) override;

	UFUNCTION()
	void OnDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy,
	              FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName,
	              FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

	virtual bool GetIsPlayer() const override { return true; }

	/*FTransform GetSightAimTransform() const { return Optic->GetSocketTransform(TEXT("Aim")); }*/
	
};
