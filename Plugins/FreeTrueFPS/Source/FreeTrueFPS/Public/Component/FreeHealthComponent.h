﻿
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FreeHealthComponent.generated.h"

#define MAX_HEALTH 200.f

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class FREETRUEFPS_API UFreeHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFreeHealthComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
							   FActorComponentTickFunction* ThisTickFunction) override;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Data")
	class UBodyDamageData* BodyDamageData;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Health")
	float MaxHealth{MAX_HEALTH};

	UPROPERTY(ReplicatedUsing = OnRep_CurrentHealth, EditAnywhere, BlueprintReadOnly, Category = "FreeHeroes | Health")
	float CurrentHealth;

	UFUNCTION()
	void OnRep_CurrentHealth();

public:
	
	/**
	 * 承受伤害
	 * @param BaseDamage 伤害
	 */
	void ApplyDamage(float BaseDamage, const UPhysicalMaterial* const PhysicalMaterial,
		AActor* DamageActor, FVector& HitFromDirection, const FHitResult& HitResult,
		AController* EventInstigator, AActor* DamageCauser, TSubclassOf<class UFreeDamageType> DamageTypeClass);

	void TakeDamage(float Damage);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroes | Health | Get")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroes | Health | Get")
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroes | Health | Get")
	float GetHealthPercentage() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroes | Health | Get")
	bool GetIsAlive() const;
	
};
