﻿
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Data/FreeType.h"
#include "FreeHeroComponent.generated.h"


class UFreeHeroBaseData;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class FREETRUEFPS_API UFreeHeroComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFreeHeroComponent();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void PostInitProperties() override;

protected:
	virtual void BeginPlay() override;

public:
	bool GetHasAuthority() const;

private:
	void InitHeroData(class UFreeHeroBaseData* HeroData);

#pragma region InputAction
public:
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void MoveForward(float Axis);
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void MoveRight(float Axis);
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void Turn(float Axis);
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void LookUp(float Axis);
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void JumpStart();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void JumpStop();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void AimingStart();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void AimingStop();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void LeanLeftStart();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void LeanRightStart();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void LeanStop();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void CycleSights();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void Reload();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void FirePressed();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void FireRelease();
	UFUNCTION(BlueprintCallable, Category = "FreeHereos | Input")
	void SwitchFireMode();

#pragma endregion 

#pragma region Properties
protected:
	
	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	class APawn* MyPawn;

	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	class USkeletalMeshComponent* TPMesh;

	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	class USkeletalMeshComponent* FPMesh;
	
	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	class UCameraComponent* FPCamera;

	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	TWeakObjectPtr<class UFreeHeroAnimInstance> AnimInstance;

	UPROPERTY(BlueprintReadOnly, Category = "FreeHeroes | Ref")
	TWeakObjectPtr<class AController> PawnController;

	UPROPERTY(ReplicatedUsing = OnRep_CurrentGun, BlueprintReadOnly, Category = "FreeHeroes | Ref")
	class AFreeFirearmBase* CurrentGunFree;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = "FreeHeroes | Inventory")
	TArray<TSubclassOf<class AFreeFirearmBase> > DefaultInventoryClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Replicated, Category = "FreeHeroes | Inventory")
	TArray<class AFreeFirearmBase*> FirearmsInventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Data")
	class UFreeHeroBaseData* FreeHeroBaseData;

	struct FFirearmAnimState* FirearmAnimState{nullptr};
	struct FFirearmRecoilState* FirearmRecoilState{nullptr};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Socket")
	FName FPCameraSocket{TEXT("FP_Camera")};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Socket")
	FName HandRSocket{TEXT("HandRSocket")};
	
	UPROPERTY(ReplicatedUsing = OnRep_IsAiming, BlueprintReadOnly, Category = "FreeHeroes | States")
	bool bIsAiming{false};

	/*UPROPERTY(Replicated, BlueprintReadOnly, Category = "FreeHeroes | States")
	bool bIsReloading{false};*/

	bool bIsLocalControlled{false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | States")
	float DefaultLeanAngle{40.f};

	/**
	 * 侧身角度(Replicated)
	 */
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "FreeHeroes | States")
	float LeanBodyAngle{0.f};

	/**
	 * input axis
	 */
	float MoveForwardAxis{0.f};
	float MoveRightAxis{0.f};
	float TurnAxis{0.f};
	float LookUpAxis{0.f};

	float BaseTurnRate{65.f};

	bool bInitComplete{false};

	float AimingSwayMultiplier{0.6f};

#pragma endregion

#pragma region RPC
protected:
	UFUNCTION(Server, Unreliable)
	void Server_SetAiming(bool bAiming);
	void Server_SetAiming_Implementation(bool bAiming);

	/*UFUNCTION(Server, Unreliable)
	void Server_PlayReload();
	void Server_PlayReload_Implementation();
	UFUNCTION(NetMulticast, Unreliable)
	void Multi_PlayReloadAnim();
	void Multi_PlayReloadAnim_Implementation();*/
	
	UFUNCTION(Server, Unreliable)
	void Server_SetLeanAngle(const float LeanAngle);
	void Server_SetLeanAngle_Implementation(const float LeanAngle);

	UFUNCTION(Server, Unreliable)
	void Server_StartFire();
	void Server_StartFire_Implementation();
	UFUNCTION(NetMulticast, Unreliable)
	void Multi_StartFire();
	void Multi_StartFire_Implementation();

	UFUNCTION(Server, Unreliable)
	void Server_SetEquipGun(class AFreeFirearmBase* Gun);
	void Server_SetEquipGun_Implementation(class AFreeFirearmBase* Gun);

#pragma endregion

#pragma region OnRep
	
protected:
	UFUNCTION()
	void OnRep_CurrentGun(class AFreeFirearmBase* LastWeapon);

	UFUNCTION()
	void OnRep_IsAiming();
	
#pragma endregion 
	
protected:
	void SetAimingState(bool bAiming);

	void SetLeanAngle(const float NewLeanAngle);

	void Fire();
	void PlayFireAnimMontage();

	float GetSightZoomSensitivity() const;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** updates current weapon */
	void SetCurrentWeapon(class AFreeFirearmBase* NewWeapon, class AFreeFirearmBase* LastWeapon = nullptr);

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();
	
	/**
	 * [server] add weapon to inventory
	 * @param Gun Gun to Add
	 */
	void AddWeapon(class AFreeFirearmBase* Gun);

	/**
	 * [server] remove weapon from inventory
	 * @param Gun gun to remove
	 */
	void RemoveWeapon(class AFreeFirearmBase* Gun);

public:

	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Init")
	void Init(class UCameraComponent * NewFPCamera, class USkeletalMeshComponent* NewTPMesh, class USkeletalMeshComponent* NewFPMesh);
	
	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Get")
	class AFreeFirearmBase* GetCurrentGun() const;

	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Gun")
	void EquipGun(class AFreeFirearmBase* Weapon);
	
	void ExecRecoil(float Multiplier) const;
	void SetMyPawn(class AFreeHeroCharacter* NewPawn);
	void SetMyAnimInstance(class UFreeHeroAnimInstance* NewAnimInstance);
	
	bool GetAiming() const;
	float GetLeanAngle() const;
	float GetAimingHeadLeanAngle() const;
	bool GetIsEnableFireMontage() const;
	
	FTransform GetSightSocketTransform() const;
	FTransform GetHandRTransform() const;
	FFreeMinMax* GetDefaultLookUpAngle() const;
	FTransform GetFPCameraSocketTransform(ERelativeTransformSpace RelativeTransformSpace) const;

	float GetMoveForwardAxis() const;
	float GetMoveRightAxis() const;
	float GetTurnAxis() const;
	float GetLookUpAxis() const;

	APawn* GetOwnerPawn() const;

	/**
	 * 默认返回全身模型TPMesh
	 * @return TPMesh
	 */
	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Get")
	class USkeletalMeshComponent* GetPawnMesh() const;
	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Get")
	class USkeletalMeshComponent* GetTPMesh() const;
	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Get")
	class USkeletalMeshComponent* GetFPMesh() const;
	
	class AController* GetPawnController() const;

	bool GetInitComplete() const;

	UFUNCTION(BlueprintCallable, Category = "FreeHeroes | Get", meta = (ExpandBoolAsExecs = "ReturnValue"))
	bool IsLocallyControlled();

	class UCameraComponent* GetFPCameraComponent() const;

	float GetAimingSwayMultiplier() const;

	FName GetFPCameraSocket() const;
	FName GetHandRSocket() const;

	bool HasMontagePlaying() const;

	void StopMontage(const class UAnimMontage* Montage, float BlendOut) const;
	void StopFireSlotMontage(float BlendOut) const ;
	void StopAllMontage(float BlendOut) const;
	
};
