﻿
#pragma once

#include "CoreMinimal.h"
#include "Data/ScopeDataType.h"
#include "Components/SceneCaptureComponent2D.h"
#include "FreeSceneCaptureComponent2D.generated.h"


class UFreeSceneCaptureComp2dData;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class FREETRUEFPS_API UFreeSceneCaptureComponent2D : public USceneCaptureComponent2D
{
	GENERATED_BODY()

public:
	UFreeSceneCaptureComponent2D();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
								   FActorComponentTickFunction* ThisTickFunction) override;
	virtual void PostInitProperties() override;

protected:
	virtual void BeginPlay() override;

private:
	void LoadSceneCaptureComp2dData();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeSceneCaptureComponent2D")
	UFreeSceneCaptureComp2dData* SceneCaptureComp2dData;
	bool bInitDataComplete{false};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeSceneCaptureComponent2D")
	FFreeScopeOptimization ScopeOptimization;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeSceneCaptureComponent2D")
	FRenderTargetSize RenderTargetSize;

	void SetupOnBegin();

public:
	UFUNCTION(BlueprintCallable, Category = "FreeSceneCaptureComponent2D", meta = (CompactNodeTitle = "StartCapture"))
	void StartCapture();
	UFUNCTION(BlueprintCallable, Category = "FreeSceneCaptureComponent2D", meta = (CompactNodeTitle = "StopCapture"))
	void StopCapture();

};
