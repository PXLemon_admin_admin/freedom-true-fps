﻿
#pragma once

#include "CoreMinimal.h"
#include "AttachmentBoostData.h"
#include "Engine/DataAsset.h"
#include "FreeType.h"
#include "AttachmentBaseData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class FREETRUEFPS_API UAttachmentBaseData : public UDataAsset
{
	GENERATED_BODY()

public:

	/**
	 * 配件名
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment")
	FName AttachmentName;

	/**
	 * 配件网格体
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment")
	class UStaticMesh* AttachmentMesh;

	/**
	 * 配件将要附加到物体的哪个插槽上
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment")
	FName AttachToItemSocket;

	/**
	 * 配件类型
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment")
	EAttachmentType Type;

	/**
	 * 给生成特效需要的位置用的插槽 如枪管
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment", meta = (EditCondition = "Type == EAttachmentType::Muzzle"))
	FName EffectSocket{TEXT("Muzzle")};

	/**
	 * 是否是消音器
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment", meta = (EditCondition = "Type == EAttachmentType::Muzzle"))
	bool bIsSilencer{false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Aim")
	bool bAimable{false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Aim", meta = (EditCondition = "bAimable"))
	FName AimSocket{TEXT("Aim")};

	/**
	 * 配件加成数据(可为负数 为负表示负加成)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoostData")
	FAttachmentBoostData AttachmentBoostData;
	
};
