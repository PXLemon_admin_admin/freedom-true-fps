﻿
#pragma once

//#include "Engine/DataTable.h"
#include "AttachmentBoostData.generated.h"

/**
 * 枪械配件加成数据(可为负数 负表示负加成)
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FAttachmentBoostData/* : public FTableRowBase*/
{
	GENERATED_BODY()

	
	/**
	 * 射弹伤害加成
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoostData", meta = (ClampMin = -99999, ClampMax = 99999))
	float DamageBoost;

	/**
	 * 后坐力加成 (百分比)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoostData", meta = (ClampMin = -100, ClampMax = 100, Units = "%"))
	float RecoilBoostPercent;

	/**
	 * 射弹初速加成
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile", meta = (ClampMin = -999999, ClampMax = 999999))
	float ProjectileInitSpeedBoost;

	/**
	 * 射弹最大速度加成
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile", meta = (ClampMin = -999999, ClampMax = 999999))
	float ProjectileMaxSpeedBoost;

	FAttachmentBoostData()
	{
		ProjectileInitSpeedBoost = 0.f;
		ProjectileMaxSpeedBoost = 0.f;
		DamageBoost = 0.f;
		RecoilBoostPercent = 0.f;
	}

	FAttachmentBoostData(float DamageBoost, float RecoilBoostPercent, float ProjectileInitSpeedBoost,
		float ProjectileMaxSpeedBoost)
		: DamageBoost(DamageBoost),
		  RecoilBoostPercent(RecoilBoostPercent),
		  ProjectileInitSpeedBoost(ProjectileInitSpeedBoost),
		  ProjectileMaxSpeedBoost(ProjectileMaxSpeedBoost)
	{
	}

	FAttachmentBoostData(const FAttachmentBoostData& Other)
		: DamageBoost(Other.DamageBoost),
		  RecoilBoostPercent(Other.RecoilBoostPercent),
		  ProjectileInitSpeedBoost(Other.ProjectileInitSpeedBoost),
		  ProjectileMaxSpeedBoost(Other.ProjectileMaxSpeedBoost)
	{
	}

	FAttachmentBoostData(FAttachmentBoostData&& Other) noexcept
		: DamageBoost(Other.DamageBoost),
		  RecoilBoostPercent(Other.RecoilBoostPercent),
		  ProjectileInitSpeedBoost(Other.ProjectileInitSpeedBoost),
		  ProjectileMaxSpeedBoost(Other.ProjectileMaxSpeedBoost)
	{
	}

	FAttachmentBoostData& operator=(const FAttachmentBoostData& Other)
	{
		if (this == &Other)
		{
			UE_LOG(LogTemp,Error, TEXT("FAttachmentBoostData& operator=(const FAttachmentBoostData& Other) if (this == &Other) "))
			return *this;
		}
			
		DamageBoost = Other.DamageBoost;
		RecoilBoostPercent = Other.RecoilBoostPercent;
		ProjectileInitSpeedBoost = Other.ProjectileInitSpeedBoost;
		ProjectileMaxSpeedBoost = Other.ProjectileMaxSpeedBoost;
		return *this;
	}

	FAttachmentBoostData& operator=(FAttachmentBoostData&& Other) noexcept
	{
		if (this == &Other)
		{
			UE_LOG(LogTemp,Error, TEXT("FAttachmentBoostData& operator=(FAttachmentBoostData&& Other) noexcept if (this == &Other) "))
			return *this;
		}
			
		DamageBoost = Other.DamageBoost;
		RecoilBoostPercent = Other.RecoilBoostPercent;
		ProjectileInitSpeedBoost = Other.ProjectileInitSpeedBoost;
		ProjectileMaxSpeedBoost = Other.ProjectileMaxSpeedBoost;
		return *this;
	}
};
