﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BodyDamageData.generated.h"

/**
 * 玩家身体各部位伤害倍率
 */
UCLASS(BlueprintType)
class FREETRUEFPS_API UBodyDamageData : public UDataAsset
{
	GENERATED_BODY()

public:
	/**
	 * 头部伤害倍率
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeros | BodyDamage")
	float HeadMultiplier{4.0f};

	/**
	 * 躯干伤害倍率(除去头 四肢就是躯干)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeros | BodyDamage")
	float TorsoMultiplier{1.5f};

	/**
	 * 手臂伤害倍率
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeros | BodyDamage")
	float ArmMultiplier{1.25f};

	/**
	 * 腿部伤害倍率
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeros | BodyDamage")
	float LegMultiplier{1.25f};

	/**
	 * 默认伤害倍率
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeros | BodyDamage")
	float DefaultMultiplier{1.1f};
	
};
