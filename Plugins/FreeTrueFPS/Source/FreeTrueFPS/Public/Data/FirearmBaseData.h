﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "FreeType.h"
#include "FirearmSocket.h"
#include "State/FirearmAnimState.h"
#include "State/FirearmRecoilState.h"
#include "State/FreeGunSwayData.h"
#include "State/FreeAttachmentState.h"
#include "State/AimingOffsetState.h"
#include "Weapon/FreeDamageType.h"
#include "FirearmBaseData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class FREETRUEFPS_API UFirearmBaseData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UFirearmBaseData()
	{
		WeaponName = NAME_None;

		WeaponMesh = nullptr;
		Weight = 1.0f;
		FirearmMuzzleFireFX = nullptr;
		FirearmMuzzleFireFXSuppressed = nullptr;
		FirearmEjectShellFX = nullptr;
		FireSound = nullptr;
		FireSoundSuppressed = nullptr;
		DamageType = UFreeDamageType::StaticClass();
		
		BurstCount = 3;
		MagCount = 3;
		PerMagCapacityMaxAmmo = 30;
		MagCapacity = PerMagCapacityMaxAmmo * MagCount;
		AmmoInClip = PerMagCapacityMaxAmmo;
		MaxAmmo = MagCapacity + AmmoInClip;
		
		AimingInterpSpeed = 50.f;
	}
	

	/**
	 * 武器名
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	FName WeaponName;

	/**
	 * 武器网格体
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	class USkeletalMesh* WeaponMesh;

	/**
	 * 武器附加到玩家身上的哪个插槽
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	FName AttachToPlayerSocket{TEXT("Weapon")};

	/**
	 * 武器射速(每分钟能射出多少发子弹)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	float FireRateRPM{700.f};

	/**
	 * 武器重量
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base", meta = (ClampMin = 0))
	float Weight;

	/**
	 * 武器伤害
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base", meta = (ClampMin = 0))
	float BaseDamage{28.f};

	/**
	 * 瞄准时的插值速度
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base", meta = (ClampMin = 1))
	float AimingInterpSpeed{50.f};

	/**
	 * 武器类型
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	EFreeWeaponType WeaponType{EFreeWeaponType::Rifle};
	
	/**
	 * 武器开火模式(可用模式)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base", meta = (ArrayClamp = "1, 3"))
	TArray<EFreeFireMode> FireModes{EFreeFireMode::Semi};

	/**
	 * Burst射击模式 连击次数
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base", meta = (ClampMin = 2, ClampMax = 10))
	int32 BurstCount;

	/**
	 * 瞄准时的偏移调整
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	FAimingOffsetState AimingOffsetState;

	/**
	 * 武器动画实例类
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	TSubclassOf<class UFreeWeaponAnimInstance> WeaponAnimClass;

	/**
	 * DamageType
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base")
	TSubclassOf<class UFreeDamageType> DamageType;
	
	/**
	 * 武器射弹类
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	TSubclassOf<class AFreeProjectile> ProjectileClass;

	/**
	 * 子弹类型(给PlayerState记录数据用的)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	EFreeAmmoType AmmoType{EFreeAmmoType::RifleAmmo};

	/**
	 * 最大子弹
	 */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	int32 MaxAmmo;

	/**
	 * 弹夹容量 (等于 MagCapacityMaxAmmo * MagCount)
	 */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo", meta = (ClampMin = 0))
	int32 MagCapacity;

	/**
	 * 弹夹数量
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo", meta = (ClampMin = 0, ClampMax = 9999))
	int32 MagCount;

	/**
	 * 每个弹夹的最大容量
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo",meta = (ClampMin = 1, ClampMax = 9999))
	int32 PerMagCapacityMaxAmmo;

	/**
	 * 当前弹夹里有多少子弹
	 */
	UPROPERTY(BlueprintReadWrite, Category = "Ammo", meta = (ClampMin = 0))
	int32 AmmoInClip;

	/**
	 * 开火枪管处特效
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	class UParticleSystem* FirearmMuzzleFireFX;

	/**
	 * 开火消音枪管枪口特效
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	class UParticleSystem* FirearmMuzzleFireFXSuppressed;

	/**
	 * 抛弹壳特效
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	class UParticleSystem* FirearmEjectShellFX;

	/**
	 * 开火音效
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	class USoundBase* FireSound;

	/**
	 * 已消音开火音效
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	class USoundBase* FireSoundSuppressed;

	/**
	 * 枪摇晃数据
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sway")
	FFreeGunSwayData GunSwayData{};

	/**
	 * 默认配件数据
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attachment")
	FFreeAttachmentState AttachmentState{};

	/**
	 * 插槽数据
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Socket")
	FFirearmSocket FirearmSocket{};
	
	/**
	 * 与武器相关的动画资源
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Anim")
	FFirearmAnimState FirearmAnimState{};

	/**
	 * 武器后坐力资源
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Recoil")
	FFirearmRecoilState FirearmRecoilState{};
	
};
