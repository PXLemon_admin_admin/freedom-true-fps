﻿
#pragma once

/*#include "Engine/DataTable.h"*/
#include "FirearmSocket.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFirearmSocket/* : public FTableRowBase*/
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName AimSocket{TEXT("Aim")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName MuzzleFXSocket{TEXT("MuzzleFX")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName MuzzleAttachSocket{TEXT("MuzzleAttach")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName HandGuardAttachSocket{TEXT("HandGuard")};

	/**
	 * 红点 全息 热成像 瞄准镜使用这个插槽
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName SightSocket{TEXT("Sight")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName LPVOSocket{TEXT("LPVO")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName LaserSocket{TEXT("Laser")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName LightSocket{TEXT("Light")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName StockSocket{TEXT("Stock")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName ForeGripSocket{TEXT("ForeGrip")};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName MagazineSocket{TEXT("Magazine")};

	/**
	 * 导轨插槽
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firearm | Socket")
	FName RailSocket{TEXT("RailSocket")};

	FFirearmSocket() = default;

	FFirearmSocket(const FName& AimSocket, const FName& MuzzleFXSocket, const FName& MuzzleAttachSocket,
		const FName& SightSocket, const FName& LpvoSocket, const FName& LaserSocket, const FName& LightSocket,
		const FName& StockSocket, const FName& ForeGripSocket, const FName& MagazineSocket, const FName& RailSocket)
		: AimSocket(AimSocket),
		  MuzzleFXSocket(MuzzleFXSocket),
		  MuzzleAttachSocket(MuzzleAttachSocket),
		  SightSocket(SightSocket),
		  LPVOSocket(LpvoSocket),
		  LaserSocket(LaserSocket),
		  LightSocket(LightSocket),
		  StockSocket(StockSocket),
		  ForeGripSocket(ForeGripSocket),
		  MagazineSocket(MagazineSocket),
		  RailSocket(RailSocket)
	{
	}

	FFirearmSocket(const FFirearmSocket& Other)
		: AimSocket(Other.AimSocket),
		  MuzzleFXSocket(Other.MuzzleFXSocket),
		  MuzzleAttachSocket(Other.MuzzleAttachSocket),
		  SightSocket(Other.SightSocket),
		  LPVOSocket(Other.LPVOSocket),
		  LaserSocket(Other.LaserSocket),
		  LightSocket(Other.LightSocket),
		  StockSocket(Other.StockSocket),
		  ForeGripSocket(Other.ForeGripSocket),
		  MagazineSocket(Other.MagazineSocket),
		  RailSocket(Other.RailSocket)
	{
	}

	FFirearmSocket(FFirearmSocket&& Other) noexcept
		: AimSocket(MoveTemp(Other.AimSocket)),
		  MuzzleFXSocket(MoveTemp(Other.MuzzleFXSocket)),
		  MuzzleAttachSocket(MoveTemp(Other.MuzzleAttachSocket)),
		  SightSocket(MoveTemp(Other.SightSocket)),
		  LPVOSocket(MoveTemp(Other.LPVOSocket)),
		  LaserSocket(MoveTemp(Other.LaserSocket)),
		  LightSocket(MoveTemp(Other.LightSocket)),
		  StockSocket(MoveTemp(Other.StockSocket)),
		  ForeGripSocket(MoveTemp(Other.ForeGripSocket)),
		  MagazineSocket(MoveTemp(Other.MagazineSocket)),
		  RailSocket(MoveTemp(Other.RailSocket))
	{
	}

	FFirearmSocket& operator=(const FFirearmSocket& Other)
	{
		if (this == &Other)
			return *this;
		AimSocket = Other.AimSocket;
		MuzzleFXSocket = Other.MuzzleFXSocket;
		MuzzleAttachSocket = Other.MuzzleAttachSocket;
		SightSocket = Other.SightSocket;
		LPVOSocket = Other.LPVOSocket;
		LaserSocket = Other.LaserSocket;
		LightSocket = Other.LightSocket;
		StockSocket = Other.StockSocket;
		ForeGripSocket = Other.ForeGripSocket;
		MagazineSocket = Other.MagazineSocket;
		RailSocket = Other.RailSocket;
		return *this;
	}

	FFirearmSocket& operator=(FFirearmSocket&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		AimSocket = MoveTemp(Other.AimSocket);
		MuzzleFXSocket = MoveTemp(Other.MuzzleFXSocket);
		MuzzleAttachSocket = MoveTemp(Other.MuzzleAttachSocket);
		SightSocket = MoveTemp(Other.SightSocket);
		LPVOSocket = MoveTemp(Other.LPVOSocket);
		LaserSocket = MoveTemp(Other.LaserSocket);
		LightSocket = MoveTemp(Other.LightSocket);
		StockSocket = MoveTemp(Other.StockSocket);
		ForeGripSocket = MoveTemp(Other.ForeGripSocket);
		MagazineSocket = MoveTemp(Other.MagazineSocket);
		RailSocket = MoveTemp(Other.RailSocket);
		return *this;
	}
};
