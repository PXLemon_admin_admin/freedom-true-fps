﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Data/FreeType.h"
#include "FreeHeroBaseData.generated.h"

/**
 * 
 */
UCLASS()
class FREETRUEFPS_API UFreeHeroBaseData : public UDataAsset
{
	GENERATED_BODY()

public:
	
	/**
	 * 瞄准时歪头角度
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	float AimingLeanHeadAngle{35.f};

	/**
	 * 瞄准时枪摇摆倍率 0.3 = 只有未瞄准时的30%
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (ClampMin = 0.1))
	float AimingSwayMultiplier{0.3f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	FFreeMinMax DefaultLookUpAngle{-75.f, 75.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input")
	float InputBaseTurnRate{65.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Socket")
	FName HandRSocket{TEXT("hand_r")};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Socket")
	FName FPCameraSocket{TEXT("FP_Camera")};
	
	/**
	 * 侧身角度(固定)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "States")
	float DefaultLeanBodyAngle{35.f};
	
};
