﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "FreeImpactData.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeImpactData : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effect")
	class UMaterialInterface* ImpactDecalMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effect")
	class UParticleSystem* ImpactParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effect")
	class USoundBase* ImpactSound;
	

	FFreeImpactData() = default;

	FFreeImpactData(UMaterialInterface* ImpactDecalMaterial, UParticleSystem* ImpactParticle, USoundBase* ImpactSound)
		: ImpactDecalMaterial(ImpactDecalMaterial),
		  ImpactParticle(ImpactParticle),
		  ImpactSound(ImpactSound)
	{
	}

	FFreeImpactData(const FFreeImpactData& Other)
		: FTableRowBase(Other),
		  ImpactDecalMaterial(Other.ImpactDecalMaterial),
		  ImpactParticle(Other.ImpactParticle),
		  ImpactSound(Other.ImpactSound)
	{
	}

	FFreeImpactData(FFreeImpactData&& Other) noexcept
		: FTableRowBase(MoveTemp(Other)),
		  ImpactDecalMaterial(Other.ImpactDecalMaterial),
		  ImpactParticle(Other.ImpactParticle),
		  ImpactSound(Other.ImpactSound)
	{
	}

	FFreeImpactData& operator=(const FFreeImpactData& Other)
	{
		if (this == &Other)
			return *this;
		FTableRowBase::operator =(Other);
		ImpactDecalMaterial = Other.ImpactDecalMaterial;
		ImpactParticle = Other.ImpactParticle;
		ImpactSound = Other.ImpactSound;
		return *this;
	}

	FFreeImpactData& operator=(FFreeImpactData&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		FTableRowBase::operator =(MoveTemp(Other));
		ImpactDecalMaterial = Other.ImpactDecalMaterial;
		ImpactParticle = Other.ImpactParticle;
		ImpactSound = Other.ImpactSound;
		return *this;
	}
};
