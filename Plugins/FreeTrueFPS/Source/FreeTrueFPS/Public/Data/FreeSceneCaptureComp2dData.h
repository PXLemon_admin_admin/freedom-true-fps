﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ScopeDataType.h"
#include "FreeSceneCaptureComp2dData.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class FREETRUEFPS_API UFreeSceneCaptureComp2dData : public UDataAsset
{
	GENERATED_BODY()

public:

	UFreeSceneCaptureComp2dData() = default;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Optimization")
	FFreeScopeOptimization ScopeOptimization;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RenderTarget")
	FRenderTargetSize RenderTargetSize;
	
};
