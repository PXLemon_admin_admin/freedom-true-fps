﻿
#pragma once

#include "FreeType.generated.h"

UENUM(BlueprintType)
enum class EMoveState : uint8
{
	Idle			UMETA(DisplayName = "Idle"),
	Walking			UMETA(DisplayName = "Walking"),
	Running			UMETA(DisplayName = "Running")
};

/**
 * 装备的武器类型
 */
UENUM(BlueprintType)
enum class EEquipWeaponType : uint8
{
	// 主武器
	Primary			UMETA(DisplayName = "Primary"),
	// 副武器
	Secondary		UMETA(DisplayName = "Secondary"),
	// 近战武器
	Melee			UMETA(DisplayName = "Melee"),
	// 爆炸物 如手雷
	Explosive		UMETA(DisplayName = "Explosive"),
	// 特殊 如肾上腺素
	Special			UMETA(DisplayName = "Special")
};

UENUM(BlueprintType)
enum class EFreeWeaponType : uint8
{
	Rifle			UMETA(DisplayName = "Rifle"),
	Pistol          UMETA(DisplayName = "Pistol"),
	Shotgun			UMETA(DisplayName = "Shotgun"),
	SniperRifle		UMETA(DisplayName = "SniperRifle"),
	Explosive		UMETA(DisplayName = "Explosive"),
	Melee           UMETA(DisplayName = "Melee")
};

UENUM(BlueprintType)
enum class EPickupItem : uint8
{
	HealthPack		UMETA(DisplayName = "HealthPack"),
	Bullet			UMETA(DisplayName = "Bullet"),
	Magazine		UMETA(DisplayName = "Magazine"),
	Firearm			UMETA(DisplayName = "Firearm"),
	Attachment		UMETA(DisplayName = "Attachment")
};

UENUM(BlueprintType)
enum class EFreeFireMode : uint8
{
	Semi			UMETA(DisplayName = "Semi"),
	FullAuto		UMETA(DisplayName = "FullAuto"),
	Burst			UMETA(DisplayName = "Burst"),
	Safe			UMETA(DisplayName = "Safe"),
};

UENUM(BlueprintType)
enum class EAttachmentType : uint8
{
	Sight			UMETA(DisplayName = "Sight"),
	// Magnifier		UMETA(DisplayName = "Magnifier"),
	Stock			UMETA(DisplayName = "Stock"),
	Muzzle			UMETA(DisplayName = "Muzzle"),
	HandGuard		UMETA(DisplayName = "HandGuard"),
	ForeGrip		UMETA(DisplayName = "ForeGrip"),
	/*Light			UMETA(DisplayName = "Light"),
	Laser			UMETA(DisplayName = "Laser"),*/
	LightLaser		UMETA(DisplayName = "LightAndLaser"),
	// 导轨
	Rail			UMETA(DisplayName = "Rail"),
	
	Other			UMETA(DisplayName = "Other")
};

UENUM(BlueprintType)
enum class EFreeSocketType : uint8
{
	Aim				UMETA(DisplayName = "Aim"),
	
	MuzzleFX		UMETA(DisplayName = "MuzzleFX"),
	MuzzleAttach	UMETA(DisplayName = "MuzzleAttach"),
	
	Sight			UMETA(DisplayName = "Sight"),
	/*Magnifier		UMETA(DisplayName = "Magnifier"),
	LPVO			UMETA(DisplayName = "LPVO"),*/
	
	Stock			UMETA(DisplayName = "Stock"),
	Muzzle			UMETA(DisplayName = "Muzzle"),
	HandGuard		UMETA(DisplayName = "HandGuard"),
	ForeGrip		UMETA(DisplayName = "ForeGrip"),
	/*Light			UMETA(DisplayName = "Light"),
	Laser			UMETA(DisplayName = "Laser"),*/
	LightLaser		UMETA(DisplayName = "LightAndLaser"),
	Magazine		UMETA(DisplayName = "Magazine"),

	// 导轨
	Rail			UMETA(DisplayName = "Rail")
};

/*
UENUM(BlueprintType)
enum class EProjectileType : uint8
{
	EBullet,
	ERocket,
	EMax,
};
*/

UENUM(BlueprintType)
enum class EFreeAmmoType : uint8
{
	RifleAmmo		UMETA(DisplayName = "RifleAmmo"),
	PistolAmmo		UMETA(DisplayName = "PistolAmmo"),
	ShotgunAmmo		UMETA(DisplayName = "ShotgunAmmo"),
	SniperAmmo		UMETA(DisplayName = "SniperAmmo"),
	Rocket			UMETA(DisplayName = "Rocket")
};

/**
 * MinMax
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeMinMax
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeMixMax")
	float Min{0.0f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeMixMax")
	float Max{0.0f};

	FFreeMinMax() = default;
	
	FFreeMinMax(const float Min,const float Max)
	{
		this->Min = Min;
		this->Max = Max;
	}

	FFreeMinMax(const FFreeMinMax& Other)
		: Min(Other.Min),
		  Max(Other.Max)
	{
	}

	FFreeMinMax(FFreeMinMax&& Other) noexcept
		: Min(Other.Min),
		  Max(Other.Max)
	{
	}

	FFreeMinMax& operator=(const FFreeMinMax& Other)
	{
		if (this == &Other)
			return *this;
		Min = Other.Min;
		Max = Other.Max;
		return *this;
	}

	FFreeMinMax& operator=(FFreeMinMax&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		Min = Other.Min;
		Max = Other.Max;
		return *this;
	}
};


USTRUCT(BlueprintType)
struct FProjectileTransform
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite, Category = "Projectile")
	FVector Location;
	UPROPERTY(BlueprintReadWrite, Category = "Projectile")
	FRotator Rotation;

	FProjectileTransform() {Location = FVector_NetQuantize100(); Rotation = FRotator();}
	
	FProjectileTransform(const FVector& INLocation, const FRotator& INRotation)
	{
		Location = FVector_NetQuantize100(INLocation);
		Rotation = INRotation;
	}
	FProjectileTransform(const FTransform& INTransform)
	{
		Location = INTransform.GetLocation();
		Rotation = INTransform.Rotator();
	}
	
	static FTransform GetTransformFromProjectile(const FProjectileTransform& ProjectileTransform)
	{
		FVector Loc = ProjectileTransform.Location;
		return FTransform(ProjectileTransform.Rotation, ProjectileTransform.Location, FVector::OneVector);
	}
	FTransform GetTransformFromProjectile() const
	{
		return FTransform(Rotation, Location, FVector::OneVector);
	}
	
};
