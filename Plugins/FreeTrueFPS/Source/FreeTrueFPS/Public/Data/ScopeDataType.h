﻿
#pragma once

#include "ScopeDataType.generated.h"

//////////////////////////////////////
/// 倍镜


UENUM(BlueprintType)
enum class EScopeAdjustment : uint8
{
	MRAD	UMETA(DisplayName = "MRAD"),
	MOA		UMETA(DisplayName = "MOA")
};

USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeScopeOptimization
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Optimization")
	bool bOverrideCaptureEveryFrame{true};

	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bOverrideCaptureEveryFrame", ClampMin = 1, ClampMax = 120))
	float RefreshRate{60.f};

	// 未瞄准时关闭组件更新
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bOverrideCaptureEveryFrame"))
	bool bDisableWhenNotAiming = true;
	// 未瞄准时刷新率 5 = 每秒5次
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "!bDisableWhenNotAiming && bOverrideCaptureEveryFrame", ClampMin = 1, ClampMax = 30))
	float RefreshRateNotAiming = 5.0f;
	// When not aiming clear the scope with a color
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bDisableWhenNotAiming"))
	bool bClearScopeWithColor = true;
	// Color to clear the scope with
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bDisableWhenNotAiming && bClearScopeWithColor"))
	FLinearColor ClearedColor = FLinearColor::Black;
	// When not aiming clear the scope with a material
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bDisableWhenNotAiming && !bClearScopeWithColor"))
	bool bClearScopeWithMaterial = false;
	// Material to clear the scope with
	UPROPERTY(EditDefaultsOnly, Category = "Optimization", meta = (EditCondition = "bDisableWhenNotAiming && !bClearScopeWithColor && bClearScopeWithMaterial"))
	UMaterialInterface* ClearedScopeMaterial;

	UPROPERTY()
	UMaterialInterface* OriginalScopeMaterial;

	FFreeScopeOptimization() = default;

	FFreeScopeOptimization(bool bOverrideCaptureEveryFrame, float RefreshRate, bool bDisableWhenNotAiming,
		float RefreshRateNotAiming, bool bClearScopeWithColor, const FLinearColor& ClearedColor,
		bool bClearScopeWithMaterial, UMaterialInterface* ClearedScopeMaterial,
		UMaterialInterface* OriginalScopeMaterial)
		: bOverrideCaptureEveryFrame(bOverrideCaptureEveryFrame),
		  RefreshRate(RefreshRate),
		  bDisableWhenNotAiming(bDisableWhenNotAiming),
		  RefreshRateNotAiming(RefreshRateNotAiming),
		  bClearScopeWithColor(bClearScopeWithColor),
		  ClearedColor(ClearedColor),
		  bClearScopeWithMaterial(bClearScopeWithMaterial),
		  ClearedScopeMaterial(ClearedScopeMaterial),
		  OriginalScopeMaterial(OriginalScopeMaterial)
	{
	}

	FFreeScopeOptimization(const FFreeScopeOptimization& Other)
		: bOverrideCaptureEveryFrame(Other.bOverrideCaptureEveryFrame),
		  RefreshRate(Other.RefreshRate),
		  bDisableWhenNotAiming(Other.bDisableWhenNotAiming),
		  RefreshRateNotAiming(Other.RefreshRateNotAiming),
		  bClearScopeWithColor(Other.bClearScopeWithColor),
		  ClearedColor(Other.ClearedColor),
		  bClearScopeWithMaterial(Other.bClearScopeWithMaterial),
		  ClearedScopeMaterial(Other.ClearedScopeMaterial),
		  OriginalScopeMaterial(Other.OriginalScopeMaterial)
	{
	}

	FFreeScopeOptimization(FFreeScopeOptimization&& Other) noexcept
		: bOverrideCaptureEveryFrame(Other.bOverrideCaptureEveryFrame),
		  RefreshRate(Other.RefreshRate),
		  bDisableWhenNotAiming(Other.bDisableWhenNotAiming),
		  RefreshRateNotAiming(Other.RefreshRateNotAiming),
		  bClearScopeWithColor(Other.bClearScopeWithColor),
		  ClearedColor(MoveTemp(Other.ClearedColor)),
		  bClearScopeWithMaterial(Other.bClearScopeWithMaterial),
		  ClearedScopeMaterial(Other.ClearedScopeMaterial),
		  OriginalScopeMaterial(Other.OriginalScopeMaterial)
	{
	}

	FFreeScopeOptimization& operator=(const FFreeScopeOptimization& Other)
	{
		if (this == &Other)
			return *this;
		bOverrideCaptureEveryFrame = Other.bOverrideCaptureEveryFrame;
		RefreshRate = Other.RefreshRate;
		bDisableWhenNotAiming = Other.bDisableWhenNotAiming;
		RefreshRateNotAiming = Other.RefreshRateNotAiming;
		bClearScopeWithColor = Other.bClearScopeWithColor;
		ClearedColor = Other.ClearedColor;
		bClearScopeWithMaterial = Other.bClearScopeWithMaterial;
		ClearedScopeMaterial = Other.ClearedScopeMaterial;
		OriginalScopeMaterial = Other.OriginalScopeMaterial;
		return *this;
	}

	FFreeScopeOptimization& operator=(FFreeScopeOptimization&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		bOverrideCaptureEveryFrame = Other.bOverrideCaptureEveryFrame;
		RefreshRate = Other.RefreshRate;
		bDisableWhenNotAiming = Other.bDisableWhenNotAiming;
		RefreshRateNotAiming = Other.RefreshRateNotAiming;
		bClearScopeWithColor = Other.bClearScopeWithColor;
		ClearedColor = MoveTemp(Other.ClearedColor);
		bClearScopeWithMaterial = Other.bClearScopeWithMaterial;
		ClearedScopeMaterial = Other.ClearedScopeMaterial;
		OriginalScopeMaterial = Other.OriginalScopeMaterial;
		return *this;
	}
};

USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeReticleMaterial
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ReticleMaterial")
	UMaterialInstance* ReticleMaterial;
	UPROPERTY(BlueprintReadOnly, Category = "ReticleMaterial")
	UMaterialInstanceDynamic* DynamicReticleMaterial;
	UPROPERTY(BlueprintReadOnly, Category = "ReticleMaterial")
	float ReticleSize = 1.0f;
	UPROPERTY(BlueprintReadOnly, Category = "ReticleMaterial")
	float StartingEyeBoxRange = -2000.0f;
};

USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeSightZoomSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, Category = "ZoomSettings")
	bool bSmoothZoom = false;
	UPROPERTY(EditDefaultsOnly, Category = "ZoomSettings", meta = (EditCondition = "bSmoothZoom"))
	float SmoothZoomSmoothness = 0.02f;
	UPROPERTY(EditDefaultsOnly, Category = "ZoomSettings", meta = (EditCondition = "bSmoothZoom"))
	float SmoothZoomSpeed = 25.0f;
	UPROPERTY(EditDefaultsOnly, Category = "ZoomSettings")
	bool bFreeZoom = true;
	UPROPERTY(EditDefaultsOnly, Category = "ZoomSettings", meta = (EditCondition = "bFreeZoom"))
	float ZoomIncrementAmount = 1.0f;
};

USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeSightMagnification
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MagnificationSettings")
	TArray<float> Magnifications = { 1.0f };
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MagnificationSettings")
	bool bIsFirstFocalPlane = true;
	UPROPERTY(EditDefaultsOnly, Category = "MagnificationSettings")
	float DecreaseReticleScaleAmount = 8.0f;

	uint8 MagnificationIndex = 0;
	float CurrentMagnification = 1.0f;
};

USTRUCT(BlueprintType)
struct FREETRUEFPS_API FReticleSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, Category = "ReticleSettings")
	int32 ReticleMaterialIndex = 1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ReticleSettings")
	TArray<FFreeReticleMaterial> ReticleMaterials;
};

/**
 * RenderTargetSize
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FRenderTargetSize
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Scope")
	int32 Width{512};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Scope")
	int32 Height{512};

	FRenderTargetSize() = default;

	FRenderTargetSize(int32 Width, int32 Height)
		: Width(Width),
		  Height(Height)
	{
	}

	FRenderTargetSize(const FRenderTargetSize& Other)
		: Width(Other.Width),
		  Height(Other.Height)
	{
	}

	FRenderTargetSize(FRenderTargetSize&& Other) noexcept
		: Width(Other.Width),
		  Height(Other.Height)
	{
	}

	FRenderTargetSize& operator=(const FRenderTargetSize& Other)
	{
		if (this == &Other)
			return *this;
		Width = Other.Width;
		Height = Other.Height;
		return *this;
	}

	FRenderTargetSize& operator=(FRenderTargetSize&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		Width = Other.Width;
		Height = Other.Height;
		return *this;
	}
};
