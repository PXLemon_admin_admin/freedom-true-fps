﻿
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FreeHeroInterface.generated.h"

class UFreeDamageType;
// This class does not need to be modified.
UINTERFACE()
class UFreeHeroInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FREETRUEFPS_API IFreeHeroInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual bool GetIsPlayer() const { return true; }
	virtual class USkeletalMeshComponent* GetPawnMesh() const { return nullptr; }
	virtual void WantsToJump() {}
	virtual void WantsToStopJump() {}
	virtual FTransform GetCameraTransform() const { return FTransform::Identity; }
	virtual class UCameraComponent* GetFPCameraComponent() const { return nullptr; }

	virtual void ApplyDamage(float BaseDamage, const UPhysicalMaterial* const PhysicalMaterial,
		AActor* DamageActor, FVector& HitFromDirection, const FHitResult& HitResult,
		AController* EventInstigator, AActor* DamageCauser, TSubclassOf<UFreeDamageType> DamageTypeClass) {}
	
};
