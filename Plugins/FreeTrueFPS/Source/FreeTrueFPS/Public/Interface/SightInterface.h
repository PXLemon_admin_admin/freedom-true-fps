﻿
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SightInterface.generated.h"

// This class does not need to be modified.
UINTERFACE()
class USightInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FREETRUEFPS_API ISightInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual bool GetIsGun() const;
	virtual void ZoomSight(bool bZoomIn);
	virtual float GetZoomSensitivity() const;
	virtual FTransform GetSightAimTransform();

};
