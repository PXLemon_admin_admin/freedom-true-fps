﻿
#pragma once

#include "CoreMinimal.h"
#include "Data/FreeType.h"
#include "Data/AttachmentBoostData.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FreeHeroLibrary.generated.h"


/**
 * FreeHero函数库
 */
UCLASS(Blueprintable)
class FREETRUEFPS_API UFreeHeroLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroLibrary")
	static float CalcDefAmmoInitSpeedByAmmoType(EFreeAmmoType AmmoType);

	/*UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroLibrary")
	static FName GetFreeSocketNameByType(EFreeSocketType FreeSocketType);*/

	/**
	 * 计算加成数据(加)
	 * @param BoostDataNeedAdd 需要加成的数据
	 * @param BoostDataAdd 加成数据 把数据增加到BoostDataNeedAdd
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroLibrary")
	static void CalcAttachmentBoostDataAdd(UPARAM(Ref)FAttachmentBoostData& BoostDataNeedAdd, UPARAM(Ref)const FAttachmentBoostData& BoostDataAdd);

	/**
	 * 计算加成数据(减)
	 * @param BoostDataNeedSubtract 需要减去的数据
	 * @param BoostDataSubtract 加成数据 将BoostDataNeedSubtract的数据(绝对值)减去自身
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeHeroLibrary")
	static void CalcAttachmentBoostDataSubtract(UPARAM(Ref)FAttachmentBoostData& BoostDataNeedSubtract, UPARAM(Ref)const FAttachmentBoostData& BoostDataSubtract);

	/*
	UFUNCTION(BlueprintCallable, Category = "FreeHeroLibrary")
	static void SpawnEffectOnImpact(UPARAM(Ref)const FHitResult& HitResult);
	*/

	static FRotator GetEstimatedMuzzleToScopeZero(const FTransform& MuzzleTransform, const FTransform& SightTransform, const float RangeMeters);

	static FRotator SetMuzzleMOA(FRotator MuzzleRotation, float MOA = 1.0f);
	
};
