﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FreeHeroPlayerState.generated.h"

UCLASS()
class FREETRUEFPS_API AFreeHeroPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	AFreeHeroPlayerState();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

protected:

	/** team number */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_TeamColor)
	int32 TeamNumber;

	/** 击杀数 */
	UPROPERTY(Transient, Replicated)
	int32 NumKills;

	/** 死亡数 */
	UPROPERTY(Transient, Replicated)
	int32 NumDeaths;

	/** 本场比赛射出的子弹数量 */
	UPROPERTY()
	int32 NumBulletsFired;

	/** 本场比赛发射的火箭数量 */
	UPROPERTY()
	int32 NumRocketsFired;

	/** 用户是否退出了比赛 */
	UPROPERTY()
	bool bQuitter;

	void UpdateTeamColors();

public:
	
	/** 复制团队颜色。适当更新玩家的网格颜色 */
	UFUNCTION()
	void OnRep_TeamColor();

	//我们不需要对射击的弹药数量进行服务器身份验证的统计数据 所以只需使用本地函数递增这些数值
	void AddBulletsFired(int32 NumBullets);
	void AddRocketsFired(int32 NumRockets);
	
	
};
