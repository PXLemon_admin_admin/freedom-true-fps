﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FreePlayerController.generated.h"

UCLASS()
class FREETRUEFPS_API AFreePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AFreePlayerController();
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void BeginPlay() override;

	
};
