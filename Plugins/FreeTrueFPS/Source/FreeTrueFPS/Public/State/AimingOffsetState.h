﻿
#pragma once

#include "AimingOffsetState.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FAimingOffsetState
{
	GENERATED_BODY()

	/**
	 * 调整瞄准点的偏移 旋转
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	FRotator AimingOffsetRot{0.f, 0.f, 0.f};

	/**
	 * 调整瞄准点的偏移 位置
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	FVector AimingOffsetLoc{0.f, 0.f, 0.f};
	

	FAimingOffsetState() = default;

	FAimingOffsetState(const FRotator& AimingOffsetRot, const FVector& AimingOffsetLoc)
		: AimingOffsetRot(AimingOffsetRot),
		  AimingOffsetLoc(AimingOffsetLoc)
	{
	}

	FAimingOffsetState(const FAimingOffsetState& Other)
		: AimingOffsetRot(Other.AimingOffsetRot),
		  AimingOffsetLoc(Other.AimingOffsetLoc)
	{
	}

	FAimingOffsetState(FAimingOffsetState&& Other) noexcept
		: AimingOffsetRot(MoveTemp(Other.AimingOffsetRot)),
		  AimingOffsetLoc(MoveTemp(Other.AimingOffsetLoc))
	{
	}

	FAimingOffsetState& operator=(const FAimingOffsetState& Other)
	{
		if (this == &Other)
			return *this;
		AimingOffsetRot = Other.AimingOffsetRot;
		AimingOffsetLoc = Other.AimingOffsetLoc;
		return *this;
	}

	FAimingOffsetState& operator=(FAimingOffsetState&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		AimingOffsetRot = MoveTemp(Other.AimingOffsetRot);
		AimingOffsetLoc = MoveTemp(Other.AimingOffsetLoc);
		return *this;
	}
};
