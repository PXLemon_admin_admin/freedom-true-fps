﻿
#pragma once

#include "FirearmAnimState.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFirearmAnimState
{
	GENERATED_BODY()

	/**
	 * 启用了FireMontage 程序化后坐力将会无效，反之则有效 (只会生效一种)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	bool bEnableFireMontage{true};

	/**
	 * 没有Reload动画时 Reload动作持续时长
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	float NoAnimReloadDuration{0.7f};

	/**
	 * 没有开火动画时 开火动作持续时长
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	float NoAnimFireDuration{1.0f};

	/**
	 * 上半身动画 Locomotion
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UBlendSpace1D* BS1DLocomotion;
	
	/**
	 * 呼吸动画
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimSequence* BreatheIdleAnimAdditive;

	/**
	 * FirstDraw 枪装备到手中时播放的动画
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FullBodyFirstDrawMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FirearmFirstDrawMontage;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FullBodyFireMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FirearmFireMontage;

	/**
	 * Reload
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FullBodyReloadMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FullBodyReloadEmptyMontage;

	/**
	 * Reload Empty
	 */
	FName ReloadMontageNotifyName{TEXT("ReloadComplete")};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FirearmReloadMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FirearmReloadEmptyMontage;

	/**
	 * 武器检视
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FullBodyInspectMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeHeroes | Anim")
	class UAnimMontage* FirearmInspectMontage;


	FFirearmAnimState() = default;

	FFirearmAnimState(bool bEnableFireMontage, float NoAnimReloadDuration, UBlendSpace1D* BS1DLocomotion,
		UAnimSequence* BreatheIdleAnimAdditive, UAnimMontage* FullBodyFirstDrawMontage,
		UAnimMontage* FirearmFirstDrawMontage, UAnimMontage* FullBodyFireMontage, UAnimMontage* FirearmFireMontage,
		UAnimMontage* FullBodyReloadMontage, UAnimMontage* FullBodyReloadEmptyMontage,
		const FName& ReloadMontageNotifyName, UAnimMontage* FirearmReloadMontage,
		UAnimMontage* FirearmReloadEmptyMontage, UAnimMontage* FullBodyInspectMontage,
		UAnimMontage* FirearmInspectMontage)
		: bEnableFireMontage(bEnableFireMontage),
		  NoAnimReloadDuration(NoAnimReloadDuration),
		  BS1DLocomotion(BS1DLocomotion),
		  BreatheIdleAnimAdditive(BreatheIdleAnimAdditive),
		  FullBodyFirstDrawMontage(FullBodyFirstDrawMontage),
		  FirearmFirstDrawMontage(FirearmFirstDrawMontage),
		  FullBodyFireMontage(FullBodyFireMontage),
		  FirearmFireMontage(FirearmFireMontage),
		  FullBodyReloadMontage(FullBodyReloadMontage),
		  FullBodyReloadEmptyMontage(FullBodyReloadEmptyMontage),
		  ReloadMontageNotifyName(ReloadMontageNotifyName),
		  FirearmReloadMontage(FirearmReloadMontage),
		  FirearmReloadEmptyMontage(FirearmReloadEmptyMontage),
		  FullBodyInspectMontage(FullBodyInspectMontage),
		  FirearmInspectMontage(FirearmInspectMontage)
	{
	}

	FFirearmAnimState(const FFirearmAnimState& Other)
		: bEnableFireMontage(Other.bEnableFireMontage),
		  NoAnimReloadDuration(Other.NoAnimReloadDuration),
		  BS1DLocomotion(Other.BS1DLocomotion),
		  BreatheIdleAnimAdditive(Other.BreatheIdleAnimAdditive),
		  FullBodyFirstDrawMontage(Other.FullBodyFirstDrawMontage),
		  FirearmFirstDrawMontage(Other.FirearmFirstDrawMontage),
		  FullBodyFireMontage(Other.FullBodyFireMontage),
		  FirearmFireMontage(Other.FirearmFireMontage),
		  FullBodyReloadMontage(Other.FullBodyReloadMontage),
		  FullBodyReloadEmptyMontage(Other.FullBodyReloadEmptyMontage),
		  ReloadMontageNotifyName(Other.ReloadMontageNotifyName),
		  FirearmReloadMontage(Other.FirearmReloadMontage),
		  FirearmReloadEmptyMontage(Other.FirearmReloadEmptyMontage),
		  FullBodyInspectMontage(Other.FullBodyInspectMontage),
		  FirearmInspectMontage(Other.FirearmInspectMontage)
	{
	}

	FFirearmAnimState(FFirearmAnimState&& Other) noexcept
		: bEnableFireMontage(Other.bEnableFireMontage),
		  NoAnimReloadDuration(Other.NoAnimReloadDuration),
		  BS1DLocomotion(Other.BS1DLocomotion),
		  BreatheIdleAnimAdditive(Other.BreatheIdleAnimAdditive),
		  FullBodyFirstDrawMontage(Other.FullBodyFirstDrawMontage),
		  FirearmFirstDrawMontage(Other.FirearmFirstDrawMontage),
		  FullBodyFireMontage(Other.FullBodyFireMontage),
		  FirearmFireMontage(Other.FirearmFireMontage),
		  FullBodyReloadMontage(Other.FullBodyReloadMontage),
		  FullBodyReloadEmptyMontage(Other.FullBodyReloadEmptyMontage),
		  ReloadMontageNotifyName(std::move(Other.ReloadMontageNotifyName)),
		  FirearmReloadMontage(Other.FirearmReloadMontage),
		  FirearmReloadEmptyMontage(Other.FirearmReloadEmptyMontage),
		  FullBodyInspectMontage(Other.FullBodyInspectMontage),
		  FirearmInspectMontage(Other.FirearmInspectMontage)
	{
	}

	FFirearmAnimState& operator=(const FFirearmAnimState& Other)
	{
		if (this == &Other)
			return *this;
		bEnableFireMontage = Other.bEnableFireMontage;
		NoAnimReloadDuration = Other.NoAnimReloadDuration;
		BS1DLocomotion = Other.BS1DLocomotion;
		BreatheIdleAnimAdditive = Other.BreatheIdleAnimAdditive;
		FullBodyFirstDrawMontage = Other.FullBodyFirstDrawMontage;
		FirearmFirstDrawMontage = Other.FirearmFirstDrawMontage;
		FullBodyFireMontage = Other.FullBodyFireMontage;
		FirearmFireMontage = Other.FirearmFireMontage;
		FullBodyReloadMontage = Other.FullBodyReloadMontage;
		FullBodyReloadEmptyMontage = Other.FullBodyReloadEmptyMontage;
		ReloadMontageNotifyName = Other.ReloadMontageNotifyName;
		FirearmReloadMontage = Other.FirearmReloadMontage;
		FirearmReloadEmptyMontage = Other.FirearmReloadEmptyMontage;
		FullBodyInspectMontage = Other.FullBodyInspectMontage;
		FirearmInspectMontage = Other.FirearmInspectMontage;
		return *this;
	}

	FFirearmAnimState& operator=(FFirearmAnimState&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		bEnableFireMontage = Other.bEnableFireMontage;
		NoAnimReloadDuration = Other.NoAnimReloadDuration;
		BS1DLocomotion = Other.BS1DLocomotion;
		BreatheIdleAnimAdditive = Other.BreatheIdleAnimAdditive;
		FullBodyFirstDrawMontage = Other.FullBodyFirstDrawMontage;
		FirearmFirstDrawMontage = Other.FirearmFirstDrawMontage;
		FullBodyFireMontage = Other.FullBodyFireMontage;
		FirearmFireMontage = Other.FirearmFireMontage;
		FullBodyReloadMontage = Other.FullBodyReloadMontage;
		FullBodyReloadEmptyMontage = Other.FullBodyReloadEmptyMontage;
		ReloadMontageNotifyName = std::move(Other.ReloadMontageNotifyName);
		FirearmReloadMontage = Other.FirearmReloadMontage;
		FirearmReloadEmptyMontage = Other.FirearmReloadEmptyMontage;
		FullBodyInspectMontage = Other.FullBodyInspectMontage;
		FirearmInspectMontage = Other.FirearmInspectMontage;
		return *this;
	}
};
