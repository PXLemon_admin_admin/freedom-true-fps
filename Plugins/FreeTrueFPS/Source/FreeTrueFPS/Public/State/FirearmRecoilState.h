﻿
#pragma once

#include "Data/FreeType.h"
#include "FirearmRecoilState.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFirearmRecoilState
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	class TSubclassOf<class UCameraShakeBase> FireCameraShake;

	//////////////////////////////////////////////////////////////////////////
	// Recoil For Control Rotation 给控制旋转用的

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil | ControlRotation")
	class UCurveFloat* ControlRotRecoilPitchCurve;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil | ControlRotation")
	class UCurveFloat* ControlRotRecoilYawCurve;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil | ControlRotation")
	FFreeMinMax RecoilPitch{0.2f, 0.5f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil | ControlRotation")
	FFreeMinMax RecoilYaw{-0.3f, 0.3f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil | ControlRotation")
	float RecoilInterpSpeed{50.f};
	
	/**
	 * 瞄准时后坐力减少百分比buff (不应该为负)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil", meta = (ClampMin = 1, ClampMax = 100, Units = "%"))
	float AimingRecoilPercentBuff{40.f};

	//////////////////////////////////////////////////////////////////////////
	// 给后坐力程序动画用的

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	class UCurveVector* RecoilLocationRifleCurve;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	class UCurveVector* RecoilRotationRifleCurve;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil", meta = (ClampMin = 1.f, ClampMax = 5.f))
	FFreeMinMax LocationRandomness{1.0f, 2.0f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	bool bEnableRotationClamp{true};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	FFreeMinMax RecoilRotationRandomRoll{-3.0f, 3.0f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	FFreeMinMax RecoilRotationRandomPitch{1.0f, 2.0f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	FFreeMinMax RecoilRotationRandomYaw{-8.0f, 8.0f};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	float RecoilMultiplier{1.0f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeHeroes | Recoil")
	float RecoilMultiplierRotPerAxis{1.0f};

	FFirearmRecoilState() = default;

	FFirearmRecoilState(const TSubclassOf<UCameraShakeBase>& FireCameraShake, UCurveFloat* ControlRotRecoilPitchCurve,
		UCurveFloat* ControlRotRecoilYawCurve, const FFreeMinMax& RecoilPitch, const FFreeMinMax& RecoilYaw,
		float RecoilInterpSpeed, float AimingRecoilPercentBuff, UCurveVector* RecoilLocationRifleCurve,
		UCurveVector* RecoilRotationRifleCurve, const FFreeMinMax& LocationRandomness, bool bEnableRotationClamp,
		const FFreeMinMax& RecoilRotationRandomRoll, const FFreeMinMax& RecoilRotationRandomPitch,
		const FFreeMinMax& RecoilRotationRandomYaw, float RecoilMultiplier, float RecoilMultiplierRotPerAxis)
		: FireCameraShake(FireCameraShake),
		  ControlRotRecoilPitchCurve(ControlRotRecoilPitchCurve),
		  ControlRotRecoilYawCurve(ControlRotRecoilYawCurve),
		  RecoilPitch(RecoilPitch),
		  RecoilYaw(RecoilYaw),
		  RecoilInterpSpeed(RecoilInterpSpeed),
		  AimingRecoilPercentBuff(AimingRecoilPercentBuff),
		  RecoilLocationRifleCurve(RecoilLocationRifleCurve),
		  RecoilRotationRifleCurve(RecoilRotationRifleCurve),
		  LocationRandomness(LocationRandomness),
		  bEnableRotationClamp(bEnableRotationClamp),
		  RecoilRotationRandomRoll(RecoilRotationRandomRoll),
		  RecoilRotationRandomPitch(RecoilRotationRandomPitch),
		  RecoilRotationRandomYaw(RecoilRotationRandomYaw),
		  RecoilMultiplier(RecoilMultiplier),
		  RecoilMultiplierRotPerAxis(RecoilMultiplierRotPerAxis)
	{
	}

	FFirearmRecoilState(const FFirearmRecoilState& Other)
		: FireCameraShake(Other.FireCameraShake),
		  ControlRotRecoilPitchCurve(Other.ControlRotRecoilPitchCurve),
		  ControlRotRecoilYawCurve(Other.ControlRotRecoilYawCurve),
		  RecoilPitch(Other.RecoilPitch),
		  RecoilYaw(Other.RecoilYaw),
		  RecoilInterpSpeed(Other.RecoilInterpSpeed),
		  AimingRecoilPercentBuff(Other.AimingRecoilPercentBuff),
		  RecoilLocationRifleCurve(Other.RecoilLocationRifleCurve),
		  RecoilRotationRifleCurve(Other.RecoilRotationRifleCurve),
		  LocationRandomness(Other.LocationRandomness),
		  bEnableRotationClamp(Other.bEnableRotationClamp),
		  RecoilRotationRandomRoll(Other.RecoilRotationRandomRoll),
		  RecoilRotationRandomPitch(Other.RecoilRotationRandomPitch),
		  RecoilRotationRandomYaw(Other.RecoilRotationRandomYaw),
		  RecoilMultiplier(Other.RecoilMultiplier),
		  RecoilMultiplierRotPerAxis(Other.RecoilMultiplierRotPerAxis)
	{
	}

	FFirearmRecoilState(FFirearmRecoilState&& Other) noexcept
		: FireCameraShake(MoveTemp(Other.FireCameraShake)),
		  ControlRotRecoilPitchCurve(Other.ControlRotRecoilPitchCurve),
		  ControlRotRecoilYawCurve(Other.ControlRotRecoilYawCurve),
		  RecoilPitch(MoveTemp(Other.RecoilPitch)),
		  RecoilYaw(MoveTemp(Other.RecoilYaw)),
		  RecoilInterpSpeed(Other.RecoilInterpSpeed),
		  AimingRecoilPercentBuff(Other.AimingRecoilPercentBuff),
		  RecoilLocationRifleCurve(Other.RecoilLocationRifleCurve),
		  RecoilRotationRifleCurve(Other.RecoilRotationRifleCurve),
		  LocationRandomness(MoveTemp(Other.LocationRandomness)),
		  bEnableRotationClamp(Other.bEnableRotationClamp),
		  RecoilRotationRandomRoll(MoveTemp(Other.RecoilRotationRandomRoll)),
		  RecoilRotationRandomPitch(MoveTemp(Other.RecoilRotationRandomPitch)),
		  RecoilRotationRandomYaw(MoveTemp(Other.RecoilRotationRandomYaw)),
		  RecoilMultiplier(Other.RecoilMultiplier),
		  RecoilMultiplierRotPerAxis(Other.RecoilMultiplierRotPerAxis)
	{
	}

	FFirearmRecoilState& operator=(const FFirearmRecoilState& Other)
	{
		if (this == &Other)
			return *this;
		FireCameraShake = Other.FireCameraShake;
		ControlRotRecoilPitchCurve = Other.ControlRotRecoilPitchCurve;
		ControlRotRecoilYawCurve = Other.ControlRotRecoilYawCurve;
		RecoilPitch = Other.RecoilPitch;
		RecoilYaw = Other.RecoilYaw;
		RecoilInterpSpeed = Other.RecoilInterpSpeed;
		AimingRecoilPercentBuff = Other.AimingRecoilPercentBuff;
		RecoilLocationRifleCurve = Other.RecoilLocationRifleCurve;
		RecoilRotationRifleCurve = Other.RecoilRotationRifleCurve;
		LocationRandomness = Other.LocationRandomness;
		bEnableRotationClamp = Other.bEnableRotationClamp;
		RecoilRotationRandomRoll = Other.RecoilRotationRandomRoll;
		RecoilRotationRandomPitch = Other.RecoilRotationRandomPitch;
		RecoilRotationRandomYaw = Other.RecoilRotationRandomYaw;
		RecoilMultiplier = Other.RecoilMultiplier;
		RecoilMultiplierRotPerAxis = Other.RecoilMultiplierRotPerAxis;
		return *this;
	}

	FFirearmRecoilState& operator=(FFirearmRecoilState&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		FireCameraShake = MoveTemp(Other.FireCameraShake);
		ControlRotRecoilPitchCurve = Other.ControlRotRecoilPitchCurve;
		ControlRotRecoilYawCurve = Other.ControlRotRecoilYawCurve;
		RecoilPitch = MoveTemp(Other.RecoilPitch);
		RecoilYaw = MoveTemp(Other.RecoilYaw);
		RecoilInterpSpeed = Other.RecoilInterpSpeed;
		AimingRecoilPercentBuff = Other.AimingRecoilPercentBuff;
		RecoilLocationRifleCurve = Other.RecoilLocationRifleCurve;
		RecoilRotationRifleCurve = Other.RecoilRotationRifleCurve;
		LocationRandomness = MoveTemp(Other.LocationRandomness);
		bEnableRotationClamp = Other.bEnableRotationClamp;
		RecoilRotationRandomRoll = MoveTemp(Other.RecoilRotationRandomRoll);
		RecoilRotationRandomPitch = MoveTemp(Other.RecoilRotationRandomPitch);
		RecoilRotationRandomYaw = MoveTemp(Other.RecoilRotationRandomYaw);
		RecoilMultiplier = Other.RecoilMultiplier;
		RecoilMultiplierRotPerAxis = Other.RecoilMultiplierRotPerAxis;
		return *this;
	}
};
