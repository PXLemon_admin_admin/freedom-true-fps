﻿
#pragma once


#include "FreeAttachmentState.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeAttachmentState
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeMuzzle* Muzzle;
	
	/*UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeBarrel* Barrel;*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeForeGrip* ForeGrip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeHandGuard* HandGuard;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeLightLaser* LightLaser;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeMagazine* Magazine;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeSightBase* Sight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Part")
	class AFreeStock* Stock;

	FFreeAttachmentState() = default;

	FFreeAttachmentState(AFreeMuzzle* Muzzle, AFreeForeGrip* ForeGrip, AFreeHandGuard* HandGuard,
		AFreeLightLaser* LightLaser, AFreeMagazine* Magazine, AFreeSightBase* Sight, AFreeStock* Stock)
		: Muzzle(Muzzle),
		  ForeGrip(ForeGrip),
		  HandGuard(HandGuard),
		  LightLaser(LightLaser),
		  Magazine(Magazine),
		  Sight(Sight),
		  Stock(Stock)
	{
	}

	FFreeAttachmentState(const FFreeAttachmentState& Other)
		: Muzzle(Other.Muzzle),
		  ForeGrip(Other.ForeGrip),
		  HandGuard(Other.HandGuard),
		  LightLaser(Other.LightLaser),
		  Magazine(Other.Magazine),
		  Sight(Other.Sight),
		  Stock(Other.Stock)
	{
	}

	FFreeAttachmentState(FFreeAttachmentState&& Other) noexcept
		: Muzzle(Other.Muzzle),
		  ForeGrip(Other.ForeGrip),
		  HandGuard(Other.HandGuard),
		  LightLaser(Other.LightLaser),
		  Magazine(Other.Magazine),
		  Sight(Other.Sight),
		  Stock(Other.Stock)
	{
	}

	FFreeAttachmentState& operator=(const FFreeAttachmentState& Other)
	{
		if (this == &Other)
			return *this;
		Muzzle = Other.Muzzle;
		ForeGrip = Other.ForeGrip;
		HandGuard = Other.HandGuard;
		LightLaser = Other.LightLaser;
		Magazine = Other.Magazine;
		Sight = Other.Sight;
		Stock = Other.Stock;
		return *this;
	}

	FFreeAttachmentState& operator=(FFreeAttachmentState&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		Muzzle = Other.Muzzle;
		ForeGrip = Other.ForeGrip;
		HandGuard = Other.HandGuard;
		LightLaser = Other.LightLaser;
		Magazine = Other.Magazine;
		Sight = Other.Sight;
		Stock = Other.Stock;
		return *this;
	}
};
