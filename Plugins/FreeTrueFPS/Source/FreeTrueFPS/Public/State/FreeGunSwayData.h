﻿
#pragma once

#include "CoreMinimal.h"
#include "Data/FreeType.h"
#include "FreeGunSwayData.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeGunSwayData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sway")
	FFreeMinMax SwayRotationRoll{-10.f, 10.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sway")
	FFreeMinMax SwayRotationPitch{-10.f, 10.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sway")
	FFreeMinMax SwayRotationYaw{-10.f, 10.f};

	FFreeGunSwayData() = default;

	FFreeGunSwayData(const FFreeMinMax& SwayRotationRoll, const FFreeMinMax& SwayRotationPitch,
		const FFreeMinMax& SwayRotationYaw)
		: SwayRotationRoll(SwayRotationRoll),
		  SwayRotationPitch(SwayRotationPitch),
		  SwayRotationYaw(SwayRotationYaw)
	{
	}

	FFreeGunSwayData(const FFreeGunSwayData& Other)
		: SwayRotationRoll(Other.SwayRotationRoll),
		  SwayRotationPitch(Other.SwayRotationPitch),
		  SwayRotationYaw(Other.SwayRotationYaw)
	{
	}

	FFreeGunSwayData(FFreeGunSwayData&& Other) noexcept
		: SwayRotationRoll(MoveTemp(Other.SwayRotationRoll)),
		  SwayRotationPitch(MoveTemp(Other.SwayRotationPitch)),
		  SwayRotationYaw(MoveTemp(Other.SwayRotationYaw))
	{
	}

	FFreeGunSwayData& operator=(const FFreeGunSwayData& Other)
	{
		if (this == &Other)
			return *this;
		SwayRotationRoll = Other.SwayRotationRoll;
		SwayRotationPitch = Other.SwayRotationPitch;
		SwayRotationYaw = Other.SwayRotationYaw;
		return *this;
	}

	FFreeGunSwayData& operator=(FFreeGunSwayData&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		SwayRotationRoll = MoveTemp(Other.SwayRotationRoll);
		SwayRotationPitch = MoveTemp(Other.SwayRotationPitch);
		SwayRotationYaw = MoveTemp(Other.SwayRotationYaw);
		return *this;
	}
};
