﻿
#pragma once

#include "Data/FreeType.h"
#include "FreeProjectileState.generated.h"

class AFreeProjectile;
/**
 * 
 */
USTRUCT(BlueprintType)
struct FREETRUEFPS_API FFreeProjectileState
{
	GENERATED_USTRUCT_BODY()
	
	/*UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	TSubclassOf<AFreeProjectile> ProjectileClass;*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float Damage{10.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	EFreeAmmoType AmmoType{EFreeAmmoType::RifleAmmo};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	TSubclassOf<class UFreeDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float ProjectileLife{5.f};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float InitSphereRadius{2.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float InitSpeed{20000.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float MaxSpeed{25555.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float ProjectileGravityScale{1.f};

	FFreeProjectileState() = default;

	FFreeProjectileState(float Damage, EFreeAmmoType AmmoType, const TSubclassOf<UFreeDamageType>& DamageType,
		float ProjectileLife, float InitSphereRadius, float InitSpeed, float MaxSpeed, float ProjectileGravityScale)
		: Damage(Damage),
		  AmmoType(AmmoType),
		  DamageType(DamageType),
		  ProjectileLife(ProjectileLife),
		  InitSphereRadius(InitSphereRadius),
		  InitSpeed(InitSpeed),
		  MaxSpeed(MaxSpeed),
		  ProjectileGravityScale(ProjectileGravityScale)
	{
	}

	FFreeProjectileState(const FFreeProjectileState& Other)
		: Damage(Other.Damage),
		  AmmoType(Other.AmmoType),
		  DamageType(Other.DamageType),
		  ProjectileLife(Other.ProjectileLife),
		  InitSphereRadius(Other.InitSphereRadius),
		  InitSpeed(Other.InitSpeed),
		  MaxSpeed(Other.MaxSpeed),
		  ProjectileGravityScale(Other.ProjectileGravityScale)
	{
	}

	FFreeProjectileState(FFreeProjectileState&& Other) noexcept
		: Damage(Other.Damage),
		  AmmoType(Other.AmmoType),
		  DamageType(MoveTemp(Other.DamageType)),
		  ProjectileLife(Other.ProjectileLife),
		  InitSphereRadius(Other.InitSphereRadius),
		  InitSpeed(Other.InitSpeed),
		  MaxSpeed(Other.MaxSpeed),
		  ProjectileGravityScale(Other.ProjectileGravityScale)
	{
	}

	FFreeProjectileState& operator=(const FFreeProjectileState& Other)
	{
		if (this == &Other)
			return *this;
		Damage = Other.Damage;
		AmmoType = Other.AmmoType;
		DamageType = Other.DamageType;
		ProjectileLife = Other.ProjectileLife;
		InitSphereRadius = Other.InitSphereRadius;
		InitSpeed = Other.InitSpeed;
		MaxSpeed = Other.MaxSpeed;
		ProjectileGravityScale = Other.ProjectileGravityScale;
		return *this;
	}

	FFreeProjectileState& operator=(FFreeProjectileState&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		Damage = Other.Damage;
		AmmoType = Other.AmmoType;
		DamageType = MoveTemp(Other.DamageType);
		ProjectileLife = Other.ProjectileLife;
		InitSphereRadius = Other.InitSphereRadius;
		InitSpeed = Other.InitSpeed;
		MaxSpeed = Other.MaxSpeed;
		ProjectileGravityScale = Other.ProjectileGravityScale;
		return *this;
	}
	
};
