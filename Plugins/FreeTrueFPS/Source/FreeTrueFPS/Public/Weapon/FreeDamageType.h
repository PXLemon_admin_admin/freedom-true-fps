﻿
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "Engine/Canvas.h"
#include "FreeDamageType.generated.h"

/**
 * 
 */
UCLASS(const, Blueprintable, BlueprintType)
class FREETRUEFPS_API UFreeDamageType : public UDamageType
{
	GENERATED_BODY()

	/** 通过该武器杀死时在死亡消息日志中显示的图标 */
	UPROPERTY(EditDefaultsOnly, Category = "HUD")
	FCanvasIcon KillIcon;

	/** 在玩家受到该伤害类型击中时播放的力反馈效果 */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UForceFeedbackEffect* HitForceFeedback;

	/** 在玩家被该伤害类型击杀时播放的力反馈效果 */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UForceFeedbackEffect* KilledForceFeedback;
};
