﻿
#pragma once

#include "CoreMinimal.h"
#include "Data/FreeType.h"
#include "Data/AttachmentBoostData.h"
#include "Interface/SightInterface.h"
#include "FreeFirearmBase.generated.h"

#define SECONDOFONEMINUTE 60.f

namespace EWeaponState
{
	enum Type
	{
		Idle,
		Firing,
		Reloading,
		Equipping,
	};
}

USTRUCT()
struct FREETRUEFPS_API FFireProjectileParam
{
	GENERATED_USTRUCT_BODY()

	FVector Origin;
	FVector ShootDirection;
	
	FFireProjectileParam()
	{
		Origin = FVector::ZeroVector;
		ShootDirection = FVector::ZeroVector;
	}

	FFireProjectileParam(const FVector Origin, const FVector ShotDir) : Origin(Origin), ShootDirection(ShotDir) {}

	FFireProjectileParam(const FFireProjectileParam& Other)
		: Origin(Other.Origin),
		  ShootDirection(Other.ShootDirection)
	{
	}

	FFireProjectileParam(FFireProjectileParam&& Other) noexcept
		: Origin(MoveTemp(Other.Origin)),
		  ShootDirection(MoveTemp(Other.ShootDirection))
	{
	}

	FFireProjectileParam& operator=(const FFireProjectileParam& Other)
	{
		if (this == &Other)
			return *this;
		Origin = Other.Origin;
		ShootDirection = Other.ShootDirection;
		return *this;
	}

	FFireProjectileParam& operator=(FFireProjectileParam&& Other) noexcept
	{
		if (this == &Other)
			return *this;
		Origin = MoveTemp(Other.Origin);
		ShootDirection = MoveTemp(Other.ShootDirection);
		return *this;
	}
};

UCLASS()
class FREETRUEFPS_API AFreeFirearmBase : public AActor, public ISightInterface
{
	GENERATED_BODY()

public:
	AFreeFirearmBase();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;
	virtual void PostInitProperties() override;

protected:
	virtual void BeginPlay() override;

	virtual void OnRep_Owner() override;

	void InitBaseData(class UFirearmBaseData* Data);

protected:

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "FreeFirearm")
	FName WeaponName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm")
	float BaseDamage{30.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm")
	float AimingInterpSpeed{50.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm")
	EFreeWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm")
	FName AttachToPawnSocket;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FreeFirearm")
	float FireRateRPM{700.f};

	float AutoFireTimerRate;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm")
	class USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(ReplicatedUsing = OnRep_HeroComponent, BlueprintReadOnly, Category = "FreeFirearm | Ref")
	class UFreeHeroComponent* HeroComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FreeFirearm")
	class UFirearmBaseData* FirearmBaseData;

	UPROPERTY(BlueprintReadOnly, Category = "FreeFirearm")
	EFreeFireMode CurrentFireMode;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm", meta = (ArrayClamp = "0, 3"))
	TArray<EFreeFireMode> FireModes;
	UPROPERTY(ReplicatedUsing = OnRep_FireModeIndex, BlueprintReadOnly, Category = "FreeFirearm")
	uint8 FireModeIndex{0};

	//////////////////////////////////////////////////////////////////////////
	// 配件
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Sights")
	TArray<class AFreeSightBase*> Sights;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "FreeFirearm | Sights")
	class AFreeSightBase* CurrentSight;

	UPROPERTY(ReplicatedUsing = OnRep_SightIndex, BlueprintReadOnly, Category = "FreeFirearm | Sights")
	uint8 SightIndex{0};

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeMuzzle* Muzzle;
	
	/*UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeBarrel* Barrel;*/

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeHandGuard* HandGuard;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeForeGrip* ForeGrip;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeLightLaser* LightLaser;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	TArray<class AFreeSightBase*> ReplicatedSights;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeMagazine* Magazine;
	
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Attachments")
	class AFreeStock* Stock;
	

	// 配件加成数据(累计)
	UPROPERTY(Replicated/*ReplicatedUsing = OnRep_AttachmentBoostData*/)
	FAttachmentBoostData AttachmentBoostData;

	UPROPERTY(BlueprintReadOnly, Category = "FreeFirearm | Ammo")
	EFreeAmmoType AmmoType;

	/**
	 * 弹夹子弹数量
	 */
	UPROPERTY(BlueprintReadOnly, Category = "FreeFirearm | Ammo", meta = (ClampMin = 0))
	int32 MagCapacity;

	/**
	 * 每个弹夹最大容量
	 */
	UPROPERTY(BlueprintReadOnly, Category = "FreeFirearm | Ammo", meta = (ClampMin = 0))
	int32 PerMagCapacityMaxAmmo;

	/**
	 * 当前弹夹剩余的子弹
	 */
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "FreeFirearm | Ammo", meta = (ClampMin = 0))
	int32 AmmoInClip;

	/**
	 * 当前总共子弹数量
	 */
	UPROPERTY(BlueprintReadOnly, Category = "FreeFirearm | Ammo", meta = (ClampMin = 0))
	int32 TotalCurrentAmmo;

	/** burst counter */
	UPROPERTY()
	int32 BurstCount{3};
	UPROPERTY()
	int32 BurstCounter{0};
	

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "FreeHeroes | States")
	bool bIsReloadingForHandIK{false};

	UPROPERTY(ReplicatedUsing = OnRep_PendingReload)
	bool bPendingReload;

	/**
	 * 正在开火
	 */
	bool bFiring{false};

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	/** Handle for efficient management of StopReload timer */
	FTimerHandle TimerHandle_StopReload;

	/** Handle for efficient management of ReloadWeapon timer */
	FTimerHandle TimerHandle_ReloadWeapon;

	/** Handle for efficient management of HandleFiring timer */
	FTimerHandle TimerHandle_HandleFiring;

	FTimerHandle TimerHandle_FireAction;

	//////////////////////////////////////////////////////////////////////////
	// Recoil

	// Temp

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil")
	float RandomRecoilPitchMin{-1.f};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil")
	float RandomRecoilPitchMax{5.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil")
	float RandomRecoilYawMin{-0.8f};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil")
	float RandomRecoilYawMax{0.8f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil")
	float RandomRecoilInterpSpeed{10.f};

	// End

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil")
	FFreeMinMax RecoilPitch{0.2f, 0.5f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil")
	FFreeMinMax RecoilYaw{-0.3f, 0.3f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil")
	float RecoilInterpSpeed{50.f};

	/**
	 * 基本后坐力Buff (减少后坐力)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Recoil", meta = (ClampMin = 0.01, ClampMax = 100))
	float BaseRecoilBuffPercent{0.01f};
	
	/**
	 * 瞄准时后坐力减少百分比buff (不应该为负)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FreeFirearm | Recoil", meta = (ClampMin = 0, ClampMax = 100))
	float AimingRecoilBuffPercent{40.f};

	// 后坐力
	float NewPitchRecoilAmount;
	float OldPitchRecoilAmount;
	float PitchRecoilAmount;
	float NewYawRecoilAmount;
	float OldYawRecoilAmount;
	float YawRecoilAmount;
	float RecoilCoordPerShoot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Shoot")
	float ShootRandomMaxPitchDegrees{2.f};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Shoot")
	float ShootRandomMaxYawDegrees{2.f};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeFirearm | Shoot")
	float ShootDistanceMeter{500.f};

	/**
	 * 调整瞄准点的偏移旋转
	 */
	FRotator AimingOffsetRot;

	FVector AimingOffsetLoc;
	
	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	UFUNCTION(server, Reliable)
	void Server_StartFire(const FProjectileTransform& ProjectileTransform/*FVector Origin, FVector_NetQuantizeNormal ShootDir*/);
	void Server_StartFire_Implementation(const FProjectileTransform& ProjectileTransform/*FVector Origin, FVector_NetQuantizeNormal ShootDir*/);

	UFUNCTION(Server, Reliable)
	void Server_StopFire();
	void Server_StopFire_Implementation();

	UFUNCTION(NetMulticast, Unreliable)
	void Multi_StartFire(const FProjectileTransform& ProjectileTransform);
	void Multi_StartFire_Implementation(const FProjectileTransform& ProjectileTransform);

	//////////////////////////////////////////////////////////////////////////
	// RPC

	UFUNCTION(Server, Unreliable)
	void Server_SetFireModeIndex(uint8 NewIndex);
	void Server_SetFireModeIndex_Implementation(uint8 NewIndex);

	UFUNCTION(Server, Unreliable)
	void Server_SetSightIndex(uint8 NewIndex);
	void Server_SetSightIndex_Implementation(uint8 NewIndex);

	UFUNCTION(Server, Reliable)
	void Server_SetBoostData(const FAttachmentBoostData& BoostData);
	void Server_SetBoostData_Implementation(const FAttachmentBoostData& BoostData);

	UFUNCTION(Server, reliable)
	void Server_SetBoostOnEquip(const class AAttachmentBase* Attachment, bool Add);
	void Server_SetBoostOnEquip_Implementation(const class AAttachmentBase* Attachment, bool Add);

	UFUNCTION(Server, Reliable)
	void Server_SetMuzzle(class AFreeMuzzle* NewMuzzle);
	void Server_SetMuzzle_Implementation(class AFreeMuzzle* NewMuzzle);

	UFUNCTION(Server, Reliable)
	void Server_SetBarrel(class AFreeBarrel* NewBarrel);
	void Server_SetBarrel_Implementation(class AFreeBarrel* NewBarrel);

	UFUNCTION(Server, Reliable)
	void Server_SetHandGuard(class AFreeHandGuard* NewHandGuard);
	void Server_SetHandGuard_Implementation(class AFreeHandGuard* NewHandGuard);

	UFUNCTION(Server, Reliable)
	void Server_SetForeGrip(class AFreeForeGrip* NewForeGrip);
	void Server_SetForeGrip_Implementation(class AFreeForeGrip* NewForeGrip);

	UFUNCTION(Server, Reliable)
	void Server_SetLightLaser(class AFreeLightLaser* NewLightLaser);
	void Server_SetLightLaser_Implementation(class AFreeLightLaser* NewLightLaser);

	UFUNCTION(Server, Reliable)
	void Server_SetSight(class AFreeSightBase* NewSight);
	void Server_SetSight_Implementation(class AFreeSightBase* NewSight);

	UFUNCTION(Server, Reliable)
	void Server_SetMagazine(class AFreeMagazine* NewMagazine);
	void Server_SetMagazine_Implementation(class AFreeMagazine* NewMagazine);

	UFUNCTION(Server, Reliable)
	void Server_SetStock(class AFreeStock* NewStock);
	void Server_SetStock_Implementation(class AFreeStock* NewStock);

	//////////////////////////////////////////////////////////////////////////
	// Replication

	UFUNCTION()
	void OnRep_FireModeIndex();

	UFUNCTION()
	void OnRep_SightIndex();

	UFUNCTION()
	void OnRep_HeroComponent();

	UFUNCTION()
	void OnRep_PendingReload();
	
	/*UFUNCTION()
	void OnRep_AttachmentBoostData();*/
	
	/*UFUNCTION()
	void OnRep_MyPawn();*/

	/** 在网络游戏中调用 为射击动作执行美观的特效*/
	virtual void SimulateWeaponFire();

	/** 在网络游戏中调用 为射击动作执行美观的特效 (e.g. for a looping shot). */
	virtual void StopSimulatingWeaponFire();

	//////////////////////////////////////////////////////////////////////////
	// Ammo

	/** [server] add ammo */
	void GiveAmmo(int AddAmount);
	
	/** consume a bullet */
	void UseAmmo();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** attaches weapon mesh to pawn's mesh */
	void AttachMeshToPawn();

	/** detaches weapon mesh from pawn */
	void DetachMeshFromPawn();

	/**
	 * FireModes检查
	 */
	void CheckFireModes();

	void StartFireTimer();

	FTransform GetPawnFPCameraTransform() const;

	void StartLineTrace(FVector CameraLoc, FRotator CameraRot);
	
	bool CheckCanReload() const;
	bool AmmoInClipIsEmpty() const;
	class UAnimMontage* GetFullBodyReloadMontage() const;
	class UAnimMontage* GetGunReloadMontage() const;
	void PlayReloadAnim();

	UFUNCTION(Server, Reliable)
	void Server_StartReload();
	void Server_StartReload_Implementation();
	UFUNCTION(NetMulticast, Unreliable)
	void Multi_StartReload();
	void Multi_StartReload_Implementation();

	float CalcMontageLength(const class UAnimMontage* Montage, float Min);

	UFUNCTION()
	void StopReload();

	UFUNCTION()
	void SetReloadingForHandIK(FName NotifyName);

	UFUNCTION()
	void ReloadComplete(FName NotifyName);

	UFUNCTION()
	void ReloadOnInterrupted(FName NotifyName);

	// Fire Effect
	void PlayRecoil();
	void RandomRecoil();
	void PlayFireAnim();
	void PlayFireFX();
	void PlayFireSound();
	void PlayFireCameraShake(bool bEnableCameraShake = true);
	void PlayFireAllEffect();
	// End Fire Effect


	// UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetAttachmentBoostDataOnEquip(const class AAttachmentBase* Attachment, const bool Add = true);

	/** get the originating location for camera damage */
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	/** 获得武器的目标，允许武器进行调整 */
	virtual FVector GetAdjustedAim() const;
	
	FVector GetMuzzleLocation() const;
	
	FVector GetMuzzleDirection() const;

	FTransform GetMuzzleTransform() const;

	/** find hit */
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

	FFireProjectileParam CalcBeforeFireProjectile();

	void FireProjectile(FVector Origin, FVector_NetQuantizeNormal ShootDir);

	FProjectileTransform GetMuzzleProjectileSocketTransform(float Meters, float MOA);

	void HandleAmmo();

	void HandleProjectile(const FProjectileTransform& ProjectileTransform);

	UFUNCTION()
	void FiringActionComplete();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Part")
	FAttachmentBoostData& GetPartBoostData();

public:
	
	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** weapon is being equipped by owner pawn */
	virtual void OnEquip(const class AFreeFirearmBase* LastWeapon);

	/** weapon is holstered by owner pawn */
	virtual void OnUnEquip();
	
	/** [server] weapon was added to pawn's inventory */
	virtual void OnEnterInventory(class UFreeHeroComponent* InHeroComponent);

	/** [server] weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	//////////////////////////////////////////////////////////////////////////
	// 配件

	/** 装备配件时 */
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void OnEquipAttachment(const class AAttachmentBase* Attachment);

	/** 卸载配件时 */
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void OnUnEquipAttachment(const class AAttachmentBase* Attachment);

	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetMuzzle(class AFreeMuzzle* NewMuzzle);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetHandGuard(class AFreeHandGuard* NewHandGuard);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetForeGrip(class AFreeForeGrip* NewForeGrip);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetLightLaser(class AFreeLightLaser* NewLightLaser);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetMagazine(class AFreeMagazine* NewMagazine);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void SetStock(class AFreeStock* NewStock);
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Part")
	void AddSight(class AFreeSightBase* NewSight);

	bool CanFire() const;

	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Fire")
	virtual void StartFire();
	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Fire")
	virtual void StopFire();

	void ResetRecoil();
	
	void SwitchFireMode();

	void Reload();

	bool ApplyProjectileState(struct FFreeProjectileState& Data) const;

	//UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Set")
	void SetMyPawn(class APawn* NewPawn);
	//UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Set")
	void SetHeroComponent(class UFreeHeroComponent* InHeroComponent);

	UFUNCTION(BlueprintCallable, Category = "FreeFirearm | Sight")
	void SwitchSight();

	//////////////////////////////////////////////////////////////////////////
	// Interface
	virtual bool GetIsGun() const override;
	virtual void ZoomSight(bool bZoomIn) override;
	virtual float GetZoomSensitivity() const override;
	
	/**
	 * 获取当前倍镜瞄准镜的缩放灵敏度
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	virtual float GetSightZoomSensitivity() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	virtual FTransform GetSightAimTransform() override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	FName GetAttachToPawnSocketName() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	FName GetReloadMontageNotifyName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	class AFreeSightBase* GetCurrentSight() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	struct FFirearmAnimState& GetFirearmAnimDataRef() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	struct FFirearmRecoilState& GetFirearmRecoilDataRef() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	struct FFreeGunSwayData& GetFirearmSwayDataRef() const;
	
	struct FFirearmAnimState* GetFirearmAnimData() const;

	struct FFirearmRecoilState* GetFirearmRecoilData() const;

	struct FFreeGunSwayData* GetFirearmSwayData() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	class USkeletalMeshComponent* GetFirearmMesh() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	EFreeFireMode GetCurrentFireMode() const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	class UFreeHeroComponent* GetHeroComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	int32 GetMaxAmmo() const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	int32 GetAmmoInClip() const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	int32 GetTotalCurrentAmmo() const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	EFreeAmmoType GetAmmoType() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	FRotator GetAimingOffsetRot() const;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "FreeFirearm | Get")
	FVector GetAimingOffsetLoc() const;

	bool GetIsReloadingForHandIK() const;

	float GetAimingInterpSpeed() const;

	float GetFormatAimingRecoilBuffPercent() const;
	float GetFormatBoostDataRecoilPercent() const;

};
