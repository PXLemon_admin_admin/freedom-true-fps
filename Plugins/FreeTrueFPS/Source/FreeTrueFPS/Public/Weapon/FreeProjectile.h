﻿
#pragma once

#include "CoreMinimal.h"
#include "State/FreeProjectileState.h"
#include "FreeProjectile.generated.h"

struct FAttachmentBoostData;
class AFreeFirearmBase;
class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class FREETRUEFPS_API AFreeProjectile : public AActor
{
	GENERATED_BODY()

public:
	AFreeProjectile();
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleDefaultsOnly, Category = "FreeProjectile")
	UProjectileMovementComponent* MovementComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "FreeProjectile")
	USphereComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "FreeProjectile")
	UParticleSystemComponent* ParticleComp;

protected:

	FVector LastPosition;

	UPROPERTY(EditDefaultsOnly, Category = "FreeProjectile")
	TEnumAsByte<ECollisionChannel> CollisionChannel;

	UPROPERTY(EditDefaultsOnly, Category = "FreeProjectile")
	uint16 BulletWeightGrains{20};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	FFreeProjectileState ProjectileData;
	
	/** controller that fired me (cache for damage calculations) */
	TWeakObjectPtr<AController> MyController;

	/** shutdown projectile and prepare for destruction */
	void DisableAndDestroy();

	/** update velocity on client */
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "FreeProjectile")
	void DoEnvImpact(const FHitResult& HitResult);
	
	void DoImpact(const FHitResult& HitResult);

	void DoSetSpeed();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FreeProjectile")
	float MovePerTick{1.f};

public:
	/** setup velocity */
	void InitVelocity(FVector& ShootDirection);

	/*void MaxSpeedBoost(float SpeedBoost);*/

	/** handle hit */
	UFUNCTION()
	void OnImpact(const FHitResult& HitResult);
		
	/** Need OnServer Boost Data */
	void SetBoostData(const FAttachmentBoostData& BoostData);

protected:
	FORCEINLINE UProjectileMovementComponent* GetMovementComp() const { return MovementComp; }
	FORCEINLINE USphereComponent* GetCollisionComp() const { return CollisionComp; }
	FORCEINLINE UParticleSystemComponent* GetParticleComp() const { return ParticleComp; }
	
};
