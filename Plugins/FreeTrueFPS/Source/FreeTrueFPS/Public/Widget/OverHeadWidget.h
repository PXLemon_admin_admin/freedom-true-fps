﻿
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OverHeadWidget.generated.h"

/**
 * 
 */
UCLASS()
class FREETRUEFPS_API UOverHeadWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, Category = "Free | Text", meta = (BindWidget))
	class UTextBlock* DisplayText;

	void SetDisplayText(const FString& TextToDisplay);

	UFUNCTION(BlueprintCallable, Category = "Free | Show")
	void ShowPlayerNetRoleText(const APawn* const InPawn);
};
