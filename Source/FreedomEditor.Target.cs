
using UnrealBuildTool;
using System.Collections.Generic;

public class FreedomEditorTarget : TargetRules
{
	public FreedomEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Freedom" } );
	}
}
